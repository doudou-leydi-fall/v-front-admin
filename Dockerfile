FROM node:alpine as admin
RUN apk add --update python make g++\
   && rm -rf /var/cache/apk/*
RUN apk add --update npm
WORKDIR /app
COPY ./.env .env
COPY ./.env.prod .env.prod
COPY ./package.json ./package.json
COPY ./codegen.yml ./codegen.yml 
COPY ./tsconfig.json ./tsconfig.json
RUN yarn --network-timeout 100000
RUN yarn add global react-scripts@3.4.0
COPY ./src ./src
COPY ./public ./public
RUN yarn run build
# production environment
FROM nginx:alpine
COPY --from=admin /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
ADD ./nginx /etc/nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]