#!/bin/bash
git checkout .
git pull
docker build --no-cache -t registry.v.io:9700/v-front-admin:latest .
#=======
docker push registry.v.io:9700/v-front-admin:latest
#=======
kubectl delete -f v-front-admin.yml
#=======
kubectl apply -f v-front-admin.yml
chmod +x ./deploy.sh