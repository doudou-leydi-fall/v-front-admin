import { combineReducers } from "redux";
import placeList from "./place/list";
import placeDetails from "./place/details";
import ui from "./ui/global";

export default combineReducers({
  placeList,
  placeDetails,
  ui
})