import { SET_PLACE_CATEGORIES } from "../constants/action-types";

const initialState = {
  categories: []
}

export default function place(state = initialState, action) {
  switch (action.type) {
    case SET_PLACE_CATEGORIES:
      console.log("SET_PLACE_CRITERIA", state);
      let criteria = state.criteria.filter(criterion => criterion.name !== "categories");
      criteria = [...state.criteria, { name: "categories", collection: action.payload }];
      console.log("reducer", criteria);
      return { ...state, criteria};
    default:
      return state
  }
}