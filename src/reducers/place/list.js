import { 
  SET_PLACE_CATEGORIES, 
  SET_PLACELIST_SIZE, 
  SET_PLACELIST_FROM,
  SET_PLACELIST_PAGER,
  SET_PLACELIST_DISTRICT
} from "../../constants/action-types";

const initialState = {
  categories: [],
  size: 10,
  from: 0,
  pager: {
    size: 10,
    from: 0,
    page: 0
  },
  district: [] 
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PLACELIST_PAGER:
      return { ...state, pager: action.payload };
    case SET_PLACE_CATEGORIES:
      return { ...state, categories: action.payload };
    case SET_PLACELIST_SIZE:
      return { ...state, size: action.payload };
    case SET_PLACELIST_FROM:
      return { ...state, from: action.payload };
    case SET_PLACELIST_DISTRICT:
      return { ...state, district: action.payload };
    default:
      return state
  }
}