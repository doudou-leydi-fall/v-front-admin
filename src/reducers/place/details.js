import { 
  SET_PLACEDETAILS_DISPLAYMODE
} from "../../constants/action-types";

const initialState = {
  displayMode: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PLACEDETAILS_DISPLAYMODE:
      return { ...state, displayMode: action.payload };
    default:
      return state
  }
};

