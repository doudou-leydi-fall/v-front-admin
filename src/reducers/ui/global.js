import { SET_BACKDROP } from "../../constants/action-types";

const initialState = {
  backdropOpen: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BACKDROP:
      return { ...state, backdropOpen: action.payload };
    default:
      return state
  }
}