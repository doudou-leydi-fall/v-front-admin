// RootContext.js
import React, { useEffect, useReducer } from 'react';
import { useSnackbar } from "notistack";
import { ConfirmModal } from './screens/components';
export const RootContext: any = React.createContext({});

const initUiState = {
  confirmModal: {
    method: null,
    open: false
  },
  snackbar: {
    msg: null,
    variant: null
  },
  gqlError: null
}

export const UI_REDUCER_TYPES = {
  OPEN_COMFIRM_MODAL: "OPEN_COMFIRM_MODAL",
  CLOSE_COMFIRM_MODAL: "CLOSE_COMFIRM_MODAL",
  TRIGGER_SNACK_BAR: "TRIGGER_SNACK_BAR",
  HANDLE_GQL_ERROR: "HANDLE_GQL_ERROR",
}

const uireducer = (state: any, action: any) => {
  switch (action.type) {
    case UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL:
      return { ...state, confirmModal: { open: action.payload.open, method: action.payload.method } }
    case UI_REDUCER_TYPES.CLOSE_COMFIRM_MODAL:
      return { ...state, confirmModal: { open: false, method: null } }
    case UI_REDUCER_TYPES.TRIGGER_SNACK_BAR:
      return { ...state, snackbar: action.payload }
    case UI_REDUCER_TYPES.HANDLE_GQL_ERROR:
      return { ...state, gqlError: action.payload }
    default:
      return state;
  }
}

const RootProvider = ({ children }: any) => {
  const [uiState, uidispatch] = useReducer(uireducer, initUiState);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (!!uiState.snackbar.msg) {
      enqueueSnackbar(uiState.snackbar.msg, { variant: uiState.snackbar.variant });
    }
  }, [uiState.snackbar]);

  useEffect(() => {
    console.log("RootProvider", uiState);
  }, [uiState.confirmModal]);

  useEffect(() => {
    const e = uiState.gqlError;
    console.log("Error ===> ", e);
    if (!!e && e.networkError) {
      enqueueSnackbar(String(e.networkError), { variant: "error" });
    }
  
    if (!!e && e.graphQLErrors) {
      e.graphQLErrors.map((error: any) => {
        enqueueSnackbar(error.message, { variant: "error" });
      });
    }
  }, [uiState.gqlError]);

  return (
    <>
      <RootContext.Provider value={{ uidispatch }}>
        {children}
      </RootContext.Provider>
      <ConfirmModal dispatch={uidispatch} open={uiState.confirmModal.open} action={uiState.confirmModal.method} />
    </>
  );
};

export default RootProvider;


  // const prevAuth = window.localStorage.getItem('authenticated') || false;
  // const prevUser = window.localStorage.getItem('user') || null;
  // const [authenticated, setAuthenticated] = useState(prevAuth);
  // const [user, setUser] = useState(prevUser);

  // const setConnectedStatus = () => {
  //   const { token } = user;
  //   window.localStorage.setItem('authenticated', authenticated);
  //   window.localStorage.setItem('user', user);
  //   window.localStorage.setItem('token', token);
  // }

  // const setDisconnectedStatus = () => {
  //   const { token } = user;
  //   window.localStorage.setItem('authenticated', authenticated);
  //   window.localStorage.setItem('user', user);
  //   window.localStorage.setItem('token', token);
  // }

  // useEffect(
  //   () => {
  //     if (authenticated) {
  //       setConnectedStatus();
  //     } else {
  //       setDisconnectedStatus();
  //     }
  //   },
  //   [authenticated, user]
  // );
  // const defaultContext = {
  //   authenticated,
  //   setAuthenticated,
  //   user,
  //   setUser
  // };