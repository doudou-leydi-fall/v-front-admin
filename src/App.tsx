import React, { Component, useContext } from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import UIController from "./screens/Root/Backdrop";
import { theme } from "./styles/theme";
import { Redirect, BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { client } from "./network";
import { ApolloProvider } from "@apollo/react-hooks";
import { Provider } from "react-redux";
import store from "./store";
import "jquery";
import "./App.css";
import SignInScreen from "./screens/Auth/SignIn";
import RootLayout from "./screens/RootLayout";
import RootProvider from "./RootContext";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <RootProvider>
        <Router>
          <Provider store={store}>
            <ApolloProvider client={client}>
              <Route path="/login">
                <SignInScreen />
              </Route>
              <Route component={RootLayout} />
              <UIController />
            </ApolloProvider>
          </Provider>
        </Router>
      </RootProvider>
    </ThemeProvider>
  );
}
// process.env.PUBLIC_URL
// const ProtectedRoute = ({ component: Component, ...routeProps }) => {
//   const { authenticated } = useContext(RootContext);
//   console.log("authenticated LOGGG", authenticated);

//   return (
//     <Route
//       {...routeProps}
//       render={props => (authenticated ?
//         <Component {...props} /> : <Redirect to='/login' />)
//       }
//     ></Route>
//   );
// };

export default App;
