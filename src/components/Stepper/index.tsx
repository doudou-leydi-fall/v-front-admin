import React, { createContext, useEffect } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import LinearProgress from '@material-ui/core/LinearProgress';

export const WizardContext = createContext({});
export const WizardProvider = WizardContext.Provider;
export const WizardConsumer = WizardContext.Consumer;

interface Step {
  label: string;
  component: any;
}

export default ({ steps = [], ...rest }: any) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [loading, setLoading]: any = React.useState(false);
  const [store, setStore]: any = React.useState({});

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const saveAndNext = (item: any) => {
    setStore({ ...store, ...item });
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }

  const addToStore = (item: any) => {
    setStore({ ...store, ...item });
  }

  useEffect(() => {
    console.log("useEffect store", store);
  }, [store]);

  return (
    <WizardProvider value={{ handleNext, handleBack, addToStore, store }}>
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((step: any, key: number) => (
            <Step key={key}>
              <StepLabel>{step.label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        {loading && <LinearProgress color="secondary" />}
        <div className={classes.stepComponentContainer}>{
          React.createElement(steps[activeStep].component, { setLoading, handleNext, handleBack, saveAndNext, addToStore, store, ...rest })}
        </div>
      </div>
    </WizardProvider>
  );
}


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    step: {
      height: 30,
      width: 30
    },
    stepComponentContainer: {
      padding: 20
    }
  }),
);