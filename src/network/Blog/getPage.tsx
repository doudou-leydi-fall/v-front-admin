import gql from "graphql-tag";

export const GET_PAGE = gql`
  query GetPage{
    getPage {
      name
      children {
        labelLeft
        labelRight
        sublabelLeft
        sublabelRight
        photo
      }
    }
  }
`;
