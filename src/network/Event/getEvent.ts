import gql from "graphql-tag";

export const GET_EVENT = gql`
  query GetEvent($id: String!) {
    getPlace(id: $id) {
      id
      name
      logo
      src
      phone
      email
      description
      categories
      location {
        address
        zone
        coordinates {
          lon
          lat
        }
        ocean
      }
      start
      temporaly
      pictures {
        small
      }
      CREATED_AT
      LAST_UPDATE_DATE
      LAST_UPDATE_USER
    }
  }
`;
