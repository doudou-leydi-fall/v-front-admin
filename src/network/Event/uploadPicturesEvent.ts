import gql from "graphql-tag";

export const UPLOAD_PICTURES_EVENT = gql`
  mutation UploadPicturesEvent($id: String!, $pictures: [Upload!]!) {
    uploadPicturesEvent(id: $id, pictures: $pictures)
  }
`;
