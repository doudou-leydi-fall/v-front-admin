import gql from "graphql-tag";

export const GROUP_BY_DATE_EVENT = gql`
  query GroupByDateEvent($criteria: [NameValueInput], $size: Int, $from: Int){
    groupByDateEvent(criteria: $criteria, size: $size, from: $from) {
      name
      value
    }
  }
`;
