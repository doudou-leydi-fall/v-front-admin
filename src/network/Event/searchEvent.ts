import gql from "graphql-tag";

export const SEARCH_EVENT = gql`
  query SearchEvent($criteria: [NameValueInput], $size: Int, $from: Int){
    searchEvent(criteria: $criteria, size: $size, from: $from) {
      total
      data {
        id
        name
        start
        description
        categories
        src
      }
    }
  }
`;
