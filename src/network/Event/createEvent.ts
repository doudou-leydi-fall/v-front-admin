import gql from "graphql-tag";

export const CREATE_EVENT = gql`
  mutation CreateEvent($event: EventInput!) {
    createEvent(event: $event)
  }
`;
