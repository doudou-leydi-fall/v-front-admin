import gql from "graphql-tag";

export const UPLOAD_PICTURES = gql`
  mutation UploadPictures($model: String!, $id: ID!, $pictures: [Upload!]!, $formatType: String!) {
    uploadPictures(model: $model, id: $id, pictures: $pictures, formatType: $formatType)
  }
`;
