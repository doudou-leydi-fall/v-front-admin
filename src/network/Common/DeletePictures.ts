import gql from "graphql-tag";

export const DELETE_PICTURES = gql`
  mutation DeletePictures($model: String!, $modelId: ID!, $picturesIds: [String!]!) {
    deletePictures(model: $model, modelId: $modelId, picturesIds: $picturesIds)
  }
`;
