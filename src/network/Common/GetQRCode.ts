import gql from "graphql-tag";

export const GET_QRCODE = {
  QUERY: gql`
    query GetQRCode($id: ID!) {
      getQRCode(id: $id) {
        id
        url
        datamodel
      }
    }`,
  OPERATION: "getQRCode"
}