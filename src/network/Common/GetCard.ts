import gql from "graphql-tag";

export const GET_CARD = {
  QUERY: gql`
    query GetCard($model: Model!, $id: ID!) {
      getCard(id: $id, model: $model) {
        id
        name
        cardType
        model
        ... on Product {
          price
          type
          pricing {
            id
            price
            name
            currency
            period
            discount
            quantity
            size
            marketprice
            main
            startDate
            endDate
          }
          sheet {
            id
            name
            value
            comment
          }
          currency
          recipes {
            id
            name
          }
          cardType
          src
          quantity,
          spicy
          src
          gastronomy
          category {
            name
            id
          }
          categoryCatalogue {
            name
            id
          }
          collectionCatalogue {
            name
            id
          }
          size
          discount
          stock
          description
          program {
            start
            end
          }
          pictures {
            id
            position
            format
            mobile
            desktop
            provider
            main
          }
          collection {
            id
            name
            cardType
          }
          seller {
            ...on Place {
              id
              name
              categories
              phone
              src
              email
              location {
                address
                zone
                country
                district
              }
              ranking {
                notation
                likes
              }
            }
          }
        }
      }
    }`,
  OPERATION: "getCard"
};
