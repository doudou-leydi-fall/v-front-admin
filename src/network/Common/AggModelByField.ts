import gql from "graphql-tag";

export const AGG_MODEL_BY_FIELD = gql`
  query AggModelByField($model: Model!, $criteria: [NameValueInput], $fieldname: String!) {
    aggModelByField(model: $model, criteria: $criteria, fieldname: $fieldname) {
      id
      name
      value
      label
    }
  }
`;
