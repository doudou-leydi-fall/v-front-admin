import gql from "graphql-tag";

export const UPLOAD_PICTURE = gql`
  mutation UploadPicture($model: String!, $id: ID!, $picture: Upload!, $field: String!) {
    uploadPicture(model: $model, id: $id, picture: $picture, field: $field)
  }
`;
