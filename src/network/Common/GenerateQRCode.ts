import gql from "graphql-tag";

export const GENERATE_QRCODE = {
  MUTATION: gql`
    mutation GenerateQRCode($model: Model!, $id: ID!) {
      generateQRCode(model: $model, id: $id)
    }
  `,
  OPERATION: "generateQRCode"
};