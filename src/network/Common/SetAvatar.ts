import gql from "graphql-tag";

export const SET_AVATAR = gql`
  mutation SetAvatar($model: Model!, $modelId: ID!, $pictureId: ID!) {
    setAvatar(model: $model, modelId: $modelId, pictureId: $pictureId)
  }
`;
