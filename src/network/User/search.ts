import gql from "graphql-tag";

export const SEARCH_USER = gql`
  query SearchUser($criteria: [NameValueInput], $size: Int, $from: Int) {
    searchUser(criteria: $criteria, size: $size, from: $from) {
      total
      data {
        id
        fullname
        firstname
        lastname
        artistname
        src
        roles
        email
        phone
      }
    }
  }
`;
