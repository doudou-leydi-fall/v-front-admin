import gql from "graphql-tag";

export const GET_USER_DETAIL = gql`
query GetUser($userid: String!) {
  getUser(userid: $userid){
    id
    fullname
    firstname
    lastname
    artistname
    gender
    birthdate
    email
    phone
    location {
      address
      country
      zone
    }
    tags
    roles
    description
    logo
    src
    pictures {
      id
      small
      big
    }
    facebook
    twitter
    CREATED_AT
    LAST_UPDATE_DATE
    LAST_UPDATE_USER
  }
}`