import gql from "graphql-tag";

export const REGISTER = gql`
  mutation Register($user: UserInput!) {
    register(user: $user)
  }
`;