import gql from "graphql-tag";

export const DELETE_USER = gql`
  mutation DeleteUser($userid: String!) {
    deleteUser(userid: $userid)
  }
`;