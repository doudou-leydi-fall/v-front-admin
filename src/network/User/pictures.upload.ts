import gql from "graphql-tag";

export const PROFILE_PICTURES_UPLOAD = gql`
  mutation AddPicturesUser($userid: String!, $pictures: [Upload!]!) {
    addPicturesUser(userid: $userid, pictures: $pictures)
  }
`;
