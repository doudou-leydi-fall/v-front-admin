import ApolloClient from "apollo-client";
import { InMemoryCache, IntrospectionFragmentMatcher } from "apollo-cache-inmemory";
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from "apollo-link-context";
import { API_HOST } from "../config";
import introspectionQueryResultData from './fragmentTypes.json';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});

console.log("process.env.API_HOST LOG", process.env.REACT_APP_API_HOST);
const cache = new InMemoryCache({ fragmentMatcher });
const link = createUploadLink({
  uri: process.env.REACT_APP_API_HOST,
  fetchOptions: () => {
    console.log('operation log');
  }
});

const authLink = setContext(async (_, { headers }) => {
  const token = await localStorage.getItem("token");
  console.log('token');
  console.log(token);
  return {
    headers: {
      ...headers,
      authorization: `Bearer ${token}`
    }
  };
});

export const client = new ApolloClient({
  link: authLink.concat(link),
  cache
});
