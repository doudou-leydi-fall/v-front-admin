import gql from "graphql-tag";

export const DELETE_ELEMENT = gql`
  mutation DeleteElement($ids:[ID!]!){
    deleteElement(ids: $ids)
  }
`;
