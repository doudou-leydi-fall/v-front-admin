import gql from "graphql-tag";

export const ADD_ELEMENT = gql`
  mutation AddElement($name: String!, $type: String!, $owner: String){
    addElement(name: $name, type: $type, owner: $owner)
  }
`;
