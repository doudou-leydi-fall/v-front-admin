import gql from "graphql-tag";

export const ADD_ROOT_CATEGORY = gql`
  mutation AddRootCategory($name: String!, $type: String!){
    addRootCategory(name: $name, type: $type)
  }
`;
