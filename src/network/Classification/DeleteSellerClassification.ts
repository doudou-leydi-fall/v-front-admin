import gql from "graphql-tag";

export const DELETE_SELLER_CLASSIFICATION = gql`
  mutation DeleteSellerClassification($ids: [ID!]!){
    deleteSellerClassification(ids: $ids)
  }
`;
