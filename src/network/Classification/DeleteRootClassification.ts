import gql from "graphql-tag";

export const DELETE_ROOT_CLASSIFICATION = gql`
  mutation DeleteRootClassification($ids: [ID!]!){
    deleteRootClassification(ids: $ids)
  }
`;
