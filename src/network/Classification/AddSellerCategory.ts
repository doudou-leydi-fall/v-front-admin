import gql from "graphql-tag";

export const ADD_SELLER_CATEGORY = gql`
  mutation AddSellerCategory($name: String!, $owner: String!, $rootCategory: I_Attribute!){
    addSellerCategory(name: $name, owner: $owner, rootCategory: $rootCategory)
  }
`;
