import gql from "graphql-tag";

export const ADD_SELLER_COLLECTION = gql`
  mutation AddSellerCollection($name: String!, $rootCollection: I_Attribute!, $categoryID: ID!, $owner: ID!){
    addSellerCollection(name: $name, rootCollection: $rootCollection, categoryID: $categoryID, owner: $owner)
  }
`;
