import gql from "graphql-tag";

export const ADD_ROOT_COLLECTION = gql`
  mutation AddRootCollection($name: String!, $categoryID: ID!, $cardType: CardType!){
    addRootCollection(name: $name, categoryID: $categoryID, cardType: $cardType)
  }
`;
