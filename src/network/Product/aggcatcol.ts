import gql from "graphql-tag";

export const PRODUCT_AGG_CATCOL = gql`
  query GetProductAgg($placeid: String!) {
    getProductAgg(placeid: $placeid) {
      name
      children {
        name
        value
      }
      value
    }
  }
`;
