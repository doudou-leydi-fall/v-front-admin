import gql from "graphql-tag";

export const CREATE_PRODUCT = {
  MUTATION: gql`
    mutation CreateProduct($product: ProductInput!) {
      createProduct(product: $product)
    }
  `,
  OPERATION: "createProduct"
};
