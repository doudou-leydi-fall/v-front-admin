import gql from "graphql-tag";

export const GET_PRODUCT = gql`
  query GetProduct($id: String!) {
    getProduct(id: $id) {
      id
      name
      ... on Product {
        price
        type
        pricing {
          id
          price
          name
          currency
          period
          discount
          quantity
          size
          marketprice
          startDate
          endDate
          main
        }
        sheet {
          id
          name
        }
        recipes {
          id
          name
        }
        cardType
        src
        quantity,
        spicy
        gastronomy
        category {
          name
          id
        }
        categoryCatalogue {
          name
          id
        }
        collectionCatalogue {
          name
          id
        }
        size
        discount
        stock
        description
        pictures {
          small
          big
        }
        collection {
          id
          name
        }
        seller {
          id
          name
          categories
          phone
          src
          email
          address
          zone
          country
          district
          ranking {
            notation
            likes
          }
        }
      }
    }
  }
`;
