import gql from "graphql-tag";

export const UPDATE_PRODUCT_CLASSIFICATION = {
  MUTATION: gql`
    mutation UpdateProductClassification($id: ID!, $categoryCatalogue: I_Attribute!, $collectionCatalogue: I_Attribute!) {
      updateProductClassification(id: $id, categoryCatalogue: $categoryCatalogue, collectionCatalogue: $collectionCatalogue)
    }
  `,
  OPERATION: "updateProductClassification"
};