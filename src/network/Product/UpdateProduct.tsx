import gql from "graphql-tag";

export const UPDATE_PRODUCT = {
  MUTATION: gql`
    mutation UpdateProduct($id: ID!, $sellerid: ID!, $product: ProductUpdateInput!) {
      updateProduct(id: $id, sellerid: $sellerid, product: $product) {
        catalogueUpdated
        productUpdated
      }
    }
  `,
  OPERATION: "updateProduct"
};