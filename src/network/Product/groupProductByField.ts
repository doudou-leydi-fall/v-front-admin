import gql from "graphql-tag";

export const GROUP_PRODUCT_BYFIELD = gql`
query groupProductByField($criteria: [NameValueInput], $field: String!) {
  groupProductByField(criteria: $criteria, field: $field) {
    name
    items {
      id
      name
      price {
        value
        currency
        period
        discount
        quantity
        size
        marketprice
        main
      }
      sheet {
        id
        name
      }
      currency
      src
      recipes {
        id
        name
      }
    }
  }
}
`;
