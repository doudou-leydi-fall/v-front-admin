import gql from "graphql-tag";

export const AGG_PRODUCT_NESTEDFIELD = gql`
  query AggProductByNestedField($criteria: [NameValueInput]!, $parentField: String!, $childField: String!) {
    aggProductByNestedField(criteria: $criteria, parentField: $parentField, childField: $childField) {
      name
      children {
        name
        value
      }
      value
    }
  }
`;
