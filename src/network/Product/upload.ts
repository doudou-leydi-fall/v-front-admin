import gql from "graphql-tag";

export const UPLOAD_PICTURES_PRODUCT = gql`
  mutation UploadPicturesPlace($productid: String!, $pictures: [Upload!]!, $formatType: String!) {
    uploadPicturesProduct(productid: $productid, pictures: $pictures, formatType: $formatType)
  }
`;
