import gql from "graphql-tag";

export const UPDATE_PRODUCT_PRICING = {
  MUTATION: gql`
    mutation UpdateProductPricing($id: ID!, $pricing: [I_Pricing!]!) {
      updateProductPricing(id: $id, pricing: $pricing)
    }
  `,
  OPERATION: "updateProductPricing"
};