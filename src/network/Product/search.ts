import gql from "graphql-tag";

export const SEARCH_PRODUCT = gql`
  query SearchProduct($criteria: [NameValueInput], $size: Int, $from: Int) {
    searchProduct(criteria: $criteria, size: $size, from: $from) {
      total
      data {
        id
        name
        price
        type
        pricing {
          name
          price
          currency
          period
          discount
          quantity
          size
          marketprice
          main
          startDate
          endDate
        }
        sheet {
          id
          name
        }
        collection {
          name
          id
        }
        recipes {
          id
          name
        }
        src
      }
    }
  }
`;
