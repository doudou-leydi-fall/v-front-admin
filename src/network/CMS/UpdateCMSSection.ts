import gql from "graphql-tag";

export const UPDATE_CMS_SECTION = gql`
  mutation UpdateCMSSection($section: UPT_CMSSection!){
    updateCMSSection(section: $section)
  }
`;
