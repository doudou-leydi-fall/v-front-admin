import gql from "graphql-tag";

export const GET_PAGE_MODEL = gql`
  query GetPageModel($criteria: [NameValueInput]){
    getPageModel(criteria: $criteria) {
      total
      data {
        id
        order
        header {
          label
        }
        sections {
          id
          order
          header {
            label
            sublabel
          }
          cardComponent {
            type
            redirection
          }
          fetchParams {
            operation
            query
            searchParams {
              criteria {
                name
                value
              }
              cmsId
            }
          }
          templateSearch {
            page
            query
            operation
            childrenComponent {
              type
              redirection
            }
            searchParams {
              index
              criteria {
                name
                props
                type
                value
                collection
              }
            }
          }
          sectionTheme {
            base
            backgroundColor
          }
        }
      }
    }
  }
`;
