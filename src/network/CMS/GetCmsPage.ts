import gql from "graphql-tag";

export const GET_CMS_PAGE = {
  QUERY: gql`
    query GetCmsPage($screen: CMSScreen!, $criteria: [NameValueInput], $size: Int!, $from: Int!){
      getCmsPage(screen: $screen, criteria: $criteria, size: $size, from: $from) {
        total
        data {
          id
          order
          header {
            label
            labelTheme
            labelFontSize
            labelFontFamily
            labelFontColor
            sublabel
            subLabelTheme
            sublabelFontSize
            sublabelFontFamily
            sublabelFontColor
            display
            logo
            enableFollowButton
            buttonColor
            buttonTextColor
          }
          footer {
            label
            sublabel
            logo
            description
            buttonLabel
            buttonColor
            buttonRedirection
            descriptionColor
          }
          backgroundColor
          sectionGroupTheme {
            primaryColor
            secondaryColor
            tertiaryColor
          }
          sections {
            ...on CMSSection {
              id
              order
              sectionType
              header {
                label
                sublabel
                labelColor 
                labelFontSize
                labelFontFamily
                sublabelFontColor
                sublabelFontSize
                labelTheme
                sublabelTheme
                logo
                buttonColor
                buttonTextColor
              }
              footer {
                label
                sublabel
                logo
                description
                buttonLabel
                buttonColor
                buttonRedirection
                descriptionColor
              }
              templateSearch {
                id
                name
              }
              cardType
              cardRedirection
              cardComponent {
                labelColor
                sublabelColor
              }
              sectionTheme
              datamodel
              model
              screen
              backgroundColor
              cards {
                id
                name
                cardType
                header {
                  label
                }
                ranking {
                  likes
                }
                ... on Product {
                  id
                  name
                  price
                  currency
                  model
                  type
                  recipes {
                    id
                    name
                  }
                  collection {
                    id
                    name
                  }
                  collectionCatalogue {
                    id
                    name
                  }
                  pricing {
                    name
                    price
                    currency
                    period
                    discount
                    quantity
                    size
                    marketprice
                    main
                  }
                  sheet {
                    id
                    name
                  }
                  seller {
                    ... on Place {
                      id
                      name
                      categoriesLabel
                      THEME {
                        primaryColor
                        secondaryColor
                        tertiaryColor
                      }
                      logo {
                        mobile
                      }
                      location {
                        address
                        zone
                        district
                        coordinates {
                          lon
                          lat
                        }
                        ocean
                        country
                      }
                    }
                  }
                  src
                  ranking {
                    likes
                    notation
                  }
                }
                ... on Place {
                  id
                  name
                  src
                  model
                  description
                  categoriesLabel
                  logo {
                    id
                    desktop
                    mobile
                  }
                  pictures {
                    position
                    format
                    mobile
                    desktop
                    provider
                    main
                  }
                  location {
                    address
                    zone
                    district
                    coordinates {
                      lon
                      lat
                    }
                    ocean
                    country
                  }
                  ranking {
                    likes
                    notation
                  }
                  THEME {
                    primaryColor
                    secondaryColor
                    tertiaryColor
                  }
                }
              }
            }
          }
        }
      }
    }
  `,
  OPERATION: "getCmsPage"
};
