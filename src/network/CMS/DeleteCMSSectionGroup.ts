import gql from "graphql-tag";

export const DELETE_CMS_SECTIONGROUP = {
  QUERY: gql`mutation DeleteCMSSectionGroup($id: ID!){
    deleteCMSSectionGroup(id: $id)
  }`,
  OPERATION: "deleteCMSSectionGroup"
};
