import gql from "graphql-tag";

export const ADD_CMS_SECTION_TO_GROUP = {
  QUERY: gql`
    mutation AddCMSSectionToGroup($section: I_CMSSection!){
      addCMSSectionToGroup(section: $section)
    }`,
  OPERATION: "addCMSSectionToGroup"
};
