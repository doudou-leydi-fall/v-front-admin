import gql from "graphql-tag";

export const ADD_CMS_SECTIONGROUP = {
  QUERY: gql`
    mutation AddCMSSectionGroup($name: String!, $screen: String!){
      addCMSSectionGroup(name: $name, screen: $screen)
    }`,
  OPERATION: "addCMSSectionGroup"
};
