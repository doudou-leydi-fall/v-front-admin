import gql from "graphql-tag";

export const GET_CMS_PLACES = gql`
  query GetCMSPlaces($id: String!){
    getCMSPlaces(id: $id) {
      id
      name
      categories
      gastronomies
      src
      logo
      phone
      location {
        address
        zone
        district
      }
      pictures {
        big
        small
      }
      ranking {
        notation
        likes
      }
      THEME {
        primaryColor
        secondaryColor
        tertiaryColor
      }
    }
  }
`;
