import gql from "graphql-tag";

export const GET_CMS_PRODUCTS = gql`
  query GetCMSProducts($id: String!){
    getCMSProducts(id: $id) {
      id
      name
      price
      recipes {
        id
        name
      }
      src
      pictures {
        small
      }
      collection {
        id
        name
      }
      seller {
        id
        name
        categories
        phone
        src
        email
        address
        zone
        country
        district
        logo
        ranking {
          notation
          likes
        }
      },
      ranking {
        notation
        likes
      }
    }
  }
`;
