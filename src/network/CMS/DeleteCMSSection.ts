import gql from "graphql-tag";

export const DELETE_CMS_SECTION = gql`
  mutation DeleteCMSSection($groupID: ID!, $sectionID: ID!){
    deleteCMSSection(groupID: $groupID, sectionID:$sectionID)
  }
`;
