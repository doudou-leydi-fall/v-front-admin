import gql from "graphql-tag";

export const METRIC_USER_ZONE = gql`
  query AggUserByZone {
    aggUserByZone {
      name
      value
    }
  }
`;
