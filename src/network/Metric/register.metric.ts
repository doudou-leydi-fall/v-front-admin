import gql from "graphql-tag";

export const METRIC_REGISTER = gql`
  query MetricRegister {
    metricRegister {
      name
      value
    }
  }
`;
