import gql from "graphql-tag";

export const SEARCH_LOCATIONS = gql`
  query SearchLocations($criteria: [NameValueInput], $size: Int, $from: Int){
    searchLocations(criteria: $criteria, size: $size, from: $from) {
      total
      data {
        name
      }
    }
  }
`;
