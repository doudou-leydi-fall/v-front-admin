import gql from "graphql-tag";

export const GET_LOCATIONS = gql`
  query GetLocations($size: Int, $criteria: [NameValueInput]){
    getLocations(size: $size, criteria: $criteria) {
      total
      data {
        id
        name
        collection
        label
        category
      }
    }
  }
`;
