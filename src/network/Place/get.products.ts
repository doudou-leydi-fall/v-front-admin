import gql from "graphql-tag";

export const SEARCH_PRODUCTS_PLACE = gql`
  query SearchProduct($params: SearchParamsInput!) {
    searchProduct(params: $params) {
      id
      name
      price
      collection
      recipes {
        id
        name
      }
    }
  }
`;
