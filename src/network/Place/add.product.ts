import gql from "graphql-tag";

export const ADD_PRODUCT = gql`
  mutation AddProductToPlace($placeid: String!, $product: ProductInput!) {
    addProductToPlace(placeid: $placeid, product: $product)
  }
`;
