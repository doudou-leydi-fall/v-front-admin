import gql from "graphql-tag";

export const UPLOAD_PICTURES_PLACE = gql`
  mutation UploadPicturesPlace($placeid: String!, $pictures: [Upload!]!) {
    uploadPicturesPlace(placeid: $placeid, pictures: $pictures)
  }
`;
