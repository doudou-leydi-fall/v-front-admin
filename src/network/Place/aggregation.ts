import gql from "graphql-tag";

export const AGG_PLACE_FIELD = gql`
  query AggPlaceByField($criteria: [NameValueInput], $fieldname: String!){
    aggPlaceByField(criteria: $criteria, fieldname: $fieldname) {
      name
      value
    }
  }
`;
