import gql from "graphql-tag";

export const GET_PLACE = gql`
  query GetPlace($id: String!) {
    getPlace(id: $id) {
      id
      name
      ... on Place {
        logo {
          id
          desktop
          mobile
        }
        src
        phone
        email
        qrcode {
          id
          mobile
        }
        description
        categories
        location {
          address
          zone
          district
          coordinates {
            lon
            lat
          }
          ocean
          country
        }
        gastronomies
        ranking {
          votes
          notation
          total
          likes
        }
        temporaly
        pictures {
          id
          mobile
          desktop
          main
        }
        THEME {
          primaryColor
          secondaryColor
          tertiaryColor
        }
        CREATED_AT
        LAST_UPDATE_DATE
        LAST_UPDATE_USER
      }
    }
  }
`;
