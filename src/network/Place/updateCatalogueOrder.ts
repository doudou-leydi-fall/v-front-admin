import gql from "graphql-tag";

export const UPDATE_CATALOGUE_ORDER = gql`
  mutation UpdateCatalogueOrder($sellerid: String!, $order: [AttributeInput!]!) {
    updateCatalogueOrder(sellerid: $sellerid, order: $order)
  }
`;