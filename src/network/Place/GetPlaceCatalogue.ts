import gql from "graphql-tag";

export const GET_PLACE_CATALOGUE = {
  QUERY: gql` query GetPlaceCatalogue($id: String!) {
    getPlaceCatalogue(id: $id) {
      id
      order
      header {
        label
      }
      sections {
        ... on CatalogueSection {
          id
          order
          total
          header {
            label
            sublabel
          }
          cardComponent {
            type
          }
          cards {
            id
            name
            ... on Product {
              pricing {
                price
                currency
                period
                discount
                quantity
                size
                marketprice
                main
              }
              sheet {
                id
                name
              }
              currency
              recipes {
                id
                name
              }
              src
              stock
              quantity
              size
              spicy
              price
              currency
              ranking {
                likes
                notation
                total
                votes
              }
            }
          }
        }
      }
    }
  }`,
  OPERATION: "getPlaceCatalogue"
};
