import gql from "graphql-tag";

export const UPDATE_PLACE = gql`
  mutation UpdatePlace($id: ID!, $place: PlaceUpdateInput!) {
    updatePlace(id: $id, place: $place)
  }
`;