import gql from "graphql-tag";

export const SEARCH_PLACE = gql`
  query SearchPlace($criteria: [NameValueInput], $size: Int, $from: Int){
    searchPlace(criteria: $criteria, size: $size, from: $from) {
      total
      data {
        id
        name
        categories
        src
        location {
          zone
          district
        }
      }
    }
  }
`;
