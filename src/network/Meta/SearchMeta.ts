import gql from "graphql-tag";

export const SEARCH_META = gql`
  query SearchMeta($model: Model!, $type: SearchType!, $q: String, $criteria: [NameValueInput]){
    searchMeta(model: $model, type: $type, q: $q, criteria: $criteria) {
      total
      data {
        id
        name
        model
        __typename
        ... on Category {
          id
          owner
          root {
            id
          }
        }
        ... on Collection {
          id
          cardType
          type
          model
          owner
        }
        ... on Classification {
          id
          cardType
          type
          model
          owner
        }
        ... on Element {
          id
          type
          model
          owner
        }
      }
    }
  }
`;
