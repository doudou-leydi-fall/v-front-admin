import gql from "graphql-tag";

export const DELETE_META = gql`
  mutation DeleteMeta($ids: [ID!]!){
    deleteMeta(ids: $ids)
  }
`;