import gql from "graphql-tag";

export const GET_META = gql`
  query GetMeta($model: Model!, $id: ID!){
    getMeta(model: $model, id: $id) {
      total
      data {
        id
        name
        ...on Meta {
          parentID
          label
          owner
          parentName
          childRequirement
          cardType
        }
      }
    }
  }
`;
