import gql from "graphql-tag";

export const ADD_META = gql`
  mutation AddMeta($meta: I_Meta!){
    addMeta(meta: $meta)
  }
`;
