import gql from "graphql-tag";

export const GET_META_LIST = gql`
  query GetMetaList($model: Model!, $parentID: ID!, $criteria: [NameValueInput]){
    getMetaList(model: $model, parentID: $parentID, criteria: $criteria) {
      total
      data {
        id
        name
        ...on Meta {
          parentID
          label
          owner
          attributes {
            name
            value
          }
          childRequirements {
            name
            valueType
          }
        }
      }
    }
  }
`;
