import gql from "graphql-tag";

export const SEARCH_LAYOUT = gql`
  query SearchLayout($start: String, $end: String){
    searchLayout(start: $start, end: $end) {
      name
      children {
        label
        operation
        params {
          criteria {
            name
            value
          }
          size
        }
      }
    }
  }
`;
