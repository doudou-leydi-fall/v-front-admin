import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import green from "@material-ui/core/colors/green";

export const theme = createMuiTheme({
  typography: {
    fontFamily: [
      "FuturaMdBT",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(",")
  },
  palette: {
    primary: {
      light: "#757ce8",
      main: "#4a148c",
      dark: "#300762",
      contrastText: "#fff"
    },
    secondary: {
      light: "#ff7961",
      main: "#F76D1E",
      dark: "#ba000d",
      contrastText: "#FFF"
    }
  },
  overrides: {
    MuiAccordion: {
      expanded: {
        border: "1px solid #FEFEFE"
      }
    },
    MuiAccordionDetails: {
      root: {
        display: "flex",
        flexDirection: "column"
      }
    },
    MuiAccordionSummary: {
      content: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        margin: 0
      }
    },
    MuiBackdrop: {

    },
    MuiFormControl: {
      root: {
        marginTop: 5,
        marginBottom: 5
      }
    },
    MuiInputBase: {
      input: {
        fontFamily: "Futura",
        fontWeight: 600,
      }
    },
    MuiStepper: {
      root: {
        padding: 10
      }
    },
    MuiAlert: {
      standardInfo: {
        
      }
    },
    MuiStepLabel: {
      alternativeLabel: {
        marginTop: "5px !important"
      },
      label: {
        fontWeight: 600
      }
    },
    MuiStepConnector: {
      alternativeLabel: {
        top: 21
      }
    },
    MuiTypography: {
      root: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      variantMapping: {
        p: 'p'
      },
      h1: {
        fontFamily: "Roboto",
        fontWeight: 600,
      },
      h2: {
        fontFamily: "Roboto",
        fontWeight: 600,
      },
      h3: {
        fontFamily: "Futura",
        color: "#300862",
        fontWeight: 600,
        fontSize: 25,
        textTransform: "capitalize"
      },
      h4: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      h5: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      h6: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      p: {
        fontSize: 14,
        fontWeight: 600,
        fontFamily: "Futura"
      },
      body1: {
        fontFamily: 'Futura',
        fontWeight: 600,
        fontSize: 12
      },
      body2: {
        fontFamily: 'Futura',
        fontWeight: 600,
        fontSize: 12
      }
    },
    MuiPaper: {
      root: {
        boxShadow: "none !important"
      }
    },
    MuiCard: {
      root: {
        backgroundColor: "#FFFFFF",
        boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03) !important",
        border: "1px solid #edf2f9",
        height: "100%",
        fontFamily: "Futura",
        fontWeight: 600,
      }
    },
    MuiDivider: {
      root: {
        backgroundColor: "#edf2f9",
        fontWeight: 600,
      }
    },
    MuiFormHelperText: {
      root: {
        color: "#FFFFFF",
        backgroundColor: "#F76D1C",
        fontWeight: 600,
        borderRadius: 4,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 3,
        paddingBottom: 3
      },
      contained: {
        paddingLeft: 10,
        paddingRight: 10,
        marginLeft: 0,
        marginRight: 0,
        borderRadius: 4,
      }
    },
    MuiCardHeader: {
      root: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        paddingBottom: 10,
        fontWeight: 600,
        textAlign: "center"
      },
      title: {
        fontSize: 14
      }
    },
    MuiCardContent: {
      root: {
        minHeight: 115
      }
    },
    MuiBox: {
      root: {
        backgroundColor: "transparent",
        padding: 24
      }
    },
    MuiButton: {
      label: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      primary: {
        color: "#FFF",
        fontFamily: "Futura",
        fontWeight: 600,
        boxShadow: "0 0.25rem 0.55rem rgba(248,50,69,.35)",
        '& :hover': {
          backgroundColor: "transparent"
        }
      },
      secondary: {
        color: "#FFF",
        fontFamily: "Futura",
        fontWeight: 600,
        boxShadow: "0 0.25rem 0.55rem rgba(248,50,69,.35)",
        '& :hover': {
          backgroundColor: "transparent"
        }
      }
    },
    MuiList: {
      root: {
        //boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03) !important",
        border: "none"
      },
      padding: {
        paddingBottom: 0,
        paddingTop: 0,
      }
    },
    MuiListItem: {
      container: {
        backgroundColor: "#F0F0F0",
        fontFamily: "Futura",
        fontWeight: 600,
      }
    },
    MuiListSubheader: {
      root: {
        backgroundColor: "#E7E7E7 !important"
      }
    },
    MuiListItemText: {
      root: {
        fontFamily: "Futura",
        fontWeight: 600,
      },
      primary: {
        color: "#73706F",
        fontWeight: 600,
        fontSize: 13,
        fontFamily: "Futura"
      },
      secondary: {
        color: "#000000",
        fontWeight: 500,
        fontFamily: "Oswald",
        fontSize: 14
      },
      menu: {
        color: "#000000",
        fontWeight: 500,
        fontSize: "15px"
      }
    },
    MuiAvatar: {
      root: {
        height: 30,
        width: 30
      }
    },
    MuiToolbar: {
      regular: {
        minHeight: 40
      }
    },
    MuiSvgIcon: {
      root: {
        height: "0.9em",
        width: "0.9em"
      }
    },
    MuiInputBase: {
      root: {
        fontSize: 17,
        fontWeight: "500"
      }
    },
    MuiInputLabel: {
      root: {
        fontFamily: 'Futura',
        fontWeight: 600
      }
    },
    MuiStepIcon: {
      text: {
        fontSize: 12,
        fontFamily: "Futura"
      }
    },
    MuiTabs: {
      root: {
        height: 47,
        fontFamily: 'Futura',
        fontWeight: 700
      },
      vertical: {
        height: "100%",
        borderRight: "none !important"
      }
    },
    MuiTab: {
      wrapper: {
        fontFamily: 'Futura',
        fontSize: 17,
        color: "#000000"
      }
    },
    MuiTableCell: {
      root: {
        padding: "3px 5px",
        fontFamily: 'Futura',
        fontSize: 13,
        fontWeight: 600
      },
      head: {
        fontWeight: "700"
      },
    },
    MuiTablePagination: {
      root: {
        width: "100%"
      }
    },
    MuiChip: {
      root: {
        color: "#fff",
        backgroundColor: "#4A158B",
        borderRadius: 4,
        fontFamily: "Futura",
        fontWeight: 600,
      },
      deleteIcon: {
        color: "#FFF"
      }
    }
  },
  status: {
    danger: "orange"
  }
});
