const COLORS:any = {
  primary: "#4a148c",
  secondary: "#F76D1E",

  GREEN_CYAN: "#2EBCB1",
  ORANGE: "#F76D1E",
  BLUE_INDIGO: "#052882",
  PURPLE: "#4a148c"
}

export default COLORS;