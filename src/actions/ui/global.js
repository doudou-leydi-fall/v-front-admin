import { SET_BACKDROP } from "../../constants/action-types";

export const setBackdrop = open => ({ type: SET_BACKDROP, payload: open });