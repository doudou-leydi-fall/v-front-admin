import { 
  SET_PLACEDETAILS_DISPLAYMODE
} from "../../constants/action-types";

export const setDisplayMode = displayMode => ({ type: SET_PLACEDETAILS_DISPLAYMODE, payload: displayMode });