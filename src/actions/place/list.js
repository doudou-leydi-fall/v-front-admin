import { 
  SET_PLACE_CATEGORIES, 
  SET_PLACELIST_SIZE, 
  SET_PLACELIST_FROM,
  SET_PLACELIST_PAGER,
  SET_PLACELIST_DISTRICT
} from "../../constants/action-types";

export const setCategories = categories => ({ type: SET_PLACE_CATEGORIES, payload: categories });
export const setSize = size => ({ type: SET_PLACELIST_SIZE, payload: size });
export const setOffset = from => ({ type: SET_PLACELIST_FROM, payload: from });
export const setPager = pager => ({ type: SET_PLACELIST_PAGER, payload: pager });
export const setDistrict = district => ({ type: SET_PLACELIST_DISTRICT, payload: district });