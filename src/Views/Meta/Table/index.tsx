import React, { createContext, Fragment, useContext, useEffect, useReducer, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from "@apollo/react-hooks";
import { AppBar, Toolbar, Button, Icon, Typography, Box } from '@material-ui/core';
import TableView from "./Table";
import TableForm from './Form';
import { RootContext, UI_REDUCER_TYPES } from '../../../RootContext';
import { TableContext } from '..';

export const HighContext = createContext({});
const HighProvider = HighContext.Provider;

export const HIGH_TYPES: any = {
  SET_DISPLAY_MODE: "SET_DISPLAY_MODE"
}

const initState = {
  displayMode: {
    mode: 0,
    formType: null,
    refresh: false,
  },
  triggerRefetch: false
}

const reducers = (state: any, action: any) => {
  switch (action.type) {
    case HIGH_TYPES.SET_DISPLAY_MODE:
      return { ...state, displayMode: action.payload };
    default:
      return state;
  }
}

const TableLayout = ({ refetchRoot, tabItem }: any) => {
  //setModel(item.name)
  const [hightState, dispatchHigh] = useReducer(reducers, initState);
  const [selectedItems, setSelectedItems] = useState([]);
  const { HighLevelParams, owner } = useContext(TableContext);
  const { fetch: { getParams, operation, query }, delete: { mutation, success, operation: deleteOperation, deleteParams, variable_name }, entity } = HighLevelParams;
  const { uidispatch } = useContext(RootContext);
  const classes = useStyles();
  const { id, childRequirements } = tabItem;
  const { data, refetch, loading: tableLoading } = useQuery(query, getParams({ id, owner }));
  const [deleteApply, { loading, error }] = useMutation(mutation);
  const onDelete = async (ids: string[], applyRefetch: Function) => {
    try {
      const { data } = await deleteApply(deleteParams(ids));

      if (data[deleteOperation]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: success,
            variant: "success"
          }
        });

        applyRefetch();
        setSelectedItems([]);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  useEffect(() => {
    if (id || hightState.displayMode.refresh) {
      refetch();
    }
  }, [id, hightState.displayMode]);

  const renderTemplate = (displayMode: number) => {
    switch (displayMode) {
      case 0:
        return <TableView loading={tableLoading} list={data && data[operation]} onSelected={setSelectedItems} />;
      case 1:
        return <TableForm parentID={id} childRequirements={childRequirements}/>;
    }
  }

  useEffect(() => {
    refetch();
  }, [tabItem]);

  return (
    <HighProvider value={{ hightState, HIGH_TYPES, dispatchHigh, tableLoading, selectedItems }}>
      <Box className={classes.container}>
        <AppBar className={classes.appbar} position="static" color="default">
          <Toolbar className={classes.toolbar}>
            {tabItem ? <Typography variant="h5">{tabItem.name}</Typography> : null}
            <div>
              <Button
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>delete_outlined</Icon>}
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => onDelete(id, refetchRoot) }
                })}
              >Tout Supprimer</Button>
              <Button
                disabled={!(selectedItems.length > 0)}
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>delete_outlined</Icon>}
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => onDelete(selectedItems, refetch) }
                })}
              >Supprimer la selection</Button>
              <Button
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>add_variant</Icon>}
                onClick={() => dispatchHigh({ type: HIGH_TYPES.SET_DISPLAY_MODE, payload: { mode: 1 } })}
              >Ajouter</Button>
            </div>
          </Toolbar>
        </AppBar>
        {renderTemplate(hightState.displayMode.mode)}
      </Box>
    </HighProvider>
  );
}

const useStyles = makeStyles({
  tableContainer: {
    padding: "15px 15px 27px",
    marginBottom: 15,
  },
  appbar: {
    width: "auto",
    marginBottom: 20
  },
  container: {
    height: "100%"
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45,
    display: "flex",
    justifyContent: "space-between"
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  },
  alert: {

  },
  toolbarContainer: {
    marginBottom: 20
  },
});
export default TableLayout;
