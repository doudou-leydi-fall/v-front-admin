import { Button, Grid, makeStyles } from "@material-ui/core";
import * as Yup from 'yup';
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import { TextField } from "../../../screens/components";
import { TableContext } from "..";
import { HighContext, HIGH_TYPES } from "./index";
import RequiredFields from "../../../screens/components/Forms/FormFields/RequiredFields";
import FieldsRequiredSelect from "../../../screens/components/Forms/FormFields/MetaFields/FieldsRequiredSelect";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";

interface TableFormProps {
  parentID: string;
  childRequirements: string
}

const TableForm = ({ parentID }: TableFormProps) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const { HighLevelParams, owner } = useContext(TableContext);
  const { dispatchHigh, selectedItems }: any = useContext(HighContext);
  const { mutation, operation, variable_name } = HighLevelParams.create;
  const [applyMutation, { loading, error }]: any = useMutation(mutation);

  const handleSubmit = async (values: any, actions: any) => {
    try {
      let variables: any = {};
      variables[variable_name] = { ...values, owner };
      const { data } = await applyMutation({ variables })

      if (data[operation]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });

        dispatchHigh({ type: HIGH_TYPES.SET_DISPLAY_MODE, payload: { mode: 0, refresh: true } })
      }

      console.log("result LOG", data);
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <Grid container spacing={1} className={classes.root}>
          <Grid item sm={4}>
            <FieldBuilder
              name="catcol"
              fieldname="CAT_COL"
              value={{
                category: props.values && props.values.category,
                collection: props.values && props.values.collection
              }}
              errors={props.errors}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={4}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
          </Grid>
          <Grid item sm={4}>
            <TextField
              label="Label"
              name="label"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.label}
              error={props.errors.label}
            />
          </Grid>
          <Grid item sm={4}>
            <RequiredFields
              fields={selectedItems && selectedItems["childRequirements"]}
              onBlur={props.onBlur}
              setFieldValue={props.setFieldValue}
              values={props.values}
              errors={props.errors}
            />
          </Grid>
          <Grid item sm={12}>
            <FieldsRequiredSelect
              label="Champs enfant"
              name="childRequirements"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              setFieldValue={props.setFieldValue}
              value={props.values.childRequirements}
              error={props.errors.childRequirements}
            />
          </Grid>
          <Grid item sm={12}>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => dispatchHigh({ type: HIGH_TYPES.SET_DISPLAY_MODE, payload: { mode: 0 } })} color="primary">Annuler</Button>
            <Button
              className={classes.button}
              variant="contained"
              type="submit"
              color="primary">
              Creer
      </Button>
          </Grid>
        </Grid>
      </form>
    );
  }

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    label: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
  });

  return (
    <Formik
      initialValues={{
        parentID,
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});

export default TableForm;