import React, { useState } from 'react';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Checkbox from '@material-ui/core/Checkbox';
import EmbeddedTable from '../TableEmbedded';
import { TYPES } from './Table';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
    width: "100%"
  },
  firstCell: {
    display: "flex"
  },
  openButton: {
    height: 40,
    width: 40
  },
  collapse: {
  }
});

export default ({ row = [], selected, dispatch }: any) => {
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell className={classes.firstCell}>
          <Checkbox
            inputProps={{ 'aria-label': 'select all desserts' }}
            checked={selected}
            onChange={(e, checked) => dispatch({
              type: TYPES.SELECTED_ITEMS,
              payload: {
                id: row.id,
                checked
              }
            })}
          />
          <IconButton className={classes.openButton} aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>

        <TableCell align="left">{row.id}</TableCell>
        <TableCell align="left">{row.parentID}</TableCell>
        <TableCell align="left">{row.parentName}</TableCell>
        <TableCell align="left">{row.name}</TableCell>
        <TableCell align="left">{row.label}</TableCell>
        <TableCell align="left">{row.childRequirement}</TableCell>
        <TableCell align="left">{row.owner}</TableCell>
        {/* <TableCell align="right">{row.carbs}</TableCell>
        <TableCell align="right">{row.protein}</TableCell> */}
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingTop: 0 }} colSpan={8}>
          <Collapse className={classes.collapse} in={open} timeout="auto" unmountOnExit>
            {open && <EmbeddedTable parent={row} selected={selected}/>}
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}