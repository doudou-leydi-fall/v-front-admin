export interface IMeta {
  id: string;
  name: string;
}