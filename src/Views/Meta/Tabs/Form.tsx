import React, { useContext } from "react";
import { Formik } from "formik";
import {
  Grid,
  Button
} from "@material-ui/core";
import * as Yup from 'yup';
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "@apollo/react-hooks";
import { TextField } from "../../../screens/components";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import { TableContext } from "../index";
import FieldsRequiredSelect from "../../../screens/components/Forms/FormFields/MetaFields/FieldsRequiredSelect";

export default ({ tabItem, owner }:any) => {
  const classes = useStyles();
  const { SupremeLevelParams } = useContext(TableContext);
  const { entity, create: { mutation, operation, variable_name } } = SupremeLevelParams;
  const { uidispatch } = useContext(RootContext);
  const { dispatchRoot, refetchRoot, ROOT_TYPES } = useContext(TableContext);
  const [applyMutation, { loading, error }] = useMutation(mutation);
  console.log("tabItem LOG", tabItem);
  const handleSubmit = async (values: any, actions: any) => {
    try {
      let variables: any = {};
      variables[variable_name] = { ...values, owner };

      const { data } = await applyMutation({ variables })

      if (data[operation]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: `${entity} Ajoutées`,
            variant: "success"
          }
        });
        dispatchRoot({ type: ROOT_TYPES.SET_DISPLAY_MODE, payload: { mode: 0, refresh: true } });
      }

    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <Grid container spacing={1} className={classes.root}>
        <Grid item sm={4}>
          <TextField
            label="Nom"
            name="name"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.name}
            error={props.errors.name}
          />
        </Grid>
        <Grid item sm={4}>
          <TextField
            label="Label"
            name="label"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.label}
            error={props.errors.label}
          />
        </Grid>
        <Grid item sm={12}>
          <FieldsRequiredSelect
            label="Champs enfant"
            name="childRequirements"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            setFieldValue={props.setFieldValue}
            value={props.values.childRequirements}
            error={props.errors.childRequirements}
          />
        </Grid>
        <Grid item sm={12}>
          <Button
            className={classes.button}
            variant="contained"
            onClick={() => dispatchRoot({ type: ROOT_TYPES.SET_DISPLAY_MODE, payload: { mode: 0 } })}
          >Annuler
            </Button>
          <Button
            className={classes.button}
            variant="contained"
            type="submit"
            color="primary">
            Creer
      </Button>
        </Grid>
      </Grid>
    </form>
  );

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    label: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
  });

  return (
    <Formik
      initialValues={{}}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});

