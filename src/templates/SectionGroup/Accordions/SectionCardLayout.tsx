import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Skeleton from '@material-ui/lab/Skeleton';
import Chip from '@material-ui/core/Chip';
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Alert from "@material-ui/lab/Alert";

interface CardProps { 
  name: string
  content?:any 
  src: any
  imageBackgroundSize?: string
  header?: any
  footer?: any
  chipTitle?: string
  chipFooterLeft?: string
  chipFooterRight?: string
}

export default ({ setDisplayMode, refetch, cards=[] }: any) => {
  const classes = containerStyles();

  return (
    <div>
      {(cards.length > 1) ? <div className={classes.cardsContainer}> {cards.map((card:any) => <SectionCard key={card.id} {...card}/>)} </div> : <Alert className={classes.alert} severity="info">Pas de contenu</Alert>}
    </div>
  );
}

const SectionCard = ({ name, content, src, imageBackgroundSize="contain", header, footer, chipTitle, chipFooterLeft, chipFooterRight }: CardProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      {src ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={src}
        title="Image title"
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}>

        </Skeleton>
      }
      <CardContent className={classes.cardContent}>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h2">{name}</Typography>
        </div>
        {chipTitle && <Chip size="small" label={chipTitle} color="secondary"/>}
        <Typography className={classes.description} variant="subtitle2" component="p">{content}</Typography>
        <div className={classes.footer}>
          {chipFooterLeft && <Chip className={classes.footerChip} size="small" label={chipFooterLeft} />}
          {chipFooterRight && <Chip className={classes.footerChip} size="small" label={chipFooterRight} color="secondary"/>}
        </div>
      </CardContent>
    </Card>
  )
};

const containerStyles = makeStyles(() => ({
  alert: {
    marginBottom: 10
  },
  cardsContainer: {
    backgroundColor: "#f8f9fa !important",
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    paddingLeft: 5
  },
}));

const useStyles = makeStyles((theme) => ({
  card: {
    width: 250,
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "center"
  },
  titleContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    overflow: "hidden"
  },
  footer: {
    display: "flex"
  },
  footerChip: {
    marginTop: 10,
    marginRight: 3
  },
  description: {
    maxHeight: 40,
    overflow: "hidden"
  },
}));