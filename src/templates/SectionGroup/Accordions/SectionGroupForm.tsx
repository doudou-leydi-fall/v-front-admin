import React, { useContext } from "react";
import { Formik } from "formik";
import * as Yup from 'yup';
import { useMutation } from "react-apollo";
import { Accordion, AccordionDetails, AccordionSummary, Button, Chip, Grid, makeStyles, Typography } from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';
import Icon from '@material-ui/core/Icon';
import { TextField } from "../../../screens/components";
import ColorField from "../../../screens/components/CustomForms/ColorField";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { ADD_CMS_SECTIONGROUP } from "../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";

export default ({ id, setDisplayMode, refetch }: any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const [applyMutation]: any = useMutation(ADD_CMS_SECTIONGROUP.QUERY);

  const renderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <TextField
        name="name"
        label="Nom"
        value={props.values["name"]}
        errors={props.errors["name"]}
        onChange={props.handleChange}
      />
      <FieldBuilder
        name="screen"
        fieldname="SCREEN"
        value={props.values["screen"]}
        errors={props.errors["screen"]}
        setFieldValue={props.setFieldValue}
      />
      <ButtonLoading loading={false} type="submit" label="Creer" />
    </form>
  );

  const handleSubmit = async (values: any) => {
    try {
      const { data } = await applyMutation({ variables: values })

      if (data[ADD_CMS_SECTIONGROUP.OPERATION]) {
        refetch();
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });

        setDisplayMode(0);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    screen: Yup.string()
      .required('Champ obligatoire')
  });

  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<Icon color="secondary" style={{ fontSize: 30 }}>add_circle</Icon>}
        id="add-accordion"
        className={classes.header}
      >
        <div style={{ position: "absolute", left: 0, right: 0, top: 0, bottom: 0 }} onClick={(e: any) => e.stopPropagation()}>

        </div>
      </AccordionSummary>
      <AccordionDetails className={classes.accordionContainer}>
        <Formik
          render={renderForm}
          initialValues={{}}
          onSubmit={handleSubmit}
          validationSchema={ValidationSchema}
        />
      </AccordionDetails>
    </Accordion>
  );
}

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    labelFontColor: "#211337"
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%"
  },
  heading: {

  },
  accordionContainer: {
    display: "flex",
    flexDirection: "column"
  },
});