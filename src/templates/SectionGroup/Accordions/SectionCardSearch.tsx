import React from "react";
import Button from "@material-ui/core/Button";
import SectionCardLayout from "./SectionCardLayout";

export default ({ setDisplayMode, refetch, cards=[] }: any) => (
    <div>
      <SectionCardLayout cards={cards} refetch={refetch}/>
      <Button onClick={() => setDisplayMode(0)} style={{ marginLeft: 3, marginRight: 3 }} variant="contained" size="small" color="primary">fermer</Button>
    </div>
  );
