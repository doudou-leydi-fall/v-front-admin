import React, { Fragment, useState, useContext } from "react";
import { Accordion, AccordionSummary, FormControlLabel, Checkbox, AccordionDetails, Typography, makeStyles, Chip, AppBar, Button, IconButton, Toolbar } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import Section from "./Section";
import Form from "./SectionForm";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { useMutation } from "react-apollo";
import { DELETE_CMS_SECTIONGROUP } from "../../../network";

export default ({ index, id, header, order, sections = [], refetch }: any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const [displayMode, setDisplayMode]: any = useState(0);
  const [isExpanded, setExpanded]: any = useState(false);
  const [applyMutation]: any = useMutation(DELETE_CMS_SECTIONGROUP.QUERY);

  const renderContent = (mode: any) => {
    switch (mode) {
      case 0:
        return sections.map((section: any, key: number) => <Section refetch={refetch} groupID={id} key={key} section={section}/>)
      case 1:
        return <Form id={id} setDisplayMode={setDisplayMode} refetch={refetch} />
      default:
        return null
    }
  }

  const handleDelete = async (e: any) => {
    try {
      const { data } = await applyMutation({ variables: { id } })

      if (data[DELETE_CMS_SECTIONGROUP.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });
        refetch();
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  return (
    <Accordion onChange={(e: object, expanded: boolean) => setExpanded(expanded)}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        id={index}
        className={classes.header}
      >
        <Typography variant="h5" className={classes.heading}>{header && header.label}</Typography>
        <div className={classes.headerBtnGroup} onClick={(e:any) => e.stopPropagation()}>
          {isExpanded && 
          <div> 
            <Button onClick={() => setDisplayMode(1)} style={{ marginLeft: 3, marginRight: 3 }} variant="contained" size="small" color="primary">Ajouter</Button>
            <Button onClick={handleDelete} style={{ marginLeft: 3, marginRight: 3 }} variant="contained" size="small" color="primary">Supprimer ce group</Button>
          </div>}
          <Button variant="contained" size="small" color="primary" disabled={true}>Position: {order}</Button>
        </div>
      </AccordionSummary>
      <AccordionDetails className={classes.accordionContainer}>
        {isExpanded && <Fragment>
          {renderContent(displayMode)}
        </Fragment>}
      </AccordionDetails>
    </Accordion>
  )
}

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  accordionContainer: {
    display: "flex",
    flexDirection: "column"
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%"
  },
  headerBtnGroup: {
    display: "flex"
  },
  heading: {
    fontSize: 28,
  },
  menuButton: {
    marginRight: 10,
  },
  title: {
    flexGrow: 1,
  },
  appbar: {
    borderRadius: 5,
    marginBottom: 20
  }
});