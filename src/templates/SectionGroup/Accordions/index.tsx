import React, { Fragment, useState } from "react";
import { AppBar, Button, IconButton, makeStyles, Toolbar, Typography } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import SectionGroup from "./SectionGroup";
import Form from "./SectionForm";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import SectionGroupForm from "./SectionGroupForm";

export default ({ sectionGroup = [], loading, refetch }: any) => {
  const classes = useStyles();
  const [displayMode, setDisplayMode]: any = useState(0);

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>Section Group</Typography>
          <ButtonLoading loading={loading} onClick={() => refetch()} label="Actualiser" color="inherit" />
        </Toolbar>
      </AppBar>
      <SectionGroupForm refetch={refetch}/>
      <SectionGroupList sectionGroup={sectionGroup} refetch={refetch} />
    </div>
  )
}

const SectionGroupList = ({ sectionGroup = [], refetch }: any) => (
  <Fragment>
    { sectionGroup.sort((x: any, y: any) => x.order - y.order).map((section: any, key: number) => <SectionGroup id={section.id} index={key} total={section.total} order={section.order} header={section.header} sections={section.sections} refetch={refetch} />)}
  </Fragment>
)

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  menuButton: {
    marginRight: 10,
  },
  title: {
    flexGrow: 1,
  },
});