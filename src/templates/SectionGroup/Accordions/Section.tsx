import React, { useState, useContext, Fragment } from "react";
import { useMutation } from "react-apollo";
import { Accordion, AccordionSummary, AccordionDetails, Typography, makeStyles, Chip, AppBar, IconButton, Toolbar, Button } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import SectionUpdateForm from "./SectionUpdateForm";
import { DELETE_CMS_SECTION } from "../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import SectionCardSearch from "./SectionCardSearch";
import SectionCardLayout from "./SectionCardLayout";

export default ({ section, groupID, refetch }: any) => {
  const { uidispatch } = useContext(RootContext);
  const classes = useStyles();
  const [displayMode, setDisplayMode] = useState(0);
  const [isExpanded, setExpanded]: any = useState(false);
  const { id, header, order, cards } = section;
  const [applyMutation, { loading:deleting, error }]: any = useMutation(DELETE_CMS_SECTION);
  console.log("cards LOG", cards);
  const handleDelete = async (groupID:number, sectionID:number) => {
    try {
      const { data } = await applyMutation({ variables: { groupID, sectionID } })

      if (data["deleteCMSSection"]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });
        refetch();
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  const renderContent = (mode:number) => {
    switch(mode) {
      case 0:
        return (<div>
          <SectionUpdateForm initialValues={{ ...section, sectionGroupID: groupID }} />
          <SectionCardLayout cards={cards} refetch={refetch}/>
          </div>)
      case 1:
        return <SectionCardSearch id={id} setDisplayMode={setDisplayMode} refetch={refetch} cards={cards}/>
      default:
        return null;
    }
  }

  return (
    <Accordion className={classes.root} onChange={(e: object, expanded: boolean) => setExpanded(expanded)}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        id={id}
      >
        <Typography className={classes.heading}>{header && header.label}</Typography>
        <Chip color="secondary" label={`Position: ${order}`} />
      </AccordionSummary>
      <AccordionDetails className={classes.accordionContainer}>
        {isExpanded && <Fragment>
          <AppBar color="default" className={classes.appbar} position="static">
            <Toolbar variant="dense">
              <ButtonLoading onClick={() => handleDelete(groupID, section.id)} className={classes.menuButton} label="Supprimer section"/>
              <Button type="submit" color="inherit">Ajouter carte</Button>
              <Button type="submit" color="inherit" onClick={() => setDisplayMode(1)}>Voir le contenu</Button>
            </Toolbar>
          </AppBar>
          { renderContent(displayMode) }
        </Fragment>}
      </AccordionDetails>
    </Accordion>
  )
}

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  appbar: {
    borderRadius: 5,
    marginBottom: 20
  },
  menuButton: {
    marginRight: 10,
  },
  title: {
    flexGrow: 1,
  },
  accordionContainer: {

  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%"
  },
  heading: {
    fontSize: 20,
  },
});