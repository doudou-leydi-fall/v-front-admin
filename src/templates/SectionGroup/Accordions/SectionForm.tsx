import React, { useContext } from "react";
import { Formik } from "formik";
import * as Yup from 'yup';
import { useMutation } from "react-apollo";
import { Button, Grid, makeStyles } from "@material-ui/core";
import { TextField } from "../../../screens/components";
import ColorField from "../../../screens/components/CustomForms/ColorField";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { ADD_CMS_SECTION_TO_GROUP } from "../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";

export default ({ id, setDisplayMode, refetch }: any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const [applyMutation]: any = useMutation(ADD_CMS_SECTION_TO_GROUP.QUERY);

  const renderSubmit = () => (
    <div>
      <Button onClick={() => setDisplayMode(0)} color="primary">Annuler</Button>
      <ButtonLoading loading={false} type="submit" label="Creer" />
    </div>
  );

  const renderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <Grid item sm={3}>
        <TextField
          name="id"
          label="Section ID"
          disabled={true}
          value={props.values["id"]}
          errors={props.errors["id"]}
        />
      </Grid>
      <Grid item sm={3}>
        <TextField
          name="sectionGroupID"
          label="sectionGroup ID"
          disabled={true}
          value={props.values["sectionGroupID"]}
          errors={props.errors["sectionGroupID"]}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          name="screen"
          fieldname="SCREEN"
          value={props.values["screen"]}
          errors={props.errors["screen"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          name="sectionTheme"
          fieldname="SECTION_THEME"
          value={props.values["sectionTheme"]}
          errors={props.errors["sectionTheme"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          name="templateSearch"
          fieldname="TEMPLATE_SEARCH"
          value={props.values["templateSearch"]}
          errors={props.errors["templateSearch"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <ColorField
          label="Background Color"
          name="backgroundColor"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.backgroundColor}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          name="datamodel"
          fieldname="SECTION_DATA_MODEL"
          value={props.values["datamodel"]}
          errors={props.errors["datamodel"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Type de carte"
          name="cardType"
          fieldname="CARD_TYPE"
          value={props.values["cardType"]}
          errors={props.errors["cardType"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Redirection des cartes"
          name="cardRedirection"
          fieldname="CARD_REDIRECTION"
          value={props.values["cardRedirection"]}
          errors={props.errors["cardRedirection"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>

      <Grid item sm={3}>
        <TextField
          label="Titre"
          name="label"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.label}
          error={props.errors.label}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Taille de police"
          name="labelFontSize"
          fieldname="FONT_SIZE"
          value={props.values["labelFontSize"]}
          errors={props.errors["labelFontSize"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <ColorField
          label="Couleur titre"
          name="labelFontColor"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values["labelFontColor"]}
          error={props.errors["labelFontColor"]}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Theme titre"
          name="labelTheme"
          fieldname="LABEL_THEME"
          value={props.values["labelTheme"]}
          errors={props.errors["labelTheme"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <TextField
          label="Sous titre"
          name="sublabel"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.sublabel}
          error={props.errors.sublabel}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Taille Police sous titre"
          name="sublabelFontSize"
          fieldname="FONT_SIZE"
          value={props.values["sublabelFontSize"]}
          errors={props.errors["sublabelFontSize"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid item sm={3}>
        <ColorField
          label="Couleur sous-titre"
          name="sublabelFontColor"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values["sublabelFontColor"]}
          error={props.errors["sublabelFontColor"]}
        />
      </Grid>
      <Grid item sm={3}>
        <FieldBuilder
          label="Theme Sous-titre"
          name="sublabelTheme"
          fieldname="SUBLABEL_THEME"
          value={props.values["sublabelTheme"]}
          errors={props.errors["sublabelTheme"]}
          setFieldValue={props.setFieldValue}
        />
      </Grid>
      <Grid sm={12}>
        {renderSubmit()}
      </Grid>
    </form >
  );

  const handleSubmit = async (values: any) => {
    try {
      const { data } = await applyMutation({ variables: { section: values } })

      if (data[ADD_CMS_SECTION_TO_GROUP.OPERATION]) {
        refetch();
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });

        setDisplayMode(0);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  const ValidationSchema = Yup.object().shape({
    datamodel: Yup.string()
      .required('Champ obligatoire'),
    label: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    cardType: Yup.string()
      .required('Champ obligatoire'),
    cardRedirection: Yup.string()
      .required('Champ obligatoire'),
    screen: Yup.string()
      .required('Champ obligatoire'),
    sectionTheme: Yup.string()
      .required('Champ obligatoire'),
    templateSearch: Yup.string()
      .required('Champ obligatoire'),
  });

  return (
    <Formik
      render={renderForm}
      initialValues={{
        backgroundColor: "#FFFFFF",
        sectionGroupID: id
      }}
      onSubmit={handleSubmit}
      validationSchema={ValidationSchema}
    />
  );
}

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    labelFontColor: "#211337"
  },
});