import React, { useContext } from "react";
import { Formik } from "formik";
import * as Yup from 'yup';
import { useMutation } from "react-apollo";
import { Button, Grid, makeStyles } from "@material-ui/core";
import { TextField } from "../../../screens/components";
import ColorField from "../../../screens/components/CustomForms/ColorField";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { UPDATE_CMS_SECTION } from "../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";

export default ({ setDisplayMode, initialValues }: any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const [applyMutation, { loading: creating, error }]: any = useMutation(UPDATE_CMS_SECTION);

  const renderSubmit = () => (
    <div>
      <Button onClick={() => setDisplayMode(0)} color="primary">Annuler</Button>
      <ButtonLoading loading={creating} type="submit" label="Creer" />
    </div>
  );

  const renderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <Grid container spacing={1} className={classes.root}>
        <Grid item sm={3}>
          <TextField
            name="id"
            label="Section ID"
            disabled={true}
            value={props.values["id"]}
            errors={props.errors["id"]}
          />
        </Grid>
        <Grid item sm={3}>
          <TextField
            name="sectionGroupID"
            label="sectionGroup ID"
            disabled={true}
            value={props.values["sectionGroupID"]}
            errors={props.errors["sectionGroupID"]}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            name="screen"
            fieldname="SCREEN"
            value={props.values["screen"]}
            errors={props.errors["screen"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            name="datamodel"
            fieldname="SECTION_DATA_MODEL"
            value={props.values["datamodel"]}
            errors={props.errors["datamodel"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            label="Type de section"
            name="sectionType"
            fieldname="SECTION_TYPE"
            value={props.values["sectionType"]}
            errors={props.errors["sectionType"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            label="Type de carte"
            name="cardType"
            fieldname="CARD_TYPE"
            value={props.values["cardType"]}
            errors={props.errors["cardType"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <ColorField
            label="Background Color"
            name="backgroundColor"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.backgroundColor}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            label="Redirection des cartes"
            name="cardRedirection"
            fieldname="CARD_REDIRECTION"
            value={props.values["cardRedirection"]}
            errors={props.errors["cardRedirection"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            name="sectionTheme"
            fieldname="SECTION_THEME"
            value={props.values["sectionTheme"]}
            errors={props.errors["sectionTheme"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            name="templateSearch"
            fieldname="TEMPLATE_SEARCH"
            value={props.values["templateSearch"]}
            errors={props.errors["templateSearch"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>

        <Grid container spacing={1}>
          <Grid item sm={3}>
            <TextField
              label="Titre"
              name="label"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.label}
              error={props.errors.label}
            />
          </Grid>
          <Grid item sm={3}>
            <FieldBuilder
              label="Taille de police"
              name="labelFontSize"
              fieldname="FONT_SIZE"
              value={props.values["labelFontSize"]}
              errors={props.errors["labelFontSize"]}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={3}>
            <ColorField
              label="Couleur titre"
              name="labelFontColor"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values["labelFontColor"]}
              error={props.errors["labelFontColor"]}
            />
          </Grid>
          <Grid item sm={3}>
            <FieldBuilder
              label="Theme titre"
              name="labelTheme"
              fieldname="LABEL_THEME"
              value={props.values["labelTheme"]}
              errors={props.errors["labelTheme"]}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item sm={3}>
            <TextField
              label="Sous titre"
              name="sublabel"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.sublabel}
              error={props.errors.sublabel}
            />
          </Grid>
          <Grid item sm={3}>
            <FieldBuilder
              label="Taille Police sous titre"
              name="sublabelFontSize"
              fieldname="FONT_SIZE"
              value={props.values["sublabelFontSize"]}
              errors={props.errors["sublabelFontSize"]}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={3}>
            <ColorField
              label="Couleur sous-titre"
              name="sublabelFontColor"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values["sublabelFontColor"]}
              error={props.errors["sublabelFontColor"]}
            />
          </Grid>
          <Grid item sm={3}>
            <FieldBuilder
              label="Theme Sous-titre"
              name="sublabelTheme"
              fieldname="SUBLABEL_THEME"
              value={props.values["sublabelTheme"]}
              errors={props.errors["sublabelTheme"]}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
        </Grid>
        <Grid sm={12}>
          {renderSubmit()}
        </Grid>
      </Grid>
    </form >
  );

  const handleSubmit = async (values: any) => {
    console.log("handleSubmit", values);
    if (values.__typename) {
      delete values.__typename;
    }

    try {
      const { data } = await applyMutation({ variables: { section: values } })

      if (data["updateCMSSection"]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  const ValidationSchema = Yup.object().shape({
    datamodel: Yup.string()
      .required('Champ obligatoire')
  });

  return (
    <Formik
      render={renderForm}
      initialValues={parseInitValues(initialValues)}
      onSubmit={handleSubmit}
      validationSchema={ValidationSchema}
    />
  );
}

const parseInitValues = ({
  header,
  model,
  __typename,
  order,
  cards,
  ...rest
}: any) => ({
  ...header,
  ...rest
})

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    labelFontColor: "#211337"
  },
});