import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import { Query } from 'react-apollo';
import Accordions from './Accordions';
import { GET_CMS_PAGE } from '../../network';

export default ({ config }:any) => {
  const classes = useStyles();
  const { FETCH_HIGH_LEVEL } = config;

  return (
    <Query query={GET_CMS_PAGE.QUERY} variables={{ screen: "FOOD_SCREEN", criteria: [], size: 100, from: 0 }} fetchPolicy="no-cache">
      {({ loading, error: fetchError, data, refetch }: any) => {
        if (fetchError) return <Alert severity="error">Erreur chargement</Alert>
        const sectionGroup = data && data[GET_CMS_PAGE.OPERATION];

        return sectionGroup ? (
          <div className={classes.root}>
            <Accordions loading={loading} refetch={refetch} total={sectionGroup.total} sectionGroup={sectionGroup.data}/>
          </div>
        ): (<Alert severity="error">Pas de donnée</Alert>)
      }}
    </Query>
  );
}

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
});
