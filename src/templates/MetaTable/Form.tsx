import { Button, Grid, makeStyles } from "@material-ui/core";
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../RootContext";
import { TextField } from "../../screens/components";
import { TableContext, TYPES } from "./index";
import FieldBuilder from "../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../screens/components/Button/ButtonLoading";

const Form = ({ dispatch }:any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const { ADD_ELEMENT, owner } = useContext(TableContext);
  const [applyMutation, { loading: creating, error }]: any = useMutation(ADD_ELEMENT.MUTATION);

  const handleSubmit = async (variables: any, actions: any) => {
    try {
      const { data } = await applyMutation({ variables })

      if (data[ADD_ELEMENT.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });

        dispatch({ type: TYPES.SET_DISPLAY_MODE, payload: { mode: 0, refresh: true } })
      }

    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    const fields = ADD_ELEMENT.FIELDS || [];

    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <Grid container spacing={1} className={classes.root}>
          <Grid item sm={4}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
          </Grid>
          {fields.map((field:any, key: number) => (
            <Grid item sm={4}>
              <FieldBuilder
                key={key}
                name={field.name}
                fieldname={field.fieldname}
                onBlur={props.handleBlur}
                value={props.values["type"]}
                error={props.errors["type"]}
                setFieldValue={props.setFieldValue}
              />
            </Grid>)
          )
          }
          <Grid item sm={12}>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => dispatch({ type: TYPES.SET_DISPLAY_MODE, payload: { mode: 0 } })} color="primary">Annuler</Button>
            <ButtonLoading
              label="Creer"
              loading={creating}
              className={classes.button}
              variant="contained"
              type="submit"
              color="primary" />
          </Grid>
        </Grid>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{ owner }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ADD_ELEMENT.VALIDATION_SCHEMA || []}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});

export default Form;