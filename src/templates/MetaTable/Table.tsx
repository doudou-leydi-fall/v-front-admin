import * as React from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody, Checkbox, Chip, makeStyles } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Box } from 'victory';
import { TYPES } from './index';
import { useEffect } from 'react';

export default ({ loading, list, state, dispatch, onSelected }: any) => {
  const classes = useStyles();
  console.log("list", list);

  useEffect(() => {
    onSelected(state.selected);
  }, [state.selected]);
  
  return list && list.data ? (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell />
          <TableCell>ID</TableCell>
          <TableCell>Model</TableCell>
          <TableCell>Type</TableCell>
          <TableCell>Nom</TableCell>
          <TableCell>Propriété</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {list.data.map((row: any) => (
          <TableRow key={row.id}>
            <TableCell className={classes.checkCell}>
              <Checkbox
                checked={state.selected.includes(row.id)}
                onChange={(e, checked) => dispatch({
                  type: TYPES.SELECTED_ITEMS,
                  payload: {
                    id: row.id,
                    checked
                  }
                })}
                inputProps={{ 'aria-label': 'select all desserts' }} />
            </TableCell>
            <TableCell component="th" scope="row">
              <Chip color="default" size="small" label={`${row.id}`} />
            </TableCell>
            <TableCell>{<Chip color="primary" size="small" label={`${row.model}`} />}</TableCell>
            <TableCell>{<Chip color="secondary" size="small" label={`${row.type}`} />}</TableCell>
            <TableCell>{row.name}</TableCell>
            <TableCell>{row.owner}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>) : (<Alert severity="info">Pas de données</Alert>)
}

const useStyles = makeStyles({
  checkCell: {
    width: 50
  },
})