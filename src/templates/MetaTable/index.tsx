import React, { createContext, Fragment, useContext, useEffect, useReducer, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from "@apollo/react-hooks";
import { AppBar, Toolbar, Button, Icon, Box } from '@material-ui/core';
import Form from "./Form"
import Table from "./Table";
import { RootContext, UI_REDUCER_TYPES } from '../../RootContext';

export const TableContext: any = createContext({});
const TableProvider: any = TableContext.Provider;

export const TYPES: any = {
  SET_DISPLAY_MODE: "SET_DISPLAY_MODE",
  SELECTED_ITEMS: "SELECTED_ITEMS",
}

const initState = {
  displayMode: {
    mode: 0,
    formType: null,
    refresh: false,
  },
  triggerRefetch: false,
  selected: []
}

const refreshSelectedList = (state: any, action: any) => {
  if (action.payload.checked) {
    return { ...state, selected: [...state.selected, action.payload.id] }
  } else {
    return { ...state, selected: state.selected.filter((item: any) => item !== action.payload.id) };
  }
}

const reducers = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.SET_DISPLAY_MODE:
      return { ...state, displayMode: action.payload };
    case TYPES.SELECTED_ITEMS:
      return refreshSelectedList(state, action);
    default:
      return state;
  }
}

export default ({ config, owner }: any) => {
  const [state, dispatch] = useReducer(reducers, initState);
  const [selectedItems, setSelectedItems] = useState([]);
  const { uidispatch } = useContext(RootContext);
  const { FETCH_ELEMENT, DELETE_ELEMENT } = config;
  const classes = useStyles();

  const { data, refetch, loading } = useQuery(FETCH_ELEMENT.QUERY, {
    variables: FETCH_ELEMENT.parseVariables({ owner }),
  });

  const [deleteApply, { loading: deleting, error }] = useMutation(DELETE_ELEMENT.MUTATION);

  const onDelete = async (ids: string[], applyRefetch: Function) => {
    try {
      const { data } = await deleteApply({ variables: { ids } });
      if (data[DELETE_ELEMENT.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Supprimées",
            variant: "success"
          }
        });

        applyRefetch();
        setSelectedItems([]);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  useEffect(() => {
    if (state.displayMode.refresh) {
      refetch();
    }
  }, [state.displayMode]);

  useEffect(() => {
    console.log("selected LOG", state.selected);
  }, [state.selected]);

  // console.log("data", data);

  const renderTemplate = (displayMode: number) => {
    switch (displayMode) {
      case 0:
        return <Table loading={loading} list={data && data["searchMeta"]} state={state} dispatch={dispatch} onSelected={setSelectedItems} />;
      case 1:
        return <Form dispatch={dispatch}/>;
    }
  }

  return (
    <TableProvider value={{ ...config, owner }}>
      <Box className={classes.container}>
        <AppBar className={classes.appbar} position="static" color="default">
          <Toolbar className={classes.toolbar}>
            <div>
              <Button
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>add_variant</Icon>}
                onClick={() => dispatch({ type: TYPES.SET_DISPLAY_MODE, payload: { mode: 1 } })}
              >Ajouter</Button>
            </div>
            <div>
              <Button
                disabled={!(selectedItems.length > 0)}
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>delete_outlined</Icon>}
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => onDelete(selectedItems, refetch) }
                })}
              >Supprimer la selection</Button>
            </div>
          </Toolbar>
        </AppBar>
        {renderTemplate(state.displayMode.mode)}
      </Box>
    </TableProvider>
  );
}

const useStyles = makeStyles({
  tableContainer: {
    padding: "15px 15px 27px",
    marginBottom: 15,
  },
  appbar: {
    width: "auto",
    marginBottom: 20
  },
  container: {
    height: "100%",
    width: "100%"
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45,
    display: "flex",
    justifyContent: "space-between"
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  },
  alert: {

  },
  toolbarContainer: {
    marginBottom: 20
  },
});
