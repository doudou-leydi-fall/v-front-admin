import { Button, Grid, makeStyles } from "@material-ui/core";
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import { TextField } from "../../../screens/components";
import { CategoryContext, CATEGORY_TYPES } from "./index";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { ClassificationContext } from "..";

const CategoryForm = () => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const { ADD_CATEGORY, owner } = useContext(ClassificationContext);
  const { dispatchCategoryAction }: any = useContext(CategoryContext);
  const [addRootCategory, { loading: creating, error }]: any = useMutation(ADD_CATEGORY.MUTATION);

  const handleSubmit = async (variables: any, actions: any) => {
    try {
      const { data } = await addRootCategory({ variables })

      if (data[ADD_CATEGORY.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });

        dispatchCategoryAction({ type: CATEGORY_TYPES.SET_DISPLAY_MODE, payload: { mode: 0, refresh: true } })
      }

    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    const fields = ADD_CATEGORY.FIELDS || [];

    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <Grid container spacing={1} className={classes.root}>
          <Grid item sm={4}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
          </Grid>
          {fields.map((field:any, key: number) => (
            <Grid item sm={4}>
              <FieldBuilder
                key={key}
                name={field.name}
                fieldname={field.fieldName}
                onBlur={props.handleBlur}
                value={props.values[field.fieldName]}
                error={props.errors[field.fieldName]}
                setFieldValue={props.setFieldValue}
              />
            </Grid>)
          )
          }
          <Grid item sm={12}>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => dispatchCategoryAction({ type: CATEGORY_TYPES.SET_DISPLAY_MODE, payload: { mode: 0 } })} color="primary">Annuler</Button>
            <ButtonLoading
              label="Creer"
              loading={creating}
              className={classes.button}
              variant="contained"
              type="submit"
              color="primary" />
          </Grid>
        </Grid>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{ owner }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ADD_CATEGORY.VALIDATION_SCHEMA || []}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});

export default CategoryForm;