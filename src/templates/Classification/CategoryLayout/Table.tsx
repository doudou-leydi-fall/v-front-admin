import React, { Fragment, createContext, useReducer, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Row from "./Row";
import { Alert } from '@material-ui/lab';
import { TablePagination } from '@material-ui/core';

export const CategoryTableContext:any = createContext({});
const CategoryTableProvider = CategoryTableContext.Provider;

export const TYPES: any = {
  SELECTED_ITEMS: "SELECTED_ITEMS",
}

const initialValue: any = {
  selected: []
}

const refreshSelectedList = (state: any, action: any) => {
  if (action.payload.checked) {
    return { ...state, selected: [...state.selected, action.payload.id] }
  } else {
    return { ...state, selected: state.selected.filter((item: any) => item !== action.payload.id) };
  }
}

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.SELECTED_ITEMS:
      return refreshSelectedList(state, action);
    default:
      return state;
  }
}

export default ({ loading, list, onSelected }: any) => {
  const [state, dispatch] = useReducer(reducer, initialValue);
  const classes = useStyles();
  const [pager, setPager]: any = useState({ from: 0, page: 0, size: 10 });

  const handleChangePage = (event: any, page: number) => {
    const offset = page * pager.size;
    setPager({ ...pager, page, from: offset });
  };

  const handleChangeRowsPerPage = (event: any) => {
    setPager({ from: 0, page: 0, size: parseInt(event.target.value, 10) })
  };

  useEffect(() => {
    onSelected(state.selected);
  }, [state.selected]);

  return (
    <CategoryTableProvider value={{ 
      state
    }}>
      <div className={classes.container}>
        {list && list.data.length > 0 ?
          (
            <Fragment>
              <Table stickyHeader aria-label="collapsible table">
                <TableHead>
                  <TableRow>
                    <TableCell />
                    <TableCell align="center">ID</TableCell>
                    <TableCell>Nom</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Model</TableCell>
                    <TableCell align="left">Propriété</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody className={classes.tableBody}>
                  {list && list.data.map((row: any) => (<Row dispatch={dispatch} key={row.name} selected={state.selected.includes(row.id)} row={row} />))}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[1, 5, 10, 25]}
                component="div"
                count={list && list.total}
                rowsPerPage={pager && pager.size}
                page={pager && pager.page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Fragment>
          ) : <Alert className={classes.alert} severity="info">Pas de données</Alert>
        }
      </div>
    </CategoryTableProvider>
  );
}

const useStyles = makeStyles({
  tableContainer: {

  },
  container: {
    height: "100%"
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45
  },
  tableBody: {
    backgroundColor: "#F5F5F5",
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  },
  alert: {

  }
});
