import { SEARCH_META, ADD_META, DELETE_META, GET_META_LIST } from "../../../network";

export default {
  entity: "Metadonnée",
  fetch: {
    query: SEARCH_META,
    operation: "searchMeta",
    model: "Category",
    getParams: ({ q }: any) => ({
      variables: {
        model: "Category",
        type: "search",
        criteria: [
          {
            name: "model", 
            value: "Category"
          },
        ]
      }
    })
  },

  create: {
    mutation: ADD_META,
    operation: "addMeta",
    variable_name: "meta",
  },

  delete: {
    mutation: DELETE_META,
    operation: "deleteMeta",
    variable_name: "meta",
    deleteParams: (ids: any) => ({
      variables: {
        ids
      }
    }),
    success: "Metadonnée supprimées"
  }
}