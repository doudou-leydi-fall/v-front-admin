import React, { createContext, useContext, useEffect, useReducer, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from "@apollo/react-hooks";
import { AppBar, Toolbar, Button, Icon, Box } from '@material-ui/core';
import CategoryForm from "./Form"
import CategoryTable from "./Table";
import { RootContext, UI_REDUCER_TYPES } from '../../../RootContext';
import { ClassificationContext } from '..';

export const CategoryContext = createContext({});
const CategoryProvider = CategoryContext.Provider;

export const CATEGORY_TYPES: any = {
  SET_DISPLAY_MODE: "SET_DISPLAY_MODE"
}

const initState = {
  displayMode: {
    mode: 0,
    formType: null,
    refresh: false,
  },
  triggerRefetch: false
}

const reducers = (state: any, action: any) => {
  switch (action.type) {
    case CATEGORY_TYPES.SET_DISPLAY_MODE:
      return { ...state, displayMode: action.payload };
    default:
      return state;
  }
}

export default () => {
  const [categoryState, dispatchCategoryAction] = useReducer(reducers, initState);
  const [selectedItems, setSelectedItems] = useState([]);
  const { uidispatch } = useContext(RootContext);
  const { FETCH_CATEGORY, DELETE_CATEGORY, owner } = useContext(ClassificationContext);
  const classes = useStyles();

  const { data, refetch, loading } = useQuery(FETCH_CATEGORY.QUERY, {
    variables: FETCH_CATEGORY.parseVariables({ owner }),
  });

  const [deleteApply, { loading:deleting, error }] = useMutation(DELETE_CATEGORY.MUTATION);
  
  const onDelete = async (ids: string[], applyRefetch: Function) => {
    try {
      const { data } = await deleteApply({ variables: { ids }});
      if (data[DELETE_CATEGORY.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Supprimées",
            variant: "success"
          }
        });

        applyRefetch();
        setSelectedItems([]);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  useEffect(() => {
    if (categoryState.displayMode.refresh) {
      refetch();
    }
  }, [categoryState.displayMode]);

  console.log("data", data);

  const renderTemplate = (displayMode: number) => {
    switch (displayMode) {
      case 0:
        return <CategoryTable loading={loading} list={data && data["searchMeta"]} onSelected={setSelectedItems} />;
      case 1:
        return <CategoryForm />;
    }
  }

  return (
    <CategoryProvider value={{ categoryState, CATEGORY_TYPES, dispatchCategoryAction }}>
      <Box className={classes.container}>
        <AppBar className={classes.appbar} position="static" color="default">
          <Toolbar className={classes.toolbar}>
            <div>
              <Button
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>add_variant</Icon>}
                onClick={() => dispatchCategoryAction({ type: CATEGORY_TYPES.SET_DISPLAY_MODE, payload: { mode: 1 } })}
              >Ajouter</Button>
            </div>
            <div>
              <Button
                disabled={!(selectedItems.length > 0)}
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>delete_outlined</Icon>}
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => onDelete(selectedItems, refetch) }
                })}
              >Supprimer la selection</Button>
            </div>
          </Toolbar>
        </AppBar>
        {renderTemplate(categoryState.displayMode.mode)}
      </Box>
    </CategoryProvider>
  );
}

const useStyles = makeStyles({
  tableContainer: {
    padding: "15px 15px 27px",
    marginBottom: 15,
  },
  appbar: {
    width: "auto"
  },
  container: {
    height: "100%",
    width: "100%"
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45,
    display: "flex",
    justifyContent: "space-between"
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  },
  toolbarContainer: {
    marginBottom: 20
  },
});
