import React, { createContext } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import CategoryLayout from './CategoryLayout';
import { Grid } from '@material-ui/core';

export const ClassificationContext:any = createContext({});
const ClassificationProvider = ClassificationContext.Provider;

export default ({ config, owner }: any) => {
  const classes = useStyles();

  return (
    <ClassificationProvider value={{...config, owner}}>
      <Grid container className={classes.root}>
        <CategoryLayout />
      </Grid>
    </ClassificationProvider>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex'
  }
}));