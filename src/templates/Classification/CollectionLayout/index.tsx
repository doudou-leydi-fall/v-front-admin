import React, { memo, useContext, useEffect, useReducer, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { useMutation, useQuery } from "@apollo/react-hooks";
import CollectionTable from './Table';
import CollectionForm from './Form';
import { AppBar, Toolbar, Button, Icon } from '@material-ui/core';
import { RootContext, UI_REDUCER_TYPES } from '../../../RootContext';
import { ClassificationContext } from '..';

export const TYPES: any = {
  SELECTED_ITEMS: "SELECTED_ITEMS",
}

const initialValue: any = {
  selected: []
}

const refreshSelectedList = (state: any, action: any) => {
  if (action.payload.checked) {
    return { ...state, selected: [...state.selected, action.payload.id] }
  } else {
    return { ...state, selected: state.selected.filter((item: any) => item !== action.payload.id) };
  }
}

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.SELECTED_ITEMS:
      return refreshSelectedList(state, action);
    default:
      return state;
  }
}

const CollectionLayout = ({ selected: parentSelected, parent }: any) => {
  const classes = useRowStyles();
  const [state, dispatch] = useReducer(reducer, initialValue);
  const [displayMode, setDisplayMode] = useState(0);
  const { uidispatch } = useContext(RootContext);
  const { FETCH_COLLECTION, DELETE_COLLECTION } = useContext(ClassificationContext);
  const { id, name, root } = parent;

  const [deleteApply] = useMutation(DELETE_COLLECTION.MUTATION);
  
  const { data, refetch, loading } = useQuery(FETCH_COLLECTION.QUERY, { 
    variables: FETCH_COLLECTION.getVariables({ id })
  });

  const onDelete = async (ids: string[], applyRefetch: Function) => {
    try {
      const { data } = await deleteApply({ variables: { ids } });

      if (data[DELETE_COLLECTION.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Supprimées",
            variant: "success"
          }
        });

        applyRefetch();
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  }

  const renderTemplate = (mode: number) => {
    switch (mode) {
      case 0:
        return <CollectionTable state={state} dispatch={dispatch} list={data && data["searchMeta"]} allSelected={parentSelected} />
      case 1:
        return <CollectionForm state={state} dispatch={dispatch} setDisplayMode={setDisplayMode} parentID={id} categoryID={root && root.id} refetch={refetch} />
      default:
        return null;
    }
  }

  useEffect(() => {
    console.log("state.selected LOG", state.selected);
  }, [state.selected]);

  return (
    <div className={classes.rootContainer}>
      <div className={classes.container}>
        <AppBar className={classes.appbar} position="static" color="default">
          <Toolbar className={classes.toolbar}>
            {name ? <Typography variant="h5">{name}</Typography> : null}
            <div>
              <Button
                disabled={!(state.selected.length > 0)}
                color="inherit"
                variant="outlined"
                className={classes.button}
                startIcon={<Icon>delete_outlined</Icon>}
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => onDelete(state.selected, refetch) }
                })}
              >Supprimer la selection</Button>
              <Button
                color="primary"
                variant="contained"
                className={classes.button}
                startIcon={<Icon>add_variant</Icon>}
                onClick={() => setDisplayMode(1)}
              >Ajouter</Button>
            </div>
          </Toolbar>
        </AppBar>
        {renderTemplate(displayMode)}
      </div>
    </div>
  );
}

export default memo(CollectionLayout);

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
      padding: 13
    },
    width: "100%"
  },
  rootContainer: {
    padding: 20
  },
  tableContainer: {

  },
  appbar: {
    width: "100%",
    marginBottom: 20
  },
  container: {
    height: "100%",
    backgroundColor: "#FFF",
    borderRadius: 8,
    padding: 20
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45,
    display: "flex",
    justifyContent: "space-between"
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  },
  alert: {

  },
  toolbarContainer: {
    marginBottom: 20
  },
});