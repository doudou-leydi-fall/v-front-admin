import { Button, Grid, makeStyles } from "@material-ui/core";
import * as Yup from 'yup';
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import { TextField } from "../../../screens/components";
import FieldBuilder from "../../../screens/components/Forms/FormFields/FieldBuilder";
import ButtonLoading from "../../../screens/components/Button/ButtonLoading";
import { ClassificationContext } from "..";

interface TableEmbeddedFormProps {
  dispatch?: any
  state: any
  refetch?: any
  formType?: string;
  setDisplayMode: Function;
  categoryID: string;
  parentID: String;
}

export default ({ setDisplayMode, parentID, categoryID, refetch }: TableEmbeddedFormProps) => {
  const classes = useStyles();
  const { ADD_COLLECTION, owner } = useContext(ClassificationContext);
  const { uidispatch } = useContext(RootContext);
  const [applyMutation, { loading: creating, error }]: any = useMutation(ADD_COLLECTION.MUTATION);

  const handleSubmit = async (variables: any, actions: any) => {
    try {
      const { data } = await applyMutation({ variables })

      if (data[ADD_COLLECTION.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });
        refetch();
        setDisplayMode(0)
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    const fields = ADD_COLLECTION.FIELDS || [];
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <Grid container spacing={1} className={classes.root}>
          <Grid item sm={4}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
          </Grid>
          {fields.map((field: any, key: number) => (
            <Grid item sm={4}>
              <FieldBuilder
                name={field.name}
                fieldname={field.fieldName}
                onBlur={props.handleBlur}
                value={props.values[field.name]}
                error={props.errors[field.name]}
                setFieldValue={props.setFieldValue}
                params={{ parentID: categoryID }}
              />
            </Grid>))}
          < Grid item sm={12}>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => setDisplayMode(0)} color="primary">Annuler</Button>
            <ButtonLoading
              label="Creer"
              loading={creating}
              className={classes.button}
              variant="contained"
              type="submit"
              color="primary" />
          </Grid>
        </Grid>
      </form >
    );
  }

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    cardType: Yup.string()
      .required('Champ obligatoire'),
  });

  return (
    <Formik
      initialValues={{
        categoryID: parentID,
        owner
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ADD_COLLECTION.VALIDATION_SCHEMA || []}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});