import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from "react-i18next";
import { SnackbarProvider } from "notistack";
import i18next from "i18next";
import './styles/fonts/roboto/stylesheet.css';
import './styles/fonts/futura/Futura-Bold.ttf';
import './styles/fonts/oswald/Oswald-Bold.ttf';
import './styles/fonts/oswald/Oswald-Bold.woff';
import './styles/fonts/oswald/Oswald-Bold.woff2';
import './styles/fonts/oswald/Oswald-Regular.ttf';
import './styles/fonts/oswald/Oswald-Regular.woff';
import './styles/fonts/oswald/Oswald-Regular.woff2';
import './styles/fonts/oswald/Oswald-Light.ttf';
import './styles/fonts/oswald/Oswald-Light.woff';
import './styles/fonts/oswald/Oswald-Light.woff2';
import './index.css';
import "typeface-roboto";
import App from './App';
import * as serviceWorker from './serviceWorker';

i18next.init({
  interpolation: { escapeValue: false },  // React already does escaping
});

ReactDOM.render(
  <I18nextProvider i18n={i18next}>
    <SnackbarProvider>
      <App />
    </SnackbarProvider>
  </I18nextProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
