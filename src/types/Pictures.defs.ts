export interface Picture {
  id?: string
  position?: number
  mobile?: string
  desktop?: string
  provider?: string
  format?: string
}