import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Card } from "../components/Card";

interface ProductCardProps {
  name?: string
  recipes?: any
  collection?: any
  price?: any
  src: any
}
 interface ItemProps {
   item?: ProductCardProps
 }
export default ({ item }:any) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Card
        title={item.name}
        content={item.recipes && item.recipes.map((recipe:any) => recipe.name).join(" - ")}
        picture={item.src}
        chipTitle={item.collection && item.collection.name}
        chipFooterLeft={`${item.price && item.price.value} FCFA`}
        imageBackgroundSize="contain"
      />
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  container: {
    height: 330,
    width: 280,
  }
}));