import React from "react";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

interface ButtonLoadingProps {
  label: string;
  loading: boolean;
  rest?: any;
  className?: string;
  variant?: string;
  type?: string;
  color?: string;
}

export default ({ label, loading, ...rest }:any) => (
  <Button {...rest}>
    {loading && <CircularProgress style={{ marginRight: 10 }} color="inherit" size={20} thickness={4} />}
    {loading ? "En cours ..." : label}
  </Button>)