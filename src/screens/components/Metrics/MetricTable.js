import React from "react";
import { makeStyles } from "@material-ui/core/styles";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

export const MetricTable = ({ metrics, columns = [] }) => {
  const classes = useStyles();
  console.log("MetricTable data", metrics);
  return (
    <div className={classes.root}>
      <div className={classes.row}>
        <span>Hello</span>
        <span>18</span>
      </div>
    </div>
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1
  },
  table: {
    minWidth: 650,
    height: 500
  },
  rowHeight: {
    height: 30
  },
  row: {
    display: "flex"
  }
});