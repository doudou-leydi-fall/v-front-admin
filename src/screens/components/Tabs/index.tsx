import React, { useState } from "react";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { Toolbar, IconButton, Avatar } from "@material-ui/core";
import Icon from '@material-ui/core/Icon';
import { useHistory } from "react-router-dom";
import { StyledTabs, StyledTab } from "./TabsUtils";

interface TabPanelProps {
  children: any
  value: any
  index: any
}

function TabPanel({ children, value, index, ...other }: TabPanelProps) {
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "#F5F5F5",
    width: "%100",
  },
  tabbar: {
    borderBottom: "1px solid #C2C2C2"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  toolbar: {
    minHeight: 128,
    justifyContent: "space-between",
    alignItems: 'flex-start',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  toolbarLeft: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  title: {
    flexGrow: 1,
  },
  subtitle: {
    fontSize: 13
  },
  src: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    marginBottom: 10,
    marginTop: 10,
    marginRight: 10
  }
}));

interface TabsHeader {
  title: string
  subtitle?: string
  logo?: string
}

interface TabsPanelProps {
  initStore?: any
  tabs: any[]
  header?: TabsHeader
  avatarEnabled?: boolean;
}

export const TabsPanel = ({ initStore, tabs, header, avatarEnabled=true }: TabsPanelProps) => {
  const classes = useStyles();
  const theme = useTheme();
  const [store, setStore] = useState(initStore);
  const [value, setValue] = React.useState(0);
  let history = useHistory();

  const handleChange = (event: any, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar className={classes.tabbar} position="static" color="default">
        {header ? <Toolbar className={classes.toolbar}>
          <div className={classes.toolbarLeft}>
            {avatarEnabled && <Avatar variant="square" alt="Remy Sharp" src={header.logo || "/assets/img/user.png"} className={classes.src} />}
            <div>
              <Typography className={classes.title} variant="h3" noWrap>
                {header.title}
              </Typography>
              <Typography className={classes.subtitle} noWrap>
                {header.subtitle}
              </Typography>
            </div>
          </div>
          <div>
            <IconButton onClick={() => history.goBack()} edge="end" color="inherit">
              <Icon style={{ fontSize: 30 }}>close_circle</Icon>
            </IconButton>
          </div>
        </Toolbar> : null}
        <StyledTabs
          value={value}
          onChange={handleChange}
          aria-label="full width tabs example"
        >
          {tabs.map((tab, key) => (
            <StyledTab label={tab.label} />
          ))}
        </StyledTabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {tabs.map((tab, key) => (
          key === value ?
            <TabPanel
              value={value}
              index={key}
            >
              {React.cloneElement(tab.component, { tabStore: store })}
            </TabPanel>
            : <div>Loadding</div>
        ))}
      </SwipeableViews>
    </div>
  );
};
