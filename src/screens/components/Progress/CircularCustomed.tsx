import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress';
import COLORS from '../../../styles/colors/ColorMap';

// Inspired by the former Facebook spinners.
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'relative',
      display: "flex"
    },
    bottom: {
      color: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    circle: {
      strokeLinecap: 'round',
    },
  }),
);

export default (props: CircularProgressProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress
        variant="determinate"
        className={classes.bottom}
        size={40}
        thickness={4}
        {...props}
        value={100}
      />
      <CircularProgress
        variant="indeterminate"
        disableShrink
        style={{
          color: COLORS[props.color || "primay"],
          animationDuration: '550ms',
          position: 'absolute',
          left: 0,
        }}
        classes={{
          circle: classes.circle,
        }}
        size={props.size || 40}
        thickness={6}
        {...props}
      />
    </div>
  );
}
