import React from "react";
import { SelectField } from "../SelectInput";

export default props => {
  return (
    <SelectField name="country" label="Pays" register={props.register}/>
  );
}