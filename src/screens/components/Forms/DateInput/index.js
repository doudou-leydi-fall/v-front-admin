import React, { useState, useEffect, useMemo } from "react";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import frLocale from "date-fns/locale/fr";
import DateFnsUtils from "@date-io/date-fns";

export const DateField = (props) => {
  const [selectedDate, handleDateChange] = useState(new Date());

  useMemo(() => {
    console.log("useEffect date", selectedDate.toISOString());
    props.setFieldValue(props.name, selectedDate.toISOString());
  }, [selectedDate]);
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={frLocale}>
      <DateTimePicker
        fullWidth
        label={props.label}
        inputVariant="outlined"
        size="small"
        value={selectedDate}
        onChange={handleDateChange}
      />
      </MuiPickersUtilsProvider>
  );
}
