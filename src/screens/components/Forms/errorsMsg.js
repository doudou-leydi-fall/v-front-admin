export default {
  required: "Champs Obligatoire",
  minLength: "Longeur trop petite",
  maxLength: "Taille maximum dépassée",
  min: "Nombre trop petite",
  max: "Nombre trop élevé",
  pattern: "Format invalide",
  validate: "Champ invalide"
};