import React from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

export const SuggestField = ({
  name,
  getOptionLabel,
  label,
  handleValue,
  setFieldValue,
  initValue,
  options,
  error
}) => {
  console.log("initValue LOG", initValue);
  return (
    <div style={{ width: "100%" }}>
      <Autocomplete
        id={name}
        name={name}
        label={label}
        multiple
        size="small"
        options={options}
        autoSelect={true}
        defaultValue={initValue && initValue.map(value => ({ "name": value }))}
        onChange={(e, values) => setFieldValue(name, values.map(handleValue))}
        getOptionLabel={getOptionLabel}
        filterSelectedOptions
        renderInput={params => (
          <TextField
            {...params}
            variant="filled"
            placeholder={label}
            fullWidth
          />
        )}
        fullWidth
        helperText={error}
      />
    </div>
  );
};
