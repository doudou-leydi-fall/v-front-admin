import React from 'react';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Icon from '@material-ui/core/Icon';
import { makeStyles, Theme } from '@material-ui/core';
import COLORS from "../../../../styles/colors/ColorMap";

const getData = ({ value, color }: any) => {
  const TYPEMAP: any = {
    ERROR: {
      variant: "contained",
      color: "secondary",
      icon: <Icon>error</Icon>,
      style: { backgroundColor: "#E60D23", color: "#FFF" },
      label: value || "Pas de donnée",
      msg: value || "Ce champs est obligatoire"
    },
    NOTHING_TO_SIGNAL: {
      variant: "contained",
      color: COLORS[color] || "secondary",
      icon: <Icon>check</Icon>,
      style: { backgroundColor: COLORS[color] || "#2EBCB1", color: "#FFF" },
      label: value || "Pas de donnée",
      msg: value || "Ce champs est obligatoire"
    }
  }

  if (value) {
    return TYPEMAP["NOTHING_TO_SIGNAL"];
  }
  return TYPEMAP["ERROR"]
}

export default ({ value, color, onClick = () => ({}) }: any) => {
  const classes = useStyles();
  const data = getData({ value, color });

  return (
    <Tooltip TransitionComponent={Zoom} title={data.msg}>
      <Button
        variant={data.variant}
        color={data.color}
        style={data.style}
        size="small"
        startIcon={data.icon}
        onClick={onClick}
      >{data.label}</Button>
    </Tooltip>
  );
}

const useStyles = makeStyles(() => ({
  button: {

  },
}),
);