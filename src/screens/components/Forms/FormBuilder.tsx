import { Button, Grid, makeStyles } from "@material-ui/core";
import * as Yup from 'yup';
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../RootContext";
import { TextField } from "../../components";
import BasisSelectMeta from "../../components/CustomForms/MetaFields/BasicSelectMeta";

const FormBuilder = (props: any) => (
  <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
    <Grid container spacing={1}>
      <Grid item sm={4}>
        <TextField
          label="Nom"
          name="name"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.name}
          error={props.errors.name}
        />
      </Grid>
      <Grid item sm={4}>
        <TextField
          label="Label"
          name="label"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.label}
          error={props.errors.label}
        />
      </Grid>
      <Grid item sm={4}>
        <BasisSelectMeta
          name="childRequirement"
          label="Type de champ requis"
          setFieldValue={props.setFieldValue}
          meta="childRequirement"
          value={props.value}
        />
      </Grid>
    </Grid>
  </form>
);

export default FormBuilder;