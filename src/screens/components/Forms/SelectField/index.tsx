import React from "react";
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText
} from "@material-ui/core";

interface Meta {
  id?: string
  name: string
  value?: string
  __typename?: string 
}

interface SelectProps {
  name: string
  label: string
  value: Meta
  error?: string
  options: any[]
  optionLabel?: string
  optionValue?: string
  setFieldValue: any
  disabled?: boolean
} 

export const SelectField = ({
  name,
  label,
  value,
  error,
  options = [],
  optionLabel = "name",
  optionValue="name",
  setFieldValue,
  disabled = false
}: SelectProps) => {

  return (
    <FormControl variant="filled" fullWidth size="small">
      <InputLabel htmlFor={name} id={name} required={true}>{label}</InputLabel>
      <Select
        disabled={disabled}
        value={value}
        onChange={e => setFieldValue(name, e.target.value)}
      >
        <MenuItem value="">None</MenuItem>
        {options.map(option => (
          <MenuItem key={option.name} value={option[optionValue]}>
            {option[optionLabel]}
          </MenuItem>))}
      </Select>
      {error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
}
