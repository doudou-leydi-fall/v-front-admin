import React, { useState, useReducer, useEffect } from "react";
import FieldBuilder from "./FieldBuilder";

const RequiredFields = ({
  fields,
  values,
  setFieldValue,
  onBlur,
  errors
}: any) => {
  const [fieldsValue, setFieldsValue]: any = useState({});

  const setValue = (e: any, v: any) => {
    let tmp = { ...fieldsValue };
    tmp[e] = v && v.id;
    setFieldsValue(tmp);
  }

  useEffect(() => {
    let tmp:any[] = []
    Object.keys(fieldsValue).map((k:any) => {
      tmp.push({ name: k, value: fieldsValue[k]})
    });
    setFieldValue("attributes", tmp)
  }, [fieldsValue])

  return (
    <div>
      {Array.isArray(fields) && fields.map(({ name }: any) => (
        <FieldBuilder
          key={name}
          name={name}
          fieldname={name}
          onBlur={onBlur}
          setFieldValue={setValue}
          value={values[name]}
          error={errors[name]}
        />
      ))}
    </div>
  );
}

export default RequiredFields;