import Catcol from "./Catcol";
import CardType from "./CardType";
import SectionType from "./SectionType";
import ClassificationType from "./ClassificationType";
import Recipes from "./Recipes";
import RootCategory from "./RootCategory";
import RootCollection from "./RootCollection";
import SectionTheme from "./SectionTheme";
import SectionDataModel from "./SectionDataModel";
import Screen from "./Screen";
import FontSize from "./FontSize";
import FontFamily from "./FontFamily";
import CardRedirection from "./CardRedirection";
import TemplateSearch from "./TemplateSearch";
import LabelTheme from "./LabelTheme";
import SubLabelTheme from "./SubLabelTheme";
import Prices from "./Prices";
import TypeProduct from "./TypeProduct";
import Sheet from "./Sheet";
import ProductClassification from "./ProductClassification";

const FieldsMap: any = {
  "CAT_COL": Catcol,
  "CARD_TYPE": CardType,
  "SECTION_TYPE": SectionType,
  "CARD_REDIRECTION": CardRedirection,
  "CLASSIFICATION_TYPE": ClassificationType,
  "RECIPES": Recipes,
  "ROOT_CATEGORY": RootCategory,
  "ROOT_COLLECTION": RootCollection,
  "SECTION_THEME": SectionTheme,
  "SECTION_DATA_MODEL": SectionDataModel,
  "SCREEN": Screen,
  "FONT_SIZE": FontSize,
  "FONT_FAMILY": FontFamily,
  "TEMPLATE_SEARCH": TemplateSearch,
  "LABEL_THEME": LabelTheme,
  "SUBLABEL_THEME": SubLabelTheme,
  "PRICES": Prices,
  "TYPE_PRODUCT": TypeProduct,
  "SHEET": Sheet,
  "PRODUCT_CLASSIFICATION": ProductClassification
}

export default FieldsMap;