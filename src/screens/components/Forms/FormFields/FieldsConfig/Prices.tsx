import Prices from "../CustomFields/Prices";

export default {
  label: "Prix",
  component: Prices,
  currency: {
    options: [
      "FCFA",
      "Euro",
      "$"
    ]
  },
  period: {
    options: [
      "PERMANENT",
      "HOURLY",
      "DAILY",
      "WEEKLY",
      "MONTHLY",
      "ANNUALY",
    ]
  },
  size: {
    options: [
      "Petite",
      "Moyenne",
      "Grande",
      "XXL",
      "Large",
      "Medium",
    ]
  }
}