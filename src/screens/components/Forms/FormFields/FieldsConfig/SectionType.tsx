import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Type de section",
  options: [
    "STANDARD"
  ],
  component: FormikSimpleSelect,
  filterValue: ({ id }: any) => id,
}