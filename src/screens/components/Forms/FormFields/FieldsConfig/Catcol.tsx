import { SelectDoubleAsync } from "../Select";
import { SEARCH_META } from "../../../../../network";

export default {
  label: "Catégory - Collection",
  query: SEARCH_META,
  operation: "searchMeta",
  component: SelectDoubleAsync,
  primary: {
    type: "SELECT",
    name: "categoryCatalogue",
    label: "Catégories",
    variables: ({ q, owner }: any) => ({
      model: "Category",
      type: "prefixSearch",
      q,
      criteria:[
        { 
          name: "type", 
          value: "FOOD"
        }, {
          name: "model", 
          value: "Category"
        },
         {
          name: "owner", 
          value: owner
        }
      ]
    }),
    filterValue: ({ id, name }: any) => ({ id, name }),
  },
  secondary: {
    type: "SELECT",
    name: "collectionCatalogue",
    label: "Collection",
    variables: ({ q, owner, parentID }: any) => ({
      model: "Collection",
      type: "prefixSearch",
      q,
      criteria:[
        { 
          name: "type", 
          value: "FOOD"
        }, {
          name: "model", 
          value: "Collection"
        },
         {
          name: "owner", 
          value: owner
        },
        {
          name: "parentID", 
          value: parentID
        }
      ]
    }),
    filterValue: ({ id, name }: any) => ({ id, name }),
  }
}