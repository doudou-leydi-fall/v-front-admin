import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Carte Redirection",
  options: [
    "PlaceListScreen",
    "ProductListScreen"
  ],
  component: FormikSimpleSelect,
  filterValue: ({ id }: any) => id,
}