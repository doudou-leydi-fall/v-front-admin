import { SEARCH_META } from "../../../../../network";
import { FormikSelectInteractive } from "../Select/SelectInteractive";

export default {
  label: "Collection parent",
  multiple: true,
  query: SEARCH_META,
  operation: "searchMeta",
  component: FormikSelectInteractive,
  variables: ({ q, type, parentID }: any) => ({
    model: "Collection",
    type: "prefixSearch",
    q,
    criteria:[
      { 
        name: "type", 
        value: type
      }, {
        name: "model", 
        value: "Collection"
      },
       {
        name: "owner", 
        value: "root"
      },
      {
        name: "parentID", 
        value: parentID
      }
    ]
  }),
  filterValue: ({ id, name }: any) => ({ id, name }),
}