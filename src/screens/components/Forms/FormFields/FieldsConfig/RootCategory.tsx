import { SEARCH_META } from "../../../../../network";
import { FormikSelectInteractive } from "../Select/SelectInteractive";

export default {
  label: "Categorie parent",
  multiple: true,
  query: SEARCH_META,
  operation: "searchMeta",
  component: FormikSelectInteractive,
  variables: ({ q, type }: any) => ({
    model: "Category",
    type: "prefixSearch",
    q,
    criteria:[
      { 
        name: "type", 
        value: type
      }, {
        name: "model", 
        value: "Category"
      },
       {
        name: "owner", 
        value: "root"
      }
    ]
  }),
  filterValue: ({ id, name }: any) => ({ id, name }),
}