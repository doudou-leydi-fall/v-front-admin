import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Type de carte",
  options: [
    "RectangleCardSmall",
    "RectangleCardMedium",
    "RectangleCardLarge",
    "SquareCardSmall",
    "SquareCardMedium",
    "SquareCardLarge",
    "RoundedCardSmall",
    "RoundedCardMedium",
    "RoundedCardLarge",
  ],
  component: FormikSimpleSelect
}