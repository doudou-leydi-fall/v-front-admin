import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Label Theme",
  options: [
    "ICY",
    "NIGHT-CLUB",
    "GREEN-NEON",
    "FIRE-GRILL",
    "ORANGE-NEON",
    "RED-NEON",
    "YELLOW-NEON"
  ],
  component: FormikSimpleSelect,
  filterValue: ({ id }: any) => id,
}