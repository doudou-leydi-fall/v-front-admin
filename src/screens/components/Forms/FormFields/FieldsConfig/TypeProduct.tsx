import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Type de product",
  options: [
    "FOOD",
    "PROGRAM",
    "ACCOMMODATION_RENT",
    "DEVICE_RENT",
    "SUBSCRIPTION"
  ],
  component: FormikSimpleSelect
}