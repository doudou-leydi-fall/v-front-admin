import ProductClassification from "../CustomFields/ProductClassification";

export default {
  component: ProductClassification,
  getCategoryVariables: ({ type, qCategory, owner }:any) => ({
    model: "Category",
    type: "prefixSearch",
    q: qCategory,
    criteria: [
      {
        name: "type",
        value: type
      }, {
        name: "model",
        value: "Category"
      },
      {
        name: "owner",
        value: owner
      }
    ]
  }),
  getCollectionVariables: ({ type, owner, parentID, qCollection }: any) => ({
    model: "Collection",
    type: "prefixSearch",
    q: qCollection,
    criteria: [
      {
        name: "type",
        value: type
      }, {
        name: "model",
        value: "Collection"
      },
      {
        name: "owner",
        value: owner
      },
      {
        name: "parentID",
        value: parentID
      }
    ]
  })
}