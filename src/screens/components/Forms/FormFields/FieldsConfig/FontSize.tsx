import { FormikSelectRange } from "../Select/SelectRange";

export default {
  type: "SELECT",
  label: "Taille Police du sous-titre",
  optionsRange: {
    min: 11,
    max: 100,
  },
  component: FormikSelectRange
}