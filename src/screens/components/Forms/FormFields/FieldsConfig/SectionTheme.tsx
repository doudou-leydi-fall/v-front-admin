import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Theme",
  options: [
    "LIGHT",
    "DARK",
    "DARKONLIGHT",
    "LIGHTONDARK"
  ],
  component: FormikSimpleSelect,
  filterValue: ({ id }: any) => id,
}