import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Écran",
  options: [
    "FOOD-HOME",
    "PROGRAM-HOME"
  ],
  component: FormikSimpleSelect
}