import { FormikSelectMultipleInteractive } from "../Select";
import { SEARCH_META } from "../../../../../network";

export default {
  label: "Recettes",
  multiple: true,
  query: SEARCH_META,
  operation: "searchMeta",
  component: FormikSelectMultipleInteractive,
  variables: ({ q, type }: any) => ({
    model: "Composition",
    type: "prefixSearch",
    criteria:[
      { 
        name: "type",
        value: type
      }, {
        name: "model", 
        value: "Element"
      },
    ],
    q
  }),
  filterValue: ({ id, name }: any) => ({ id, name }),
}