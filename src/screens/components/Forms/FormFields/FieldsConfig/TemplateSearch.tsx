import { SEARCH_META } from "../../../../../network";
import { FormikSelectAsync } from "../Select";

export default {
  type: "SELECT",
  label: "Template de recherche",
  query: SEARCH_META,
  operation: "searchMeta",
  component: FormikSelectAsync,
  variables: () => ({
    model: "TemplateSearch",
    type: "search",
    criteria:[]
  }),
  filterValue: ({ id, name }: any) => ({ id, name }),
}