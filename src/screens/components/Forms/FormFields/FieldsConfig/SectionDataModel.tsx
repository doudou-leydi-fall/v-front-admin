import { FormikSimpleSelect } from "../Select/Select";

export default {
  type: "SELECT",
  label: "Model de donnée",
  options: [
    "Product",
    "Event",
    "Place",
    "Profile"
  ],
  component: FormikSimpleSelect,
  filterValue: ({ id }: any) => id,
}