import { FormikSelect } from "../Select/FormikSelect";

export default {
  type: "SELECT",
  label: "Font Family",
  options: [
    {
      id: "FUTURA_MEDIUM",
      name: "FUTURA_MEDIUM"
    },
    {
      id: "SANSATION_REGULAR",
      name: "SANSATION_REGULAR"
    },
    {
      id: "AVENIR",
      name: "AVENIR"
    },
    {
      id: "AVENIR_ITALIC",
      name: "AVENIR_ITALIC"
    },
    {
      id: "AVENIR_HEAVYITALIC",
      name: "AVENIR_HEAVYITALIC"
    },
    {
      id: "AVENIR_HEAVY",
      name: "AVENIR_HEAVY"
    },
    {
      id: "AVENIR_BLACKITALIC",
      name: "AVENIR_BLACKITALIC"
    },
    {
      id: "AVENIR_BOOKITALIC",
      name: "AVENIR_BOOKITALIC"
    },
    {
      id: "LEARNING_CURVE",
      name: "LEARNING_CURVE"
    },
    {
      id: "LEARNING_CURVE_BOLD",
      name: "LEARNING_CURVE_BOLD"
    },
    {
      id: "MENTO",
      name: "MENTO"
    },
    {
      id: "SANSATION_REGULAR",
      name: "SANSATION_REGULAR"
    }
  ],
  component: FormikSelect,
  filterValue: ({ id }: any) => id,
}