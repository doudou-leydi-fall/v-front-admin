import { FormikSelect } from "../Select/FormikSelect";

export default {
  type: "SELECT",
  label: "Type",
  options: [
    {
      id: "FOOD",
      name: "Alimentaire"
    },
    {
      id: "PROGRAM",
      name: "Programme"
    }
  ],
  component: FormikSelect,
  filterValue: ({ id }: any) => id,
}