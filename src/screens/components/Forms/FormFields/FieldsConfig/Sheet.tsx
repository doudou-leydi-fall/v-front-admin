import Sheet from "../CustomFields/Sheet";

export default {
  label: "Prix",
  component: Sheet,
  currency: {
    options: [
      "FCFA",
      "Euro",
      "$"
    ]
  },
  period: {
    options: [
      "PERMANENT",
      "HOURLY",
      "DAILY",
      "WEEKLY",
      "MONTHLY",
      "ANNUALY",
    ]
  },
  size: {
    options: [
      "Petite",
      "Moyenne",
      "Grande",
      "XXL",
      "Large",
      "Medium",
    ]
  }
}