import React, { Fragment, memo, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormHelperText } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { IBasicSelectProps, IFormikSelectProps } from "./Select.d";

export const Select = ({
  name,
  label,
  value,
  errors,
  handleChange,
  filterValue = ({ id, name }: any) => ({ id, name }),
  fetchError,
  loading,
  options = [],
  defaultValue = () => ({}),
  textChange = () => ({})
}: IBasicSelectProps) => {
  const [open, setOpen] = useState(false);
  
  const setValue = (e: any, value: any) => {
    if (value && value.id && value.name) {
      handleChange(filterValue(value))
    } else {
      handleChange(null);
    }
  }

  return !fetchError ? (
    <Fragment>
      <Autocomplete
        id={name}
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        autoSelect={true}
        getOptionSelected={(option: any, value: any) => option.name === value.name}
        getOptionLabel={(option: any) => option.name}
        options={options}
        loading={loading}
        onChange={setValue}
        defaultValue={value}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            variant="filled"
            size="small"
            onChange={(e: any) => textChange(e.target.value)}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      {errors && <FormHelperText>{errors}</FormHelperText>}
    </Fragment>
  ) : <Alert severity="error">Champ {label} - Erreur Lors du chargement</Alert>;
}

export const SimpleSelect = ({
  name,
  label,
  value,
  errors,
  handleChange,
  fetchError,
  loading,
  options = [],
  textChange = () => ({})
}: IBasicSelectProps) => {
  const [open, setOpen] = useState(false);

  const setValue = (e: any, value: any) => {
    if (!!value) {
      handleChange(value)
    } else {
      handleChange(null);
    }
  }

  return !fetchError ? (
    <Fragment>
      <Autocomplete
        id={name}
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        fullWidth
        autoSelect={true}
        options={options}
        loading={loading}
        onChange={setValue}
        defaultValue={value}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            variant="filled"
            size="small"
            onChange={(e: any) => textChange(e.target.value)}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      {errors && <FormHelperText>{errors}</FormHelperText>}
    </Fragment>
  ) : <Alert severity="error">Champ {label} - Erreur Lors du chargement</Alert>;
}

export const FormikSimpleSelect = ({ name, setFieldValue, ...rest }: IFormikSelectProps) => (
  <SimpleSelect
    name={name}
    handleChange={(value: any) => setFieldValue(name, value)}
    {...rest}
  />
)