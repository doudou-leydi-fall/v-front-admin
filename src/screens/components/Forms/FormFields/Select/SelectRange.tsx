import React, { Fragment } from "react";
import { FormControl, FormHelperText, InputLabel, MenuItem } from "@material-ui/core";
import Select from '@material-ui/core/Select';
import { ForumTwoTone } from "@material-ui/icons";
import { IFormikSelectProps } from "./Select.d";

export const SelectRange = ({
  name,
  label,
  value,
  error,
  handleChange,
  filterValue = ({ id, name }: any) => ({ id, name }),
  optionsRange }: any) => {

  const renderOptions = () => {
    const options: any[] = [];

    for (let i = optionsRange.min; i < (optionsRange.max + 1); i++) {
      options.push(<MenuItem key={i} value={i}>{i}</MenuItem>);
    }
    return options;
  }

  return (
    <Fragment>
      <FormControl size="small" variant="filled" fullWidth>
        <InputLabel id={name}>{label}</InputLabel>
        <Select
          labelId={name}
          id={name}
          value={value}
          onChange={(e:any) => handleChange(e.target.value)}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {renderOptions()}
        </Select>
      </FormControl>
      {error && <FormHelperText>{error}</FormHelperText>}
    </Fragment>
  );
}

export const FormikSelectRange = ({ name, setFieldValue, ...rest }: IFormikSelectProps) => (
  <SelectRange
    name={name}
    handleChange={(value:any) => setFieldValue(name, value)}
    {...rest}
  />
)

