import React, { Fragment, memo, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormHelperText, Select } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { IBasicSelectProps } from "./Select.d";
import { Query } from 'react-apollo';

export const SelectMultiple = ({
  name,
  label,
  value,
  errors,
  handleChange,
  filterValue = ({ id, name }: any) => ({ id, name }),
  fetchError,
  loading,
  options = [],
  textChange,
  multiple = false,
}: IBasicSelectProps) => {
  const [open, setOpen] = useState(false);

  const setValue = (e: any, value: any) => {
    if (!!value) {
      if (Array.isArray(value)) {
        console.log("value setValue LOG", value);
        handleChange(value.map((item:any) => filterValue(item)))
      } else {
        handleChange(filterValue(value));
      }
    } else {
      handleChange(null);
    }
  }

  return !fetchError ? (
    <Fragment>
      <Autocomplete
        multiple={multiple}
        id={name}
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        defaultValue={value || []}
        getOptionSelected={(option: any, value: any) => option.name === value.name}
        getOptionLabel={(option: any) => option.name}
        options={options}
        loading={loading}
        onChange={setValue}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            variant="filled"
            size="small"
            onChange={(e: any) => textChange(e.target.value)}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      {errors && <FormHelperText>{errors}</FormHelperText>}
    </Fragment>
  ) : <Alert severity="error">Champ {label} - Erreur Lors du chargement</Alert>;
}

export const SelectMultipleInteractive = ({
  name,
  label,
  value,
  error,
  handleChange=()=>({}),
  filterValue=()=>({}),
  query,
  operation,
  variables,
  params,
  enabled,
  multiple,
  ...rest
}: any) => {
  const [q, setQ]: any = useState("")

  return (
    <Query query={query} variables={variables({ q, ...rest })}>
      {({ loading, error: fetchError, data }: any) => {
        if (fetchError) return <Alert severity="error">Champ {label} - Erreur chargement</Alert>
        return (
          <SelectMultiple
            name={name}
            label={label}
            value={value}
            multiple={multiple}
            errors={fetchError}
            options={data && data[operation].data || []}
            loading={loading}
            handleChange={handleChange}
            filterValue={filterValue}
            textChange={(q: string) => setQ(q)}
            {...rest}
          />
        );
      }}
    </Query>
  );
}

export const FormikSelectMultipleInteractive = ({
  name,
  setFieldValue,
  ...rest
}: any) => (
    <SelectMultipleInteractive
      name={name}
      handleChange={(value:any[]) => setFieldValue(name, value)}
      {...rest}
    />
  )

export const FormikSelectMultiple = ({
  name,
  setFieldValue,
  ...rest
}: any) => (
    <SelectMultiple
      name={name}
      handleChange={(value:any) => setFieldValue(name, value)}
      {...rest}
    />
  )
