import { ISelectAsyncProps } from "./Select.d";
import { Query } from "react-apollo";
import React, { useState } from "react";
import { Alert } from "@material-ui/lab";
import { Select } from "./Select";

export const SelectInteractive = ({
  name,
  label,
  value,
  errors,
  handleChange,
  filterValue,
  query,
  operation,
  variables,
  params,
  enabled,
  multiple
}: ISelectAsyncProps) => {
  const [q, setQ]: any = useState("")

  return (
    <Query query={query} variables={variables({ q, ...params })}>
      {({ loading, error: fetchError, data }: any) => {
        if (fetchError) return <Alert severity="error">Champ {label} - Erreur chargement</Alert>
        return (
          <Select
            name={name}
            label={label}
            value={value}
            multiple={multiple}
            errors={fetchError}
            options={data && data[operation].data}
            loading={loading}
            handleChange={handleChange}
            filterValue={filterValue}
            textChange={(q: string) => setQ(q)}
          />
        );
      }}
    </Query>
  );
}

export const FormikSelectInteractive = ({ name, setFieldValue, ...rest }: any) => (
  <SelectInteractive
    name={name}
    handleChange={(value: any) => setFieldValue(name, value)}
    {...rest}
  />
)