export interface ISelectCommonProps {
  name: string
  label: string
  value: any,
  errors?: any,
  onBlur?: any,
  filterValue?: Function
  fetchError?: any
  loading?: boolean
  options: any[]
  enabled?: boolean
  textChange?: any
  formatInitValue?: any
  multiple?: boolean
  defaultValue?: any
}

export interface IBasicSelectProps extends ISelectCommonProps {
  handleChange: Function
}

export interface ISelectProps extends ISelectCommonProps {
  setFieldValue: Function
}

export interface IAsyncSelectCommonProps extends ISelectCommonProps {
  query?: any
  operation: string
  variables: any
  parentID?: string
  params?: any
}

export interface IFormikSelectProps extends ISelectCommonProps {
  setFieldValue: Function
}

export interface ISelectAsyncProps extends IAsyncSelectCommonProps {
  handleChange: Function
}

export interface IFormikAsyncSelectProps extends IAsyncSelectCommonProps {
  setFieldValue: Function
}