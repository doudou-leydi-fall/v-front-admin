import React from "react";
import { IFormikAsyncSelectProps } from "./Select.d";
import { SelectAsync } from "./SelectAsync";

export const FormikSelectAsync = ({
  name,
  setFieldValue,
  ...rest
}: IFormikAsyncSelectProps) => (
    <SelectAsync
      name={name}
      handleChange={(value: any) => setFieldValue(name, value)}
      {...rest}
    />
  )