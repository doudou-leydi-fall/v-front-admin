import React, { useState, useEffect, Fragment } from "react";
import { Query } from "react-apollo";
import { Alert } from "@material-ui/lab";
import { Select } from "./Select";

export const SelectDoubleAsync = ({
  primary,
  secondary,
  operation,
  query,
  value,
  error,
  params,
  setFieldValue
}: any) => {
  const [primaryValue, setPrimary]: any = useState(value[primary.name]);
  const [secondaryValue, setSecondary]: any = useState(value[secondary.name]);

  useEffect(() => {
    setFieldValue(primary.name, primaryValue);
  }, [primaryValue]);

  useEffect(() => {
    setFieldValue(secondary.name, secondaryValue);
  }, [secondaryValue]);

  return (
    <Fragment>
      <Query query={query} variables={primary.variables({ ...params })}>
        {({ loading, error: fetchError, data }: any) => {
          if (fetchError) return <Alert severity="error">Champ {primary.label} - Erreur chargement</Alert>
          const options = data && data[operation] && data[operation].data || [];

          return (
            <Select
              name={primary.name}
              label={primary.label}
              value={value[primary.name]}
              errors={fetchError}
              options={options}
              loading={loading}
              handleChange={(value:any) => setPrimary(value)}
              filterValue={primary.filterValue}
            />
          );
        }}
      </Query>
      { primaryValue && 
      <Query query={query} variables={secondary.variables({ parentID: primaryValue.id, ...params })}>
        {({ loading, error: fetchError, data }: any) => {
          if (fetchError) return <Alert severity="error">Champ {secondary.label} - Erreur chargement</Alert>
          const options = data && data[operation] && data[operation].data || [];
          
          return (
            <Select
              name={secondary.name}
              label={secondary.label}
              value={value[secondary.name]}
              errors={fetchError}
              options={options}
              loading={loading}
              handleChange={(value:any) => setSecondary(value)}
              filterValue={secondary.filterValue}
            />
          );
        }}
      </Query>}
    </Fragment>
  );
}
