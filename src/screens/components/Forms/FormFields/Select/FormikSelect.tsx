import React from "react";
import { IFormikSelectProps } from "./Select.d";
import { Select } from "./Select";

export const FormikSelect = ({ name, setFieldValue, ...rest }: IFormikSelectProps) => (
  <Select
    name={name}
    handleChange={(value:any) => setFieldValue(name, value)}
    {...rest}
  />
)