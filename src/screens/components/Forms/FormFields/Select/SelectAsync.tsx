import { ISelectAsyncProps } from "./Select.d";
import { Query } from "react-apollo";
import React from "react";
import { Alert } from "@material-ui/lab";
import { Select } from "./Select";

export const SelectAsync = ({
  name,
  label,
  value,
  handleChange,
  filterValue,
  query,
  operation,
  variables,
  params,
}: ISelectAsyncProps) => {

  return (
    <Query query={query} variables={variables(params)}>
      {({ loading, error: fetchError, data }: any) => {
        if (fetchError) return <Alert severity="error">Champ {label} - Erreur chargement</Alert>
        return (
          <Select
            name={name}
            label={label}
            value={value}
            errors={fetchError}
            options={data && data[operation].data}
            loading={loading}
            handleChange={handleChange}
            filterValue={filterValue}
          />
        );
      }}
    </Query>
  );
}