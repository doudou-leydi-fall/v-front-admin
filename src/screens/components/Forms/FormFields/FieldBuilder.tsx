import React, { Fragment, memo } from "react";
import FieldsMap from "./FieldsConfig";

const FieldBuilder = ({
  fieldname,
  name,
  value,
  errors,
  setFieldValue,
  onBlur,
  params,
  ...restProps
}: any) => {
  const { component, ...rest } = FieldsMap[fieldname];
  return (
    <Fragment>
      { React.createElement(component, {
        ...rest,
        name,
        value,
        errors,
        setFieldValue,
        onBlur,
        params,
        ...restProps
      })}
    </Fragment>
  );
}

export default memo(FieldBuilder);