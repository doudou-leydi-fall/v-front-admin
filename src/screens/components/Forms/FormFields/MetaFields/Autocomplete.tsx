import React, { Fragment } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { FormHelperText } from '@material-ui/core';

export const SelectCheck = ({
  name,
  label,
  options,
  getOptionLabel = (option: any) => option.name,
  values,
  error,
  setFieldValue,
  filterValue
}: any) => {
  return (
    <Fragment>
      <Autocomplete
        multiple
        id={name}
        options={options}
        disableCloseOnSelect
        getOptionLabel={getOptionLabel}
        renderOption={(option, { selected }) => (
          <React.Fragment>
            <Checkbox
              icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
              checkedIcon={<CheckBoxIcon fontSize="small" />}
              style={{ marginRight: 8 }}
              checked={selected}
            />
            {getOptionLabel(option)}
          </React.Fragment>
        )}
        renderInput={(params) => (
          <TextField {...params} variant="outlined" label={label} placeholder={label} />
        )}
      />
      {error && <FormHelperText>{error}</FormHelperText>}
    </Fragment>
  );
}

export const SelectCheckAsync = ({
  query,
  operation,
  variables,
}: any) => {
  return (
    <SelectCheck />
  );
};