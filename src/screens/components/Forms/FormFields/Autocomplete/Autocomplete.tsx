import React, { Fragment } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { FormHelperText } from '@material-ui/core';
import FieldsMap from '../FieldsMap';

const FieldsRequiredSelect = ({
  name,
  label,
  options=[],
  value=[],
  error,
  handleChange,
  filterValue = ({ id, name }: any) => ({ id, name }),
}: any) => {

  // const handleChange = (e:any, v:any) => {
  //   if (Array.isArray(v)) {
  //     setFieldValue("childRequirements", v.map(({ name }:any) => ({ name })))
  //   } else {
  //     setFieldValue("childRequirements", []);
  //   }
  // }

  const setValue = (e: any, value: any) => {
    if (!!value) {
      handleChange(filterValue(value))
    } else {
      handleChange(null);
    }
  }

  return (
    <Fragment>
      <Autocomplete
        multiple
        id={name}

        // onChange={(e:any, v:any) => setFieldValue("childRequirements", { name: v.name })}
        onChange={setValue}
        options={options}
        disableCloseOnSelect
        getOptionLabel={(option:any) => option.label}
        renderOption={(option:any, { selected }:any) => (
          <React.Fragment>
            <Checkbox
              icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
              checkedIcon={<CheckBoxIcon fontSize="small" />}
              style={{ marginRight: 8 }}
              checked={selected}
            />
            {option.label}
          </React.Fragment>
        )}
        renderInput={(params) => (
          <TextField {...params} variant="outlined" label={label} placeholder={label} />
        )}
      />
      {error && <FormHelperText>{error}</FormHelperText>}
    </Fragment>
  );
}

export default FieldsRequiredSelect;