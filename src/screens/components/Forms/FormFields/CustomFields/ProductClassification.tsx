import React, { Fragment, useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import { Select } from "../../../../components/Forms/FormFields/Select"
import { TextField } from "../../../../components";
import { useQuery } from "react-apollo";
import { SEARCH_META } from "../../../../../network";

export default ({ value = {}, owner, getCategoryVariables, getCollectionVariables, setFieldValue, type, displayParent = true }: any) => {
  const [qCategory, setQCategory] = useState("");
  const [qCollection, setQCollection] = useState("");
  const [catOptions, setCatOptions]: any = useState([]);
  const [colOptions, setColOptions]: any = useState([]);
  const [col, setCol]: any = useState(value.collectionCatalogue);
  const [categoryCatalogueID, setCategoryCatalogueID]: any = useState(value.categoryCatalogue && value.categoryCatalogue.id);

  const { data: catResponse, loading: loadingCat }: any = useQuery(SEARCH_META, {
    variables: getCategoryVariables({ owner, qCategory, type }),
    fetchPolicy: "no-cache"
  });

  const { data: colResponse, refetch: refetchCol, loading: loadingCol }: any = useQuery(SEARCH_META, {
    variables: getCollectionVariables({ parentID: categoryCatalogueID, owner, qCollection, type })
  });

  const handleCatChange = (value: any) => {
    setCategoryCatalogueID(value && value.id);
    console.log("setCategoryCatalogueID", value);
    setFieldValue("categoryCatalogue", value)
    refetchCol();
  }

  const handleColChange = (value: any) => {
    setFieldValue("collectionCatalogue", value)
  }

  useEffect(() => {
    if (catResponse && catResponse["searchMeta"]) {
      const options = catResponse["searchMeta"].data;
      console.log("CAT LOG", options);
      setCatOptions(options);
    }
  }, [catResponse]);

  useEffect(() => {
    if (colResponse && colResponse["searchMeta"]) {
      const options = colResponse["searchMeta"].data;

      // if (options.length > 1) {
        setColOptions(options);
      // } else {
      //   setColOptions([]);
      // }
    }
  }, [colResponse]);

  return (
    <div>
      <Grid container spacing={1}>
        {displayParent &&
          <Fragment>
            <Grid item sm={6}>
              <TextField
                label="Category rattachée"
                name="category"
                disabled={true}
                value={value.category && value.category.name}
              />
            </Grid>
            <Grid item sm={6}>
              <TextField
                label="Collection rattachée"
                name="collection"
                disabled={true}
                value={value.collection && value.collection.name}
              />
            </Grid>
          </Fragment>
        }
        <Grid item sm={6}>
          <Select
            name="categoryCatalogue"
            label="Catégories"
            value={value.categoryCatalogue}
            defaultValue={value.categoryCatalogue}
            options={catOptions}
            loading={loadingCat}
            handleChange={handleCatChange}
            filterValue={({ id, name }: any) => ({ id, name })}
          />
        </Grid>
        <Grid item sm={6}>
          <Select
            name="collectionCatalogue"
            label="Collection"
            defaultValue={col}
            value={col}
            options={colOptions}
            loading={loadingCol}
            handleChange={handleColChange}
            filterValue={({ id, name }: any) => ({ id, name })}
          />
        </Grid>
      </Grid>
    </div>
  )
}