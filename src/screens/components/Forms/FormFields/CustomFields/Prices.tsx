import React, { useReducer, useEffect, useState } from "react";
import { FormControl, InputLabel, Grid, MenuItem, Select, Button, makeStyles, TextField, FormControlLabel, Checkbox, ButtonGroup } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import StarIcon from "@material-ui/icons/Star";
import EditIcon from "@material-ui/icons/Edit";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

const yup = require("yup");

const periodMap: any = {
  "PERMANENT": "Permanent",
  "HOURLY": "Horaire",
  "DAILY": "Journalier",
  "WEEKLY": "Hebdomadaire",
  "MONTHLY": "Mensuel",
  "ANNUALY": "Annuel",
}

const TYPES: any = {
  ADD_PRICE: "ADD_PRICE",
  DELETE_PRICE: "DELETE_PRICE",
  UPDATE_PRICE: "UPDATE_PRICE",
  EDIT_FIELD: "EDIT_FIELD",
  MAIN_PRICE: "MAIN_PRICE"
}

const initValues = {
  pricing: [],
  errors: []
}

const validator = (state: any, action: any) => {
  let errors: any = [];
  const { price } = action.payload;

  if (price === 0) {
    return { ...state, errors: ["prix"] }
  }
  let newPricing = [...state.pricing, action.payload];
  return { errors, pricing: newPricing.map((pricing: any, id: number) => ({ ...pricing, id: id + 1 })) }
}

const deletePricing = (state: any, action: any) => {
  return { ...state, pricing: state.pricing.filter((pricing: any) => pricing.id !== action.payload) };
}

const updatePricing = (state: any, action: any) => {
  return {
    ...state, pricing: state.pricing.map((pricing: any) => {
      if (pricing.id === action.payload.id) {
        return action.payload;
      }
      return pricing;
    })
  };
}

const updateField = (state: any, { payload: { id, field, value } }: any) => {
  return {
    ...state, pricing: state.pricing.map((pricing: any) => {
      if (pricing.id === id) {
        pricing[field] = value;
      }
      return pricing;
    })
  };
}

const defineMainPrice = (state: any, { payload }: any) => {
  return {
    ...state, pricing: state.pricing.map((pricing: any) => {
      if (pricing.id === payload) {
        return { ...pricing, main: true };
      }
      return { ...pricing, main: false };;
    })
  };
}

const reducers: any = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.ADD_PRICE:
      return validator(state, action);
    case TYPES.DELETE_PRICE:
      return deletePricing(state, action);
    case TYPES.UPDATE_PRICE:
      return updatePricing(state, action);
    case TYPES.EDIT_FIELD:
      return updateField(state, action);
    case TYPES.MAIN_PRICE:
      return defineMainPrice(state, action);
    default:
      return state
  }
}

export default (props: any) => {
  const [state, dispatch]: any = useReducer(reducers, {
    pricing: props.value || [],
    errors: []
  });

  useEffect(() => {
    props.setFieldValue("pricing", state.pricing);
  }, [state.pricing]);

  return (
    <div>
      {state.pricing.map((singlePricing: any) => <PricingUpdateForm key={singlePricing.id} initPricing={singlePricing} {...props} state={state} dispatch={dispatch} />)}
      <PricingForm {...props} state={state} dispatch={dispatch} />
    </div>
  );
}

const PricingUpdateForm = ({ initPricing={}, size: { options: sizeOptions = [] }, period: { options: periodOptions = [] }, currency: { options: currencyOptions = [] }, state, dispatch }: any) => {
  const classes = styles();
  const [interval, enableInterval] = useState(false);
  const [disabled, disableEdit]: any = useState(true);
  const [pricing, setPricing]:any = useState(initPricing);

  useEffect(() => {
    setPricing(initPricing)
    disableEdit(true);
  }, [initPricing]);

  return (
    <div className={classes.root}>
      { state.errors.length > 0 && <Alert className={classes.alert} severity="error">
        <AlertTitle>Error: Les champs suivants sont obligatoires</AlertTitle>
        <span>{state.errors.join(" - ")}</span>
      </Alert>}
      <Grid container>
        <Grid container sm={6}>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              disabled={disabled}
              type="number"
              id="price"
              fullWidth
              size="small"
              variant="filled"
              label={state.errors["price"] || "Prix"}
              value={pricing.price}
              onChange={(e) => setPricing({ ...pricing, price: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              disabled={disabled}
              id="name"
              fullWidth
              size="small"
              variant="filled"
              label={state.errors["name"] || "Nom"}
              value={pricing.name}
              onChange={(e) => setPricing({ ...pricing, name: e.target.value })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="currency">Devise</InputLabel>
              <Select
                disabled={disabled}
                value={pricing.currency}
                labelId="currency"
                id="currency"
                label={state.errors["currency"] || "DEVISE"}
                onChange={(e) => setPricing({ ...pricing, currency: e.target.value })}
              >
                {currencyOptions.map((options: any) => <MenuItem key={options} value={options}>{options}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              disabled={disabled}
              id="quantity"
              fullWidth
              type="number"
              variant="filled"
              size="small"
              placeholder=""
              label={state.errors["quantity"] || "Quantité"}
              value={pricing.quantity}
              onChange={(e) => setPricing({ ...pricing, quantity: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="period">{state.errors["period"] || "Periode"}</InputLabel>
              <Select
                disabled={disabled}
                labelId="period"
                id="period"
                value={pricing.period}
                defaultValue={pricing.period}
                onChange={(e) => setPricing({ ...pricing, period: e.target.value })}
              >
                {periodOptions.map((options: any) => <MenuItem key={options} value={options}>{periodMap[options]}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="size">{state.errors["size"] || "Size"}</InputLabel>
              <Select
                disabled={disabled}
                labelId="size"
                id="size"
                defaultValue={pricing.size}
                onChange={(e) => setPricing({ ...pricing, size: e.target.value })}
              >
                {sizeOptions.map((options: any) => <MenuItem key={options} value={options}>{options}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="discount"
              disabled={disabled}
              fullWidth
              type="number"
              size="small"
              variant="filled"
              label="discount"
              value={pricing.discount}
              onChange={(e) => setPricing({ ...pricing, discount: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <FormControlLabel
              control={<Checkbox checked={state.checkedA} onChange={(e, checked) => enableInterval(checked)} name="checkedA" />}
              label="Programme (Cocher si le prix dépend d'une intervalle de temps)"
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <TextField
              id="start-date"
              disabled={!interval}
              label={state.errors["startDate"] || "Début"}
              fullWidth
              variant="filled"
              type="datetime-local"
              value={pricing.startDate}
              defaultValue={pricing.startDate}
              onChange={(e) => setPricing({ ...pricing, startDate: e.target.value })}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <TextField
              id="end-date"
              disabled={!interval}
              label={state.errors["endDate"] || "Date de fin"}
              fullWidth
              variant="filled"
              type="datetime-local"
              defaultValue={pricing.endDate}
              value={pricing.endDate}
              onChange={(e) => setPricing({ ...pricing, endDate: e.target.value })}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
        </Grid>
        <Grid item sm={6}>
          <TextField
            id="price-comment"
            label="Commentaires sur le prix"
            disabled={disabled}
            fullWidth
            multiline
            rows={15}
            defaultValue={pricing.comment}
            variant="outlined"
            onChange={(e) => setPricing({ ...pricing, comment: e.target.value })}
          />
        </Grid>

        <Grid item sm={12} className={classes.btnAddContainer}>
          <ButtonGroup size="small" color="primary" fullWidth>
            <Button onClick={() => dispatch({ type: TYPES.DELETE_PRICE, payload: pricing.id })} fullWidth size="large" variant="contained" color="default" startIcon={<DeleteIcon />}>Supprimer</Button>
            {disabled ? <Button onClick={() => disableEdit(false)} variant="contained" color="primary" startIcon={<EditIcon />}>Editer</Button> : 
            <Button onClick={() => dispatch({ type: TYPES.UPDATE_PRICE, payload: pricing })} variant="contained" color="secondary" startIcon={<EditIcon />}>Mettre à jour</Button>}
            <Button onClick={() => dispatch({ type: TYPES.MAIN_PRICE, payload: pricing.id })} variant="contained" color={pricing.main ? "primary" : "secondary"} startIcon={pricing.main ? <StarIcon color="secondary"/> : <EditIcon />}>{ pricing.main ? "Prix principal" : "Definir comme prix principal"}</Button>
          </ButtonGroup>
        </Grid>
      </Grid>
    </div>
  );
}

const PricingForm = ({ name, size: { options: sizeOptions = [] }, period: { options: periodOptions = [] }, currency: { options: currencyOptions = [] }, state, dispatch }: any) => {
  const classes = styles();
  const [pricing, setPricingAttribute]: any = useState({ price: 0, currency: "FCFA", name: "", quantity: 1, size: null, period: "PERMANENT", startDate: null, endDate: null, main: false });
  const [interval, enableInterval] = useState(false);

  return (
    <div className={classes.root}>
      { state.errors.length > 0 && <Alert className={classes.alert} severity="error">
        <AlertTitle>Error: Les champs suivants sont obligatoires</AlertTitle>
        <span>{state.errors.join(" - ")}</span>
      </Alert>}
      <Grid container>
        <Grid container sm={6}>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="price"
              type="number"
              fullWidth
              size="small"
              variant="filled"
              label={state.errors["price"] || "Prix"}
              value={pricing.price}
              onChange={(e) => setPricingAttribute({ ...pricing, price: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="name"
              fullWidth
              size="small"
              variant="filled"
              label={state.errors["name"] || "Nom"}
              value={pricing.name}
              onChange={(e) => setPricingAttribute({ ...pricing, name: e.target.value })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="currency">Devise</InputLabel>
              <Select
                value={pricing.currency}
                labelId="currency"
                id="currency"
                label={state.errors["currency"] || "DEVISE"}
                onChange={(e) => setPricingAttribute({ ...pricing, currency: e.target.value })}
              >
                {currencyOptions.map((options: any) => <MenuItem key={options} value={options}>{options}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="quantity"
              fullWidth
              type="number"
              variant="filled"
              size="small"
              placeholder=""
              label={state.errors["quantity"] || "Quantité"}
              value={pricing.quantity}
              onChange={(e: any) => setPricingAttribute({ ...pricing, quantity: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="period">{state.errors["period"] || "Periode"}</InputLabel>
              <Select
                labelId="period"
                id="period"
                value={pricing.period}
                defaultValue={pricing.period}
                onChange={(e) => setPricingAttribute({ ...pricing, period: e.target.value })}
              >
                {periodOptions.map((options: any) => <MenuItem key={options} value={options}>{periodMap[options]}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <FormControl size="small" fullWidth variant="filled">
              <InputLabel id="size">{state.errors["size"] || "Size"}</InputLabel>
              <Select
                labelId="size"
                id="size"
                value={pricing.size}
                onChange={(e) => setPricingAttribute({ ...pricing, size: e.target.value })}
              >
                {sizeOptions.map((options: any) => <MenuItem key={options} value={options}>{options}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="discount"
              fullWidth
              type="number"
              size="small"
              variant="filled"
              label={state.errors["discount"] || "discount"}
              value={pricing.discount}
              onChange={(e) => setPricingAttribute({ ...pricing, discount: parseFloat(e.target.value) })}
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <FormControlLabel
              control={<Checkbox checked={state.checkedA} onChange={(e, checked) => enableInterval(checked)} name="checkedA" />}
              label="Programme (Cocher si le prix dépend d'une intervalle de temps)"
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <TextField
              id="start-date"
              disabled={!interval}
              label={state.errors["startDate"] || "Début"}
              fullWidth
              variant="filled"
              type="datetime-local"
              value={pricing.startDate}
              onChange={(e) => setPricingAttribute({ ...pricing, startDate: e.target.value })}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item sm={12} className={classes.gridItem}>
            <TextField
              id="end-date"
              disabled={!interval}
              label={state.errors["endDate"] || "Date de fin"}
              fullWidth
              variant="filled"
              type="datetime-local"
              className={classes.resume}
              value={pricing.endDate}
              onChange={(e) => setPricingAttribute({ ...pricing, endDate: e.target.value })}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
        </Grid>
        <Grid item sm={6}>
          <TextField
            id="price-comment"
            label={state.errors["note"] || "Commentaires sur le prix"}
            fullWidth
            multiline
            rows={12}
            defaultValue=""
            variant="outlined"
            onChange={(e) => setPricingAttribute({ ...pricing, comment: e.target.value })}
          />
        </Grid>

        <Grid item sm={12} className={classes.btnAddContainer}>
          <Button onClick={() => dispatch({ type: TYPES.ADD_PRICE, payload: pricing })} fullWidth size="large" variant="contained" color="default" startIcon={<AddIcon />}>Ajouter Prix</Button>
        </Grid>
      </Grid>
    </div>
  );
}

const styles = makeStyles(() => ({
  root: {
    background: "#FFFFFF",
    padding: 20,
    marginBottom: 20,
    borderRadius: 12
  },
  btnAddContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  gridItem: {
    paddingLeft: 2,
    paddingRight: 2
  },
  resume: {
    paddingLeft: 5
  },
  alert: {
    marginBottom: 10
  }
}));