import React, { useReducer, useEffect, useState } from "react";
import { Grid, MenuItem, Select, Button, makeStyles, TextField, FormControlLabel, Checkbox, ButtonGroup } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import StarIcon from "@material-ui/icons/Star";
import EditIcon from "@material-ui/icons/Edit";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

const yup = require("yup");

const periodMap: any = {
  "PERMANENT": "Permanent",
  "HOURLY": "Horaire",
  "DAILY": "Journalier",
  "WEEKLY": "Hebdomadaire",
  "MONTHLY": "Mensuel",
  "ANNUALY": "Annuel",
}

const TYPES: any = {
  ADD_ATTRIBUTE: "ADD_ATTRIBUTE",
  DELETE_ATTRIBUTE: "DELETE_ATTRIBUTE",
  UPDATE_ATTRIBUTE: "UPDATE_ATTRIBUTE",
  EDIT_FIELD: "EDIT_FIELD",
  MAIN_ATTRIBUTE: "MAIN_ATTRIBUTE"
}

const initValues = {
  attributes: [],
  errors: []
}

const validator = (state: any, action: any) => {
  let errors: any = [];
  const { name } = action.payload;

  let newAttributes = [...state.attributes, action.payload];
  return { errors, attributes: newAttributes.map((attributes: any, id: number) => ({ ...attributes, id: id + 1 })) }
}

const deleteAttributes = (state: any, action: any) => {
  return { ...state, attributes: state.attributes.filter((attributes: any) => attributes.id !== action.payload) };
}

const updateAttributes = (state: any, action: any) => {
  return {
    ...state, attributes: state.attributes.map((attributes: any) => {
      if (attributes.id === action.payload.id) {
        return action.payload;
      }
      return attributes;
    })
  };
}

const updateField = (state: any, { payload: { id, field, value } }: any) => {
  return {
    ...state, attributes: state.attributes.map((attributes: any) => {
      if (attributes.id === id) {
        attributes[field] = value;
      }
      return attributes;
    })
  };
}

const reducers: any = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.ADD_ATTRIBUTE:
      return validator(state, action);
    case TYPES.DELETE_ATTRIBUTE:
      return deleteAttributes(state, action);
    case TYPES.UPDATE_ATTRIBUTE:
      return updateAttributes(state, action);
    case TYPES.EDIT_FIELD:
      return updateField(state, action);
    default:
      return state
  }
}

export default (props: any) => {
  const [state, dispatch]: any = useReducer(reducers, {
    attributes: props.value || [],
    errors: []
  });

  useEffect(() => {
    console.log("useEffect LOG", state.attributes);
    props.setFieldValue("sheet", state.attributes);
  }, [state.attributes]);

  return (
    <div>
      {state.attributes.map((attr: any) => <UpdateForm key={attr.id} initAttr={attr} {...props} state={state} dispatch={dispatch} />)}
      <AttributeForm {...props} state={state} dispatch={dispatch} />
    </div>
  );
}

const UpdateForm = ({ initAttr = {}, state, dispatch }: any) => {
  const classes = styles();
  const [disabled, disableEdit]: any = useState(true);
  const [attributes, setAttribute]: any = useState(initAttr);

  useEffect(() => {
    setAttribute(initAttr)
    disableEdit(true);
  }, [initAttr]);

  return (
    <div className={classes.root}>
      { state.errors.length > 0 && <Alert className={classes.alert} severity="error">
        <AlertTitle>Error: Les champs suivants sont obligatoires</AlertTitle>
        <span>{state.errors.join(" - ")}</span>
      </Alert>}
      <Grid container>
        <Grid container sm={12}>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="name"
              name="name"
              size="small"
              disabled={disabled}
              fullWidth
              variant="filled"
              label={state.errors["name"] || "Nom"}
              value={attributes.name}
              onChange={(e) => setAttribute({ ...attributes, name: e.target.value })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="value"
              name="value"
              disabled={disabled}
              fullWidth
              size="small"
              variant="filled"
              label={state.errors["value"] || "Valeur"}
              value={attributes.value}
              onChange={(e) => setAttribute({ ...attributes, value: e.target.value })}
            />
          </Grid>
        </Grid>
        <Grid item sm={12}>
          <TextField
            id="comment"
            label="Commentaires sur le prix"
            disabled={disabled}
            fullWidth
            multiline
            rows={4}
            defaultValue={attributes.comment}
            variant="outlined"
            onChange={(e) => setAttribute({ ...attributes, comment: e.target.value })}
          />
        </Grid>
        <Grid item sm={12} className={classes.btnAddContainer}>
          <ButtonGroup size="small" color="primary" fullWidth>
            <Button onClick={() => dispatch({ type: TYPES.DELETE_ATTRIBUTE, payload: attributes.id })} fullWidth size="large" variant="contained" color="default" startIcon={<DeleteIcon />}>Supprimer</Button>
            {disabled ? <Button onClick={() => disableEdit(false)} variant="contained" color="primary" startIcon={<EditIcon />}>Editer</Button> :
              <Button onClick={() => dispatch({ type: TYPES.UPDATE_ATTRIBUTE, payload: attributes })} variant="contained" color="secondary" startIcon={<EditIcon />}>Mettre à jour</Button>}
          </ButtonGroup>
        </Grid>
      </Grid>
    </div>
  );
}

const AttributeForm = ({ name, state, dispatch }: any) => {
  const classes = styles();
  const [attributes, setAttribute]: any = useState({ name: "", value: "", comment: "" });

  return (
    <div className={classes.root}>
      { state.errors.length > 0 && <Alert className={classes.alert} severity="error">
        <AlertTitle>Error: Les champs suivants sont obligatoires</AlertTitle>
        <span>{state.errors.join(" - ")}</span>
      </Alert>}
      <Grid container>
        <Grid container sm={12}>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="name"
              name="name"
              size="small"
              variant="filled"
              fullWidth
              label={state.errors["name"] || "Nom"}
              value={attributes.name}
              onChange={(e) => setAttribute({ ...attributes, name: e.target.value })}
            />
          </Grid>
          <Grid item sm={6} className={classes.gridItem}>
            <TextField
              id="value"
              name="value"
              size="small"
              variant="filled"
              fullWidth
              label={state.errors["value"] || "Valeur"}
              value={attributes.value}
              onChange={(e) => setAttribute({ ...attributes, value: e.target.value })}
            />
          </Grid>
        </Grid>
        <Grid item sm={12}>
          <TextField
            id="comment"
            name="comment"
            label={state.errors["comment"] || "Commentaires"}
            fullWidth
            multiline
            rows={4}
            defaultValue=""
            variant="outlined"
            onChange={(e) => setAttribute({ ...attributes, comment: e.target.value })}
          />
        </Grid>

        <Grid item sm={12} className={classes.btnAddContainer}>
          <Button onClick={() => dispatch({ type: TYPES.ADD_ATTRIBUTE, payload: attributes })} fullWidth size="large" variant="contained" color="default" startIcon={<AddIcon />}>Ajouter</Button>
        </Grid>
      </Grid>
    </div>
  );
}

const styles = makeStyles(() => ({
  root: {
    background: "#FFFFFF",
    padding: 20,
    marginBottom: 20,
    borderRadius: 12
  },
  btnAddContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  gridItem: {
    paddingLeft: 2,
    paddingRight: 2
  },
  resume: {
    paddingLeft: 5
  },
  alert: {
    marginBottom: 10
  }
}));