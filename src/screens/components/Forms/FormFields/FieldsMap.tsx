import React from "react";
import { SelectDoubleAsync, FormikSelect } from "./Select";
import { IMeta } from "./MetaFields/Meta.defs";
import { GET_META_LIST, SEARCH_META } from "../../../../network";
import { FormikSelectMultipleInteractive } from "./Select";
import { FormikSelectInteractive } from "./Select/SelectInteractive";
import { FormikSelectRange } from "./Select/SelectRange";

const FieldsMap: any = {
  "catcol": {
    label: "Catégory - Collection",
    query: SEARCH_META,
    operation: "searchMeta",
    component: SelectDoubleAsync,
    primary: {
      type: "SELECT",
      name: "category",
      label: "Catégories",
      variables: ({ q, owner }: any) => ({
        model: "Category",
        type: "prefixSearch",
        q,
        criteria:[
          { 
            name: "type", 
            value: "FOOD"
          }, {
            name: "model", 
            value: "Category"
          },
           {
            name: "owner", 
            value: owner
          }
        ]
      }),
      filterValue: ({ id, name }: IMeta) => ({ id, name }),
    },
    secondary: {
      type: "SELECT",
      name: "collection",
      label: "Collection",
      variables: ({ q, owner, parentID }: any) => ({
        model: "Collection",
        type: "prefixSearch",
        q,
        criteria:[
          { 
            name: "type", 
            value: "FOOD"
          }, {
            name: "model", 
            value: "Collection"
          },
           {
            name: "owner", 
            value: owner
          },
          {
            name: "parentID", 
            value: parentID
          }
        ]
      }),
      filterValue: ({ id, name }: IMeta) => ({ id, name }),
    }
  },

  "cardType": {
    type: "SELECT",
    label: "Type de carte",
    options: [
      {
        id: "RectangleCardSmall",
        name: "RectangleCardSmall"
      },
      {
        id: "RectangleCardMedium",
        name: "RectangleCardMedium"
      },
      {
        id: "RectangleCardLarge",
        name: "RectangleCardLarge"
      },
      {
        id: "SquareCardSmall",
        name: "SquareCardSmall"
      },
      {
        id: "SquareCardMedium",
        name: "SquareCardMedium"
      },
      {
        id: "SquareCardLarge",
        name: "SquareCardLarge"
      },
      {
        id: "RoundedCardSmall",
        name: "RoundedCardSmall"
      },
      {
        id: "RoundedCardMedium",
        name: "RoundedCardMedium"
      },
      {
        id: "RoundedCardLarge",
        name: "RoundedCardLarge"
      },
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },

  "classificationType": {
    type: "SELECT",
    label: "Type",
    options: [
      {
        id: "FOOD",
        name: "Alimentaire"
      }
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },

  "recipes": {
    label: "Recettes",
    multiple: true,
    query: SEARCH_META,
    operation: "searchMeta",
    component: FormikSelectMultipleInteractive,
    variables: ({ q }: any) => ({
      model: "Composition",
      type: "prefixSearch",
      criteria:[
        { 
          name: "type", 
          value: "FOOD"
        }, {
          name: "model", 
          value: "Element"
        },
      ],
      q
    }),
    filterValue: ({ id, name }: IMeta) => ({ id, name }),
  },

  "rootCategory": {
    label: "Categorie parent",
    multiple: true,
    query: SEARCH_META,
    operation: "searchMeta",
    component: FormikSelectInteractive,
    variables: ({ q }: any) => ({
      model: "Category",
      type: "prefixSearch",
      q,
      criteria:[
        { 
          name: "type", 
          value: "FOOD"
        }, {
          name: "model", 
          value: "Category"
        },
         {
          name: "owner", 
          value: "root"
        }
      ]
    }),
    filterValue: ({ id, name }: IMeta) => ({ id, name }),
  },

  "rootCollection": {
    label: "Collection parent",
    multiple: true,
    query: SEARCH_META,
    operation: "searchMeta",
    component: FormikSelectInteractive,
    variables: ({ q, parentID }: any) => ({
      model: "Collection",
      type: "prefixSearch",
      q,
      criteria:[
        { 
          name: "type", 
          value: "FOOD"
        }, {
          name: "model", 
          value: "Collection"
        },
         {
          name: "owner", 
          value: "root"
        },
        {
          name: "parentID", 
          value: parentID
        }
      ]
    }),
    filterValue: ({ id, name }: IMeta) => ({ id, name }),
  },

  "sectionTheme": {
    type: "SELECT",
    label: "Theme",
    options: [
      {
        id: "LIGHT",
        name: "LIGHT"
      },
      {
        id: "DARK",
        name: "DARK"
      },
      {
        id: "DARKONLIGHT",
        name: "DARKONLIGHT"
      },
      {
        id: "LIGHTONDARK",
        name: "LIGHTONDARK"
      },
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },

  "sectiondatamodel": {
    type: "SELECT",
    label: "Model de donnée",
    options: [
      {
        id: "Product",
        name: "Product"
      },
      {
        id: "Event",
        name: "Event"
      },
      {
        id: "Place",
        name: "Place"
      },
      {
        id: "Profile",
        name: "Profile"
      },
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },

  "screen": {
    type: "SELECT",
    label: "Écran",
    options: [
      {
        id: "FOOD-HOME",
        name: "FOOD-HOME"
      },
      {
        id: "PROGRAM-HOME",
        name: "PROGRAM-HOME"
      }
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },
  
  "FontSize": {
    type: "SELECT",
    label: "Taille Police du sous-titre",
    optionsRange: {
      min: 11,
      max: 100,
    },
    component: FormikSelectRange,
    filterValue: ({ id }: IMeta) => id,
  },

  "FontFamily": {
    type: "SELECT",
    label: "Font Family",
    options: [
      {
        id: "FUTURA_MEDIUM",
        name: "FUTURA_MEDIUM"
      },
      {
        id: "SANSATION_REGULAR",
        name: "SANSATION_REGULAR"
      },
      {
        id: "AVENIR",
        name: "AVENIR"
      },
      {
        id: "AVENIR_ITALIC",
        name: "AVENIR_ITALIC"
      },
      {
        id: "AVENIR_HEAVYITALIC",
        name: "AVENIR_HEAVYITALIC"
      },
      {
        id: "AVENIR_HEAVY",
        name: "AVENIR_HEAVY"
      },
      {
        id: "AVENIR_BLACKITALIC",
        name: "AVENIR_BLACKITALIC"
      },
      {
        id: "AVENIR_BOOKITALIC",
        name: "AVENIR_BOOKITALIC"
      },
      {
        id: "LEARNING_CURVE",
        name: "LEARNING_CURVE"
      },
      {
        id: "LEARNING_CURVE_BOLD",
        name: "LEARNING_CURVE_BOLD"
      },
      {
        id: "MENTO",
        name: "MENTO"
      },
      {
        id: "SANSATION_REGULAR",
        name: "SANSATION_REGULAR"
      }
    ],
    component: FormikSelect,
    filterValue: ({ id }: IMeta) => id,
  },
}

export default FieldsMap;