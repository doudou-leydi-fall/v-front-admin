import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

export default ({ 
  name,
  label,
  value=[],
  placeholder,
  options,
  setFieldValue,
  getOptionLabel,
  getOptionValue,
  formatInitValue,
  error,
  freeSolo=true
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        id={name}
        options={options || []}
        defaultValue={Array.isArray(value) && value.map(formatInitValue)}
        onChange={(e, v) => setFieldValue(name, v.map(getOptionValue))}
        getOptionLabel={getOptionLabel}
        freeSolo={freeSolo}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label={label}
            placeholder={placeholder}
            helperText={error}
          />
        )}
      />
    </div>
  );
}
