
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Checkbox } from "@material-ui/core";

export default ({
  name,
  label,
  value,
  setFieldValue
}) => {
  const classes = useStyles();

  return ( 
    <div variant="filled" className={classes.root}>
      <Typography variant="p">{label}</Typography>
      <Checkbox 
        name={name}
        checked={value}
        onChange={(e, v) => setFieldValue(name, v)}
      />
    </div>
)};

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 13,
    background: "transparent",
    height:46,
    border: "1px solid #D3D3D3"
  }
}));
