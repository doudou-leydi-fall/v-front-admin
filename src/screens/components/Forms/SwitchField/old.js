
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Switch,
  Typography
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 10,
    paddingLeft: 13,
    background: "transparent",
    height:46,
    border: "1px solid #D3D3D3"
  }
}));

export const SwitchField = ({
  label,
  name,
  value,
  setFieldValue,
  error
}) => {
  const classes = useStyles();

  return ( 
    <div variant="filled" className={classes.root}>
      <Typography variant="p">{label}</Typography>
      <Switch
        name={name}
        checked={value}
        onChange={(e, v) => setFieldValue(name, v)}
        value={value}
        />
    </div>
)
};