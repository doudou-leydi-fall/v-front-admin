import React from "react";
import TextField from "@material-ui/core/TextField";

export default ({
  name,
  label,
  value,
  type,
  onChange,
  onBlur,
  error,
  multiline,
  rows,
  disabled=false
}) => (
    <TextField
      onChange={onChange}
      onBlur={onBlur}
      value={value}
      name={name}
      label={label}
      placeholder={label}
      helperText={error}
      type={type}
      variant="filled"
      size="small"
      fullWidth
      multiline={multiline}
      rows={rows}
      disabled={disabled}
    />
  );