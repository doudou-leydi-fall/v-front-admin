import React, { useEffect, useMemo, useState, useReducer } from 'react';
export const FormControlContext = React.createContext();

const FormControlContextProvider = ({ children }) => {
  console.log("ContextProvider");
  const [inputs, dispatch] = useReducer((inputs, action) => {
    console.log("reducer action LOG", action);
    switch (action.type) {
      case 'ADD_PRICE_LABEL':
        console.log("priceLabel", inputs);
        const { index, label, value} = action.payload;
        inputs[index] = {index, name: label, value };
        inputs[index + 1] = {index: index + 1, name: "", value: ""};
        return inputs;
      case 'ADD_PRICE_VALUE':
        inputs[action.index] = {...inputs[action.index], value: action.payload };
        console.log("priceValue", inputs);
        inputs[index + 1] = {index: index + 1, name: "", value: ""};
        return inputs;
      case 'ADD_INPUT':
        console.log("ADD_INPUT");
        inputs[inputs.length] = { name: "", value: null };
        console.log("ADD_INPUT", inputs);
        inputs[index + 1] = {index: index + 1, name: "", value: ""};
        return inputs;
      default:
        return inputs;
    }
  }, [{name: "", value: null }]);

  useEffect(
    () => {
      console.log("input changedd", inputs);
    },
    [inputs]
  );
  const contextValue = useMemo(() => {
    return { inputs, dispatch };
  }, [inputs, dispatch]);

  return (
    <FormControlContext.Provider value={contextValue}>
      {children}
    </FormControlContext.Provider>
  );
};

export const FormControlConsumer = FormControlContext.Consumer;

export default FormControlContextProvider;