import React, { useState, Fragment } from "react";
import TextField from "./simple";

export const TextFieldMutli = () => {
  const [ inputs, setInputs ] = useState([{class: "", value: null }]);

  return (
    <Fragment>
      {inputs.map(input => <TextField/>)}
    </Fragment>
  );
}