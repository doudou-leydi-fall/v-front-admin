import React, { useState, useEffect } from "react";
import { TextField, IconButton, Button, makeStyles } from "@material-ui/core";
import { HighlightOff } from '@material-ui/icons';

export const PricesField = props => {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [value, setValue] = useState(0);
  const [prices, setPrices] = useState([]);
  
  const addPrice = () => {
    const newPrices = [...prices, {name, value}];
    setPrices(newPrices);
  }

  const removePrice = (key) => {
    const newPrices = prices.filter((price, index) => index != key);
    setPrices(newPrices);
  }

  useEffect(() => {
    console.log("useEffect Log prices", prices);
    props.setFieldValue("prices", prices);
  }, [prices]);

  return (
    <div className={classes.container}>
      <div className={classes.inputsContainer}>
        <TextField
          className={classes.inputLeft}
          onChange={(e) => setName(e.target.value)}
          onBlur={props.onBlur}
          value={name}
          name={props.name}
          label={"Categorie"}
          placeholder={props.label}
          type={props.type}
          variant="outlined"
          helperText={props.error}
          size="small"
          multiline={props.multiline}
          rows={props.rows}
        />
        <TextField
          type="number"
          className={classes.inputRight}
          onChange={(e) => setValue(parseFloat(e.target.value))}
          onBlur={props.onBlur}
          value={value}
          name={props.name}
          label={"Prix"}
          placeholder={props.label}
          type={props.type}
          variant="outlined"
          helperText={props.error}
          size="small"
          multiline={props.multiline}
          rows={props.rows}
        />
            {/*_renderButton(inputs[key].displayMode, dispatch, key)*/}
        </div>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          classes={classes} 
          onClick={(e) => addPrice()}>
            Ajouter
        </Button>
        <PriceList prices={prices} removePrice={removePrice} classes={classes}/>
      </div>
  );
}

const PriceList = ({prices, classes, removePrice}) => (
  <div className={classes.priceList}>
    {
      prices.map((price, key) => <div className={classes.priceItem}>
        <div className={classes.itemContent}>
          <div className={classes.priceName}>{price.name}</div>
          <div className={classes.priceValue}>{price.value}</div>
        </div>
        <div className={classes.itemBtn}>
          <IconButton onClick={() => removePrice(key)} color="secondary" aria-label="add an alarm">
            <HighlightOff />
          </IconButton>
        </div>
      </div>)
    }
  </div>
);

const useStyles = makeStyles(theme => ({
  container: {
    
  },
  actionButton: { 
    marginLeft: 3
  },
  button: {
    width: "100%"
  },
  inputsContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputLeft: {
    width: "45%"
  },
  inputRight: {
    width: "55%",
    marginLeft: 5
  },
  priceList: {

  },
  priceItem: {
    height: 40,
    display: "flex",
    color: "#4A148C",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    border: "1px solid #E2E2E2",
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 5,
    borderRadius: 4
  },
  itemContent: {
    width: "90%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  itemBtn: {
    width: "10%"
  }
}));