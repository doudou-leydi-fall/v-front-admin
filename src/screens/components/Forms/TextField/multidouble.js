import React, { Fragment, useState, useContext, useReducer, useEffect } from "react";
import { TextField, ButtonGroup,Button, makeStyles } from "@material-ui/core";
import { Add, Edit, HighlightOff, CheckCircle } from '@material-ui/icons';
import FormControlContextProvider, {FormControlContext} from "./store";

export const TextFieldMutliDouble = props => {
  //const {inputs, dispatch} = useContext(FormControlContext);
  const reducers = (state, action) => {
    switch (action.type) {
      case 'ADD_NAME':
        state[action.payload.index] = {...state[action.payload.index],name: action.payload.name, index: action.payload.index, btnMode: "editMode"};
        return state;
      case 'ADD_INPUT':
        console.log("ADD_INPUT", action.payload.index);
        state[action.payload.index] = {...state[action.payload.index], btnMode: "displayMode"};
        state[action.payload.index + 1] = {name: "", value: 0, index: action.payload.index + 1, btnMode: "editMode"}
        console.log("state", state);
        //props.setFieldValue("prices", state.map(price => ({name: price.name, value: price.value})));
        return state;
      case 'ADD_VALUE':
        console.log("priceLabel", state);
        state[action.payload.index] = {...state[action.payload.index], value: action.payload.value};
        //inputs[index + 1] = {index: index + 1, name: "", value: ""};
        return state;
      default:
        return state;
    }
  }
  const [inputs, dispatch] = useReducer(reducers, [{name: "", value: 0, index: 0, btnMode: "editMode"}]);

  const [] = useState();
  const classes = useStyles();

  const _renderButton = (btnMode, dispatch, index) => {
    switch(btnMode) {
      case "displayMode":
        return <BtnGroupDisplay classes={classes} dispatch={dispatch} index={index}/>
        break;
      case "addMode":
        return <BtnGroupAdd classes={classes} dispatch={dispatch} index={index}/>
        break;
      case "editMode":
        return <BtnGroupEdit classes={classes} dispatch={dispatch} index={index}/>
        break;
      default:
        return <BtnGroupAdd classes={classes} dispatch={dispatch} index={index}/>
    }
  }

  useEffect(() => {
    console.log("useEffect", inputs);
  });

  return (
    <div>
      hello
      {
        inputs.map((input, key) => (
          <div className={classes.inputsContainer}>
            <TextField
              key={key}
              className={classes.inputLeft}
              onChange={(e) => dispatch({type: "ADD_NAME", payload: {name: e.target.value, index: key}})}
              onBlur={props.onBlur}
              value={props.value}
              name={props.name}
              label={props.label}
              placeholder={props.label}
              type={props.type}
              variant="filled"
              helperText={props.error}
              size="small"
              multiline={props.multiline}
              rows={props.rows}
            
            />
            <TextField
              className={classes.inputRight}
              onChange={(e) => dispatch({type: "ADD_VALUE", payload: {value: e.target.value, index: key}})}
              onBlur={props.onBlur}
              value={props.value}
              name={props.name}
              label={props.label}
              placeholder={props.label}
              type={props.type}
              variant="filled"
              helperText={props.error}
              size="small"
              multiline={props.multiline}
              rows={props.rows}
            />
            {/*_renderButton(inputs[key].displayMode, dispatch, key)*/}
            <BtnGroupAdd classes={classes} dispatch={dispatch} index={key}/>
          </div>
          
        ))
      }
    </div>
  );
}

const BtnGroupDisplay = ({classes, dispatch}) => (
  <ButtonGroup 
    color="primary" 
    size="large"
    className={classes.actionButton} 
    aria-label="outlined primary button group">
    <Button
      variant="contained"
      color="primary"
      onClick={() => dispatch()}
    >
      <Edit/>
    </Button>
    <Button onClick={() => dispatch()}><HighlightOff/></Button>
  </ButtonGroup>
);

const BtnGroupAdd = ({classes, dispatch, index}) => (
  <ButtonGroup 
    color="primary" 
    size="large" 
    className={classes.actionButton} 
    aria-label="outlined primary button group">
    <Button
      variant="contained"
      color="primary"
      onClick={() => dispatch({type: "ADD_INPUT", payload: {index}})}
    >
      <Add/>
    </Button>
    <Button onClick={() => dispatch("ADD_INPUT", {index})}><HighlightOff/></Button>
  </ButtonGroup>
);

const BtnGroupEdit = ({classes, dispatch}) => (
  <ButtonGroup 
    color="primary" 
    size="large" 
    className={classes.actionButton} 
    aria-label="outlined primary button group">
    <Button
      variant="contained"
      color="primary"
      onClick={() => dispatch()}
    >
      <CheckCircle/>
    </Button>
    <Button onClick={() => dispatch()}><HighlightOff/></Button>
  </ButtonGroup>
);

const useStyles = makeStyles(theme => ({
  root: {

  },
  actionButton: { 
    marginLeft: 3
  },
  button: {
    width: "100%"
  },
  inputsContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputLeft: {
    width: "40%"
  },
  inputRight: {
    width: "55%",
    marginLeft: 5
  }
}));

export const MultiField = props => (
  <FormControlContextProvider>
    <TextFieldMutliDouble {...props}/>
  </FormControlContextProvider>
)

 /*const handleChange = (action, index, name, value) => {
    switch (action) {
      case "add":
        inputs[index] = { name, value };
        inputs[index + 1] = { name: "", value: 0 };
        break;
      case "edit":
        inputs[index] = { name, value };
        break;
      case "remove":
        if(index > 0) {
          inputs = inputs.filter(input => input != inputs[index]);
        }
        console.log("remove LOG", inputs);
        break;
    }
    console.log("inputs LOG", inputs);
  }*/
