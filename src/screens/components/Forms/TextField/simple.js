import React from "react";
import { TextField } from "@material-ui/core";
import InputAdornment from '@material-ui/core/InputAdornment';

export default props => (
  <TextField
    onChange={props.onChange}
    onBlur={props.onBlur}
    value={props.value}
    inputRef={props.ref}
    name={props.name}
    label={props.label}
    placeholder={props.label}
    type={props.type}
    variant="filled"
    helperText={props.error}
    size="small"
    fullWidth
    disabled={props.disabled}
    multiline={props.multiline}
    rows={props.rows}
    InputProps={props.adornment && {
      endAdornment: <InputAdornment position="end">{props.adornment}</InputAdornment>,
    }}
  />)