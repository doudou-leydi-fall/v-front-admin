import React from "react";
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  makeStyles
} from "@material-ui/core";

export const SelectField = props => {
  return (
    <FormControl variant="filled" fullWidth size="small">
      <InputLabel htmlFor={props.name} id={props.name}>{props.label}</InputLabel>
      <Select
        defaultValue={props.options[0].value}
        type={props.type}
        labelId={props.name}
        id={props.name}
        value={props.value}
        onChange={(e, values) =>
          props.setFieldValue(props.name, e.target.value)
        }
        fullWidth
      >
        <MenuItem value="">
            <em>None</em>
          </MenuItem>
        {props.options.map(option => (
          <MenuItem value={option.value}>{option[props.optionLabelName]}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export const SelectNumericField = props => (
  <FormControl variant="outlined"  size="small">
    <InputLabel id={props.name}>{props.label}</InputLabel>
    <Select
      type={props.type}
      labelId={props.name}
      id={`select_${props.name}`}
      value={props.value}
      onChange={(e, values) =>
        props.setFieldValue(props.name, parseInt(e.target.value))
      }
      
    >
      {props.options.map(option => (
        <MenuItem value={option.value}>{option[props.optionLabelName]}</MenuItem>
      ))}
    </Select>
  </FormControl>
);
