import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import errorsMsg from "../errorsMsg";

export default ({ register, error, defaultValue }) => (
  <TextField
    name="email"
    label="Email"
    defaultValue={defaultValue}
    inputRef={register({
      required: true,
      pattern: /^\S+@\S+$/i
    }
    )}
    helperText={
      error
      && errorsMsg[error.type]}
    variant="filled"
    size="small"
    fullWidth
  />);
