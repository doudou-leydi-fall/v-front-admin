import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Skeleton from '@material-ui/lab/Skeleton';
import CardActions from '@material-ui/core/CardActions';
import CancelIcon from '@material-ui/icons/Cancel';
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from "@material-ui/core/Checkbox";
import Fab from "@material-ui/core/Fab";

export default ({ item: picture, imageBackgroundSize = "contain", selectImg, onChecked }: any) => {
  const classes = useStyles();
  const [checked, check]:any = useState(false);

  useEffect(() => onChecked(checked), [checked]);

  return (
    <Card className={classes.card}>
      {picture ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={picture}
        title="Image title"
        onClick={selectImg}
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}>

        </Skeleton>
      }
      <CardActions className={classes.buttonPanel} disableSpacing>
        <IconButton aria-label="add to favorites">
          <CancelIcon />
        </IconButton>
        <Fab color="secondary" aria-label="share">
          <WallpaperIcon />
        </Fab>
        {/* <Checkbox
          checked={checked}
          // onChange={(e) => check(e.target.checked)}
          inputProps={{ 'aria-label': 'primary checkbox' }}
        /> */}
      </CardActions>
    </Card>
  )
};

const useStyles = makeStyles((theme) => ({
  buttonPanel: {
    backgroundColor: "#F5F5F5",
    display: "flex",
    justifyContent: "space-between"
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  btnContainer: {
    display: "flex",
    flexDirection: "column"
  },
  footer: {
    display: "flex"
  },
  button: {
    width: "100%"
  }
}));