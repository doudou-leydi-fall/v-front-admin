import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Skeleton from '@material-ui/lab/Skeleton';
import Chip from '@material-ui/core/Chip';

interface CardProps { 
  title: string
  content?:any 
  picture: any
  imageBackgroundSize?: string
  header?: any
  footer?: any
  chipTitle?: string
  chipFooterLeft?: string
  chipFooterRight?: string
}

export default ({ title, content, picture, imageBackgroundSize="cover", header, footer, chipTitle, chipFooterLeft, chipFooterRight }: CardProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      {picture ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={picture}
        title="Image title"
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}>

        </Skeleton>
      }
      <CardContent className={classes.cardContent}>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h2">{title}</Typography>
        </div>
        {chipTitle && <Chip size="small" label={chipTitle} color="secondary"/>}
        <Typography className={classes.description} variant="subtitle2" component="p">{content}</Typography>
        <div className={classes.footer}>
          {chipFooterLeft && <Chip className={classes.footerChip} size="small" label={chipFooterLeft} />}
          {chipFooterRight && <Chip className={classes.footerChip} size="small" label={chipFooterRight} color="secondary"/>}
        </div>
      </CardContent>
    </Card>
  )
};

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  titleContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    overflow: "hidden"
  },
  footer: {
    display: "flex"
  },
  footerChip: {
    marginTop: 10,
    marginRight: 3
  },
  description: {
    maxHeight: 40,
    overflow: "hidden"
  }
}));