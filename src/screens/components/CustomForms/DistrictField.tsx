import React, { useEffect } from "react";
import { SelectField } from "../Forms/SelectField";
import { useQuery } from "@apollo/react-hooks";
import { GET_LOCATIONS } from "../../../network";

interface DistrictFieldProps {
  value: any
  setFieldValue: any
  error?: any
  zone?: any
}

export default ({
  value,
  zone,
  setFieldValue,
  error
}: DistrictFieldProps) => {
  const { data, loading, refetch } = useQuery(GET_LOCATIONS, { variables: { size: 200, criteria: [{ name: "collection", value: "dakar-districts"}, {name: "parent.name", value: zone}]}});

  useEffect(() => {
    refetch();
  }, [zone]);

  return (
    <SelectField 
     name="location.district" 
     label="Quartier" 
     value={value} 
     setFieldValue={setFieldValue} 
     options={data && data.getLocations && data.getLocations.data}
     optionLabel="label"
     optionValue="label"
     disabled={!zone}
     error={error}
     />
  )
}