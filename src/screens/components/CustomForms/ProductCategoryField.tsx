import React from "react";
import SelectField from "../Forms/SelectField/SelectObject";
import { useQuery } from "@apollo/react-hooks";
import { GET_META } from "../../../network";

interface Meta {
  id?: string
  name: string
  value?: string
  cardType?: string
  __typename?: string 
}

interface ProductCategoryFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: ProductCategoryFieldProps) => {
  const { data }:any = useQuery(GET_META, { 
    variables: { 
      criteria: [{ 
        name: "collection", 
        value: "product-category" 
      }] 
    } 
  });

  return (
    <SelectField 
      name="category" 
      label="Category"
      value={value} 
      setFieldValue={setFieldValue} 
      options={data && data.getMeta && data.getMeta.data.map(({ id, name, cardType }:Meta) => ({ id, name , cardType}))}
      error={error}
    />
  )
}