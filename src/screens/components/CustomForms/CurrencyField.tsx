import React from "react";
import { SelectField } from "../Forms/SelectField";

const options = [
  { name: "Euro", label: "€"},
  { name: "FCFA", label: "FCFA"}
]

interface CurrencyFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: CurrencyFieldProps) => {
  return (
    <SelectField 
      name="currency" 
      label="Devise" 
      value={value} 
      setFieldValue={setFieldValue} 
      options={options}
      error={error}
    />
  )
}