import React from "react";
import { SelectField } from "../Forms/SelectField";

interface SizeFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

const options = [
  { name: "Petite" },
  { name: "Moyenne" },
  { name: "Grandes" },
  { name: "Double"},
  { name: "XL"}
];

export default ({
  value,
  setFieldValue,
  error
}: SizeFieldProps) => {
  return (
    <SelectField 
     name="size" 
     label="Taille" 
     value={value} 
     setFieldValue={setFieldValue} 
     options={options}
     optionLabel="name"
     optionValue="name"
     error={error}
     />
  )
}