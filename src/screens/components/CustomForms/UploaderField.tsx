import React, { useCallback, useRef } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useSnackbar } from "notistack";
import { UPLOAD_PICTURES } from "../../../network";
import { CircularProgress } from "@material-ui/core";

interface UploaderFieldProps {
  buttonComponent: React.ReactElement<any>,
  addFileToQueue: any,
  loading?: boolean,
  multiple?: boolean,
}

export default ({ 
  buttonComponent,
  addFileToQueue,
  loading=false,
  multiple=true,

}: UploaderFieldProps) => {
  const fileInput:any = useRef(null);

  return (
    <div>
      <input
        style={{ display: 'none' }}
        id="upload-photo"
        name="multiplefiles"
        type="file"
        multiple={multiple}
        ref={fileInput}
        onChange={(e) => addFileToQueue(e.target.files)}
      />
      { loading ? <CircularProgress size={20}/> : React.cloneElement(buttonComponent, { onClick: () => fileInput.current.click() }) }
    </div>
  )
}
