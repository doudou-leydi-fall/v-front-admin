import React from "react";
import SelectField from "../Forms/SelectField/SelectObject";
import { useQuery } from "@apollo/react-hooks";
import { GET_META } from "../../../network";

interface FoodCollectionFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: FoodCollectionFieldProps) => {
  const { data, loading } = useQuery(GET_META, { 
    variables: { 
      size: 200, 
      criteria: [{ 
        name: "collection", 
        value: "food-collection"
      }]
    }
  });

  return (
    <SelectField 
      name="collection" 
      label="Variétés"
      value={value} 
      setFieldValue={setFieldValue} 
      options={data && data.getMeta && data.getMeta.data.map(({ id, name, cardType }: any) => ({ id, name, cardType }))}
      error={error}
    />
  )
}