import React from "react";
import Autocomplete from "../Forms/Autocomplete";
import FormControl from '@material-ui/core/FormControl';
import { useQuery } from "@apollo/react-hooks";
import { GET_META } from "../../../network";

interface Meta {
  id?: string
  name: string
  value?: string
  cardType?: string
  __typename?: string 
}

interface GastronomiesFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: GastronomiesFieldProps) => {
  const { data }:any = useQuery(GET_META, { 
    variables: { 
      size: 300, 
      criteria: [{ 
        name: "collection", 
        value: "gastronomy" 
      }] 
    } 
  });

  return (
    <FormControl fullWidth>
      <Autocomplete
        name="gastronomies"
        label="Gastronomies"
        placeholder="Gastronomies"
        value={value}
        getOptionLabel={(option: Meta) => option.name}
        options={data && data.getMeta && data.getMeta.data.map(({ name }: Meta) => ({ name }))}
        setFieldValue={setFieldValue}
        getOptionValue={({ name }: Meta) => name}
        formatInitValue={(item:any) => ({ "name": item })}
        error={error}
      />
    </FormControl>
  )
}