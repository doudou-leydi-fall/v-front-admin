
export interface IMeta {
  id: string
  name: string
  cardType: string
}

export interface IMetaFormParams {
  query: any;
  variables: any;
  operation: string;
  filterValue: Function,
  defaultValues?: any
}
