import { GET_META } from '../../../../network'; 
import { IMeta, IMetaFormParams } from './Meta.defs';

const queryParams: IMetaFormParams = {
  query: GET_META,
  operation: "getMeta",
  variables: {
    model: "META",
      criteria: [{ 
        name: "level",
        value: "supremeLevel"
      }],
      fieldname: "name",
  },
  filterValue: ({ id }: IMeta) => id,
}

export default queryParams;