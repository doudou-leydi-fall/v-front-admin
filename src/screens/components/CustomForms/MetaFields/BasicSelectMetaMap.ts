const BasisSelectMetaMap:any =  {
  "childRequirement": {
    options: [
      {
        id: "cardType",
        name: "Type de carte"
      }
    ]
  },
  "cardType": {
    options: [
      {
        id: "LargeCardL",
        name: "LargeCardL"
      },
      {
        id: "SquareCardM",
        name: "SquareCardM"
      },
      {
        id: "SquareCardXL",
        name: "SquareCardXL"
      },
      {
        id: "RoundedCardM",
        name: "RoundedCardM"
      },
    ]
  }
}

export default BasisSelectMetaMap;