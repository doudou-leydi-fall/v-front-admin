import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import BasisSelectMetaMap from "./BasicSelectMetaMap";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

const options = [
  {
    id: "cardType",
    name: "Type de carte"
  }
];

interface BasicSelectMetaProps {
  name: string;
  meta: string;
  value: any;
  setFieldValue: any;
  label: string;
}

const BasisSelectMeta = ({ name, label, value, setFieldValue, meta }: BasicSelectMetaProps) => {
  const classes = useStyles();
  const { options } = BasisSelectMetaMap[meta];
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setFieldValue(name, event.target.value as string);
  };

  return (
    <FormControl variant="filled" fullWidth size="small" className={classes.formControl}>
      <InputLabel id={name}>{label}</InputLabel>
      <Select
        labelId={name}
        value={value}
        onChange={handleChange}
      >
        <MenuItem value={undefined}>
          <em>None</em>
        </MenuItem>
        {options.map((option:any) => <MenuItem key={option.id} value={option.id}>{option.name}</MenuItem>)}
      </Select>
    </FormControl>
  );
}

export default BasisSelectMeta;