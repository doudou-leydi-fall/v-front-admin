import { IMetaFormParams } from "./Meta.defs";
import SupremeLevel from "./SupremeLevel.query";

export default new Map<string, IMetaFormParams>([
  ['SupremeLevel', SupremeLevel],
])