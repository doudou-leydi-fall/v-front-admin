// *https://www.registers.service.gov.uk/registers/country/use-the-api*
import fetch from 'cross-fetch';
import React, { Fragment, memo, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import MetaQueryMap from './MetaQueryMap';
import { useQuery } from 'react-apollo';
import { FormControl, FormHelperText } from '@material-ui/core';

interface CountryType {
  name: string;
}

interface MetaSelectProps {
  name: string
  label: string
  value: any,
  error?: any,
  onBlur?: any,
  meta: string,
  setFieldValue: Function
}
const SelectMeta = ({
  name,
  label,
  meta,
  value,
  error,
  setFieldValue,
}: MetaSelectProps) => {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState<CountryType[]>([]);
  const { query, operation, variables, filterValue }: any = MetaQueryMap.get(meta);
  const { data, loading } = useQuery(query, { variables });

  useEffect(() => {
    if (data && data[operation]) {
      console.log("data[operation]", data[operation]);
      setOptions(data[operation].data);
    }
  }, [data]);
  
  return (
    <Fragment>
      <Autocomplete
        id={name}
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        getOptionSelected={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name}
        options={options}
        loading={loading}
        value={value}
        onChange={(e:any,value:any)  => setFieldValue(name, filterValue(value))}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            variant="filled"
            size="small"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      {error && <FormHelperText>{error}</FormHelperText>}
    </Fragment>
  );
}

export default memo(SelectMeta);
