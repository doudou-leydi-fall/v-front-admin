import React from "react";
import { SelectField } from "../Forms/SelectField";
import { useQuery } from "@apollo/react-hooks";
import { GET_LOCATIONS } from "../../../network";

interface ZoneFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: ZoneFieldProps) => {
  const { data, loading } = useQuery(GET_LOCATIONS, {
    variables: {
      size: 200,
      criteria: [{ 
        name: "collection", 
        value: "dakar-zones" 
      }]
    }
  });

  return (
    <SelectField
      name="location.zone"
      label="Zone"
      value={value}
      setFieldValue={setFieldValue}
      options={data && data.getLocations && data.getLocations.data
      }
      optionLabel="label"
      optionValue="label"
      error={error}
    />
  )
}