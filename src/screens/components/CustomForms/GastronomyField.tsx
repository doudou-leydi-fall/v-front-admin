import React from "react";
import { SelectField } from "../Forms/SelectField";
import { useQuery } from "@apollo/react-hooks";
import { GET_META } from "../../../network";

interface Meta {
  id?: string
  name: string
  value?: string
  cardType?: string
  __typename?: string 
}

interface GastronomyFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: GastronomyFieldProps) => {
  const { data }:any = useQuery(GET_META, { 
    variables: { 
      size: 300, 
      criteria: [{ 
        name: "collection", 
        value: "gastronomy" 
      }] 
    } 
  });

  return (
    <SelectField 
      name="gastronomy" 
      label="Gastronomie" 
      value={value} 
      setFieldValue={setFieldValue} 
      options={data && data.getMeta && data.getMeta.data.map(({ name }:Meta) => ({ name }))}
      error={error}
    />
  )
}