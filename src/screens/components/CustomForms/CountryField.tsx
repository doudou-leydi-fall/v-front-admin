import React from "react";
import { SelectField } from "../Forms/SelectField";
import { useQuery } from "@apollo/react-hooks";
import { GET_LOCATIONS } from "../../../network";

interface CountryFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: CountryFieldProps) => {
  const { data, loading } = useQuery(GET_LOCATIONS, {
    variables: {
      size: 200,
      criteria: [{
        name: "collection",
        value: "world-countries"
      }]
    }
  });

  return (
    <SelectField
      name="location.country"
      label="Pays"
      value={value}
      setFieldValue={setFieldValue}
      options={data && data.getLocations && data.getLocations.data}
      error={error}
    />
  )
}
