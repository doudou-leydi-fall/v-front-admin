import React from "react";
import Autocomplete from "../Forms/Autocomplete";
import FormControl from '@material-ui/core/FormControl';
import { useQuery } from "@apollo/react-hooks";
import { GET_META } from "../../../network";

interface Meta {
  id?: string
  name: string
  value?: string
  cardType?: string
  __typename?: string 
}

interface RecipesFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

export default ({
  value,
  setFieldValue,
  error
}: RecipesFieldProps) => {
  const { data }: any = useQuery(GET_META, {
    variables: {
      criteria: [{
        name: "collection",
        value: "food-composition"
      }]
    }
  });

  return (
    <FormControl fullWidth>
      <Autocomplete
        name="recipes"
        label="Recettes"
        placeholder="Recettes"
        value={value}
        getOptionLabel={(option:Meta) => option.name}
        options={data && data.getMeta && data.getMeta.data.map(({ name }:Meta) => ({ name }))}
        setFieldValue={setFieldValue}
        getOptionValue={({ name }:Meta) => name}
        formatInitValue={(item:any) => ({ "name": item })}
        error={error}
      />
    </FormControl>
  )
}