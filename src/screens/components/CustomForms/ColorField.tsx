import React from "react";
import { TextField } from "@material-ui/core";
import InputAdornment from '@material-ui/core/InputAdornment';

interface ColorFieldProps {
  onChange: any
  onBlur?: any
  name: string
  label: string
  value: any
  error?: any
  disabled?: boolean
}

export default ({
  onChange,
  onBlur,
  name,
  label,
  value,
  error,
  disabled
}:ColorFieldProps) => (
  <TextField
    onChange={onChange}
    onBlur={onBlur}
    value={value}
    name={name}
    label={label}
    placeholder={label}
    variant="filled"
    helperText={error}
    size="small"
    fullWidth
    disabled={disabled}
    InputProps={{
      endAdornment: <div style={{ height: 35, width: 35, borderRadius: 4, backgroundColor: value}}></div>,
    }}
  />)