import React from "react";
import { SelectField } from "../Forms/SelectField";

interface SpicyFieldProps {
  value: any
  setFieldValue: any
  error?: any
}

const options = [
  { name: "Non épicé", value: "NONE" },
  { name: "Épicé", value: "LITTLE" },
  { name: "Moyennement épicé", value: "MEDIUM" },
  { name: "Très épicé", value: "VERY" }
];

export default ({
  value,
  setFieldValue,
  error
}: SpicyFieldProps) => {
  return (
    <SelectField 
     name="spicy" 
     label="Épices" 
     value={value} 
     setFieldValue={setFieldValue} 
     options={options}
     optionLabel="name"
     optionValue="value"
     error={error}
     />
  )
}