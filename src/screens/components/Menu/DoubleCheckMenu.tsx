import React, { useEffect, useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const reducer = (state:any, action:any) => {
  let newstate = [];

  switch (action.type) {
    case "OPEN_COLLAPSE":
      newstate = state.map((item:any, key:number) => {
        if (action.payload === key) {
          return { ...item, open: !state[action.payload].open };
        }
        return item;
      });
      return [...newstate];
    case "SELECT_ITEM":
      let newchild = [];
      newstate = state.map((item:any, key:any) => {
        if (action.payload.key === key) {
          newchild = item.children.map((child:any, subkey:any) => {
            if (action.payload.subkey === subkey) {
              return { ...child, selected: action.payload.checked };
            }
            return { ...child };
          });
          return { ...state[key], children: newchild };
        }
        return { ...state[key] };
      });
      return [...newstate];
    case "SELECT_ALL":
      let list = [];
      newstate = state.map((item:any, key:number) => {
        if (action.payload.key === key) {
          list = item.children.map((child:any) => {
            return { ...child, selected: action.payload.checked };
          });
          return { ...state[key], children: list };
        }
        return { ...state[key] };
      });
      return [...newstate];
    default:
      console.log("reducer LOG", state);
      return state;
  }
}

interface DoubleMenuCkeckListProps {
  label: string
  listItems: any[]
  defaultOpen: boolean
  onSelected: any
}

const DoubleMenuCkeckList = ({ label, listItems, defaultOpen=false, onSelected }: DoubleMenuCkeckListProps) => {
  //const [criteria, dispatch] = ProductStore;
  const classes = useStyles();
  const initItems = listItems.map((item:any) => {
    item.open = defaultOpen;
    if (item.children) {
      item.children.map((subitem:any) => {
        subitem.selected = false;
        return subitem;
      });
    }
    return item;
  });

  const [items, dispatch] = useReducer(reducer, initItems);

  useEffect(() => {
    const selected:any[] = [];
  
    items.map((item:any) => {
      item.children.map((subitem:any) => {
        if (subitem.selected) {
          selected.push(subitem.name);
        }
      })
    });

    onSelected(selected);
  }, [items]);

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
      subheader={
        <ListSubheader className={classes.categoryName} component="div">
          {label}
        </ListSubheader>
      }
    >
      {items.map((item:any, key:number) => (
        <div key={key}>
          <ListItem className={classes.rootItem} button onClick={() => dispatch({ type: "OPEN_COLLAPSE", payload: key })}>
            <ListItemText className={classes.listItemName} primary={item.name} />
            {item.open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={item.open} timeout="auto" unmountOnExit>
            <List className={classes.subItemContainer} component="div" disablePadding>
              <ListItem button>
                <ListItemIcon>
                  <span className={classes.count}>{item.value}</span>
                </ListItemIcon>
                <ListItemText className={classes.listItemName} primary="Tout" />
                <ListItemSecondaryAction>
                  <Checkbox
                    edge="end"
                    onChange={(e, checked) => dispatch({ type: "SELECT_ALL", payload: { key, checked } })}
                  />
                </ListItemSecondaryAction>
              </ListItem>
              {item && item.children && item.children.map((subitem:any, subkey:number) => (
                <ListItem button>
                  <ListItemIcon>
                    <span className={classes.count}>{subitem.value}</span>
                  </ListItemIcon>
                  <ListItemText className={classes.listItemName} primary={subitem.name} />
                  <ListItemSecondaryAction>
                    <Checkbox
                      edge="end"
                      checked={subitem.selected}
                      onChange={(e, checked) => dispatch({ type: "SELECT_ITEM", payload: { key, subkey, checked } })}
                    />
                  </ListItemSecondaryAction>
                </ListItem>
              ))
              }
            </List>
          </Collapse>
        </div>
      ))}
    </List>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: 260,
    maxWidth: 260,
    minWidth: 260,
  },
  label: {
    textTransform: "capitalize",
    padding: "10px 15px",
    color: "#858585"
  },
  buttonCreate: {
    margin: "20px 0px"
  },
  rootItem: {
    backgroundColor: "#F0F0F0"
  },
  listItemName: {
    textTransform: "capitalize"
  },
  count: {
    background: "#F76C1E",
    height: 20,
    minWidth: 20,
    borderRadius: 30,
    padding: "1px 7px",
    fontSize: 12,
    fontWeight: 700,
    color: "#FFF"
  },
  categoryName: {
    textTransform: "capitalize"
  },
  collectionName: {
    textTransform: "capitalize"
  },
  subItemContainer: {
    paddingLeft: 0
  }
}));

export default DoubleMenuCkeckList;