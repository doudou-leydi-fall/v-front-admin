import React, { useEffect } from "react";
import Divider from '@material-ui/core/Divider';
import { makeStyles } from "@material-ui/core/styles";
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';

interface MenuCkeckListProps {
  label: string
  listItems: any[]
  onSelected:any
}

const MenuCkeckList = ({ label, listItems, onSelected }: MenuCkeckListProps) => {
  //const [criteria, dispatch] = ProductStore;
  const classes = useStyles();
  const [selectedItems, setSelectedItems]:any = React.useState([]);

  useEffect(() => {
    onSelected(selectedItems);
  });

  const handleChecked = (checked:boolean, value:any) => {
    if (checked) {
      setSelectedItems([...selectedItems, value]);
    } else {
      setSelectedItems(selectedItems.filter((c:any) => c != value));
    }
  }

  //console.log("data", criteria);
  return (
    <div className={classes.root}>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader className={classes.categoryName} component="div">
            {label}
          </ListSubheader>
        }
        className={classes.root}
      >
        {listItems && listItems.length > 0 &&
          listItems.map((item, key) => (
            <ListItem key={key} button>
              <ListItemIcon>
                <span className={classes.count}>{item.value}</span>
              </ListItemIcon>
              <ListItemText
                className={classes.collectionName}
                primary={item.name}
                // variant="menu"
              />
              <ListItemSecondaryAction>
                <Checkbox
                  edge="end"
                  checked={selectedItems.includes(item.name)}
                  onChange={(e, checked) => handleChecked(checked, item.name)}
                />
              </ListItemSecondaryAction>
            </ListItem>
          ))}
      </List>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: 260,
    maxWidth: 260,
    minWidth: 260,
    marginTop: 10
  },
  buttonCreate: {
    margin: "20px 0px"
  },

  nested: {
    paddingLeft: theme.spacing(4)
  },
  count: {
    background: "#F76C1E",
    height: 20,
    minWidth: 20,
    borderRadius: 30,
    padding: "1px 7px",
    fontSize: 12,
    fontWeight: 700,
    color: "#FFF"
  },
  categoryName: {
    textTransform: "capitalize"
  },
  collectionName: {
    textTransform: "capitalize"
  }
}));

export default MenuCkeckList;