import React, { useCallback, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import CropperQueueCard from "./CropperQueueCard";
import PictureModal from "../Modal/PictureModal";
import { Picture } from "../../../types";
import { Alert } from "@material-ui/lab";

interface CropperQueueLayoutProps {
  pictures?: Picture[]
  cardType?: string
  dispatch: Function
}

export default ({ pictures=[], dispatch, cardType }: CropperQueueLayoutProps) => {
  const classes = useStyles();
  const [img, setImg]:any = useState(null);

  const onClose = useCallback(() => {
    setImg(null);
  }, []);

  return (
    <>
      <div className={classes.container}>
        {pictures.length > 0 && pictures.map((picture:any, key:number) => (
          <div key={key} className={classes.card}>
            <CropperQueueCard key={key} dispatch={dispatch} selectImg={() => setImg(picture.link)} item={picture} cardType={cardType} />
          </div>
          ))}
      </div>
      <PictureModal img={img} onClose={onClose}/>
    </>
  )
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  card: {
    width: 280,
    margin: 5
  }
}))