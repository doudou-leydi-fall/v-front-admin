import React, { createContext, useReducer, useEffect } from "react";

const UploaderContext = createContext({});
export const UploaderConsumer = UploaderContext.Consumer;
export default UploaderContext;

export const TYPES: any = {
  SAVE_LINKS_UPLOAD: "SAVE_LINKS_UPLOAD",
  REMOVE_FROM_LOCALLINKS: "REMOVE_FROM_LOCALLINKS",
  ADDFILE_TO_UPLOADQUEUE: "ADDFILE_TO_UPLOADQUEUE",
  REMOVE_FROM_UPLOADQUEUE: "REMOVE_FROM_UPLOADQUEUE",
  ADD_TO_SELECTION: "ADD_TO_SELECTION",
  REMOVE_FROM_SELECTION: "REMOVE_FROM_SELECTION",
  SET_AVATAR: "SET_AVATAR",
  CLEAR_ALL: "CLEAR_ALL"
}

export const intialState = {
  localImages: [],
  uploadQueue: [],
  initialPictures: [],
  selectedPictures: [],
  selectedAvatar: null,
}

export const reducers = (state: any, action: any) => {
  switch (action.type) {
    case TYPES.SAVE_LINKS_UPLOAD:
      return { ...state, localImages: action.payload }
    case TYPES.ADDFILE_TO_UPLOADQUEUE:
      return { ...state, uploadQueue: [...state.uploadQueue, action.payload] }
    case TYPES.REMOVE_FROM_UPLOADQUEUE:
      return { ...state, uploadQueue: state.uploadQueue.filter((file: any) => file.index !== action.payload) }
    case TYPES.REMOVE_FROM_LOCALLINKS:
      return { ...state, localImages: state.localImages.filter((file: any) => file.index !== action.payload) }
    case TYPES.ADD_TO_SELECTION:
      return { ...state, selectedPictures: [...state.selectedPictures, action.payload] }
    case TYPES.REMOVE_FROM_SELECTION:
      return { ...state, selectedPictures: state.selectedPictures.filter((id: any) => id !== action.payload) }
    case TYPES.CLEAR_ALL:
      return { ...state, localImages: [], uploadQueue: [], selectedPictures: [] }
    case TYPES.SET_AVATAR:
      return { ...state, selectedAvatar: action.payload }
    default:
      return state;
  }
}

export const UploaderProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(reducers, intialState);

  return (
    <>
      <UploaderContext.Provider value={{ state, dispatch }}>
        {children}
      </UploaderContext.Provider>
    </>
  );
};