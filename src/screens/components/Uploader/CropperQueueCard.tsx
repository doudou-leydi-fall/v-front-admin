import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Skeleton from '@material-ui/lab/Skeleton';
import CardActions from '@material-ui/core/CardActions';
import CancelIcon from '@material-ui/icons/Cancel';
import IconButton from '@material-ui/core/IconButton';

export default ({ item, imageBackgroundSize = "contain", selectImg, dispatch }: any) => {
  const classes = useStyles();
  
  return (
    <Card className={classes.card}>
      {(item && item.link) ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={item.link}
        title="Image title"
        onClick={selectImg}
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}>

        </Skeleton>
      }
      <CardActions className={classes.buttonPanel} disableSpacing>
        <Typography>En file d'attente</Typography>
        <IconButton onClick={() => dispatch({ type: "REMOVE_FROM_UPLOADQUEUE", payload: item.index })} aria-label="add to favorites">
          <CancelIcon />
        </IconButton>
      </CardActions>
    </Card>
  )
};

const useStyles = makeStyles((theme) => ({
  buttonPanel: {
    backgroundColor: "#F5F5F5",
    display: "flex",
    justifyContent: "center"
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  btnContainer: {
    display: "flex",
    flexDirection: "column"
  },
  footer: {
    display: "flex"
  },
  button: {
    width: "100%"
  }
}));