import React, { useState, useCallback, useEffect, useReducer, memo, useContext, createContext, useRef } from 'react'
import Cropper from 'react-easy-crop'
import Slider from '@material-ui/core/Slider'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Icon from '@material-ui/core/Icon';
import ImgDialog from './ImgDialog'
import { getCroppedImg, filesToReader, readFile } from './ImageUtils'
import { styles } from './styles'
import RatioMap from './RatioMap'
import CropperQueueLayout from './CropperQueueLayout';
import CircularCustomed from '../Progress/CircularCustomed';
import CroppedPicturesLayout from './CroppedPicturesLayout';
import { RootContext, UI_REDUCER_TYPES } from '../../../RootContext';
import { reducers, TYPES, intialState } from "./UploaderStore";
import classes from '*.module.css';
import { ButtonGroup } from '@material-ui/core';

interface SingleCroppedPanelProps {
  classes: any
  cardType: string
  loading: boolean
  imageSrc: any
  index: number,
  dispatch: Function
  disable: boolean
}

const SingleCroppedPanel = ({
  classes,
  imageSrc,
  dispatch,
  index,
  cardType = "Default",
  loading = false,
  disable = false,
}: SingleCroppedPanelProps) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [zoom, setZoom]: any = useState(1)
  const [croppedAreaPixels, setCroppedAreaPixels]: any = useState(null)
  const [croppedImage, setCroppedImage]: any = useState(null)

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels)
  }, [])

  const showCroppedImage = useCallback(async () => {
    try {
      const croppedImage: any = await getCroppedImg(
        imageSrc,
        croppedAreaPixels,
      )
      setCroppedImage(croppedImage.url)
    } catch (e) {
      console.error(e)
    }
  }, [imageSrc, croppedAreaPixels])

  const addToQueue = useCallback(async () => {
    try {
      const croppedImage: any = await getCroppedImg(
        imageSrc,
        croppedAreaPixels,
      )
      const link = await readFile(croppedImage.file);
      dispatch({ type: TYPES.ADDFILE_TO_UPLOADQUEUE, payload: { index, link, file: croppedImage.file } });
    } catch (e) {
      console.error(e)
    }
  }, [imageSrc, croppedAreaPixels]);

  const onClose = useCallback(() => {
    setCroppedImage(null)
  }, [])

  return !disable ? (
    <div>
      <div className={classes.cropContainer}>
        <Cropper
          image={imageSrc}
          crop={crop}
          zoom={zoom}
          aspect={RatioMap[cardType]}
          onCropChange={setCrop}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
          style={{containerStyle: { backgroundColor: "#FFF"}}}
        />
        {loading && <LinearProgress color="secondary" />}
      </div>
      <div className={classes.controls}>
        <div className={classes.sliderContainer}>
          <Typography
            variant="overline"
            classes={{ root: classes.sliderLabel }}
          >
            Zoom
              </Typography>
          <Slider
            value={zoom}
            min={1}
            max={3}
            step={0.1}
            onChange={(e, zoom) => setZoom(zoom)}
          />
        </div>
        <Button
          onClick={() => dispatch({ type: TYPES.REMOVE_FROM_LOCALLINKS, payload: index })}
          variant="contained"
          color="primary"
          classes={{ root: classes.cropButton }}
        >
          Supprimer
            </Button>
        <Button
          onClick={showCroppedImage}
          variant="contained"
          color="primary"
          classes={{ root: classes.cropButton }}
        >
          Aperçu
            </Button>
        <Button
          onClick={addToQueue}
          variant="contained"
          color="primary"
          classes={{ root: classes.cropButton }}
        >
          Valider
              {loading && <CircularProgress classes={{ root: classes.spinner }} color="inherit" size={20} thickness={4} />}
        </Button>
      </div>
      <ImgDialog img={croppedImage} onClose={onClose} />
    </div>
  ) : null
}

const SingleCroppedComponent = memo(withStyles(styles)(SingleCroppedPanel));

interface CroppedPanelProps {
  title?: string
  loading: boolean
  uploadDone: boolean
  setUploadStatus: any
  cardType: string
  uploadPictures: Function
  initialPictures?: any[]
  refetch?: any
  deletePictures?: any
  deleteLoading: boolean
  deleteDone?: boolean
  setDeleteDone?: any
  setAvatar?: any
  isAvatarDone?: boolean
  setAvatarLoading?: boolean
  setAvatarDone?: any
}

const CroppedPanel = ({
  title = "",
  loading = false,
  uploadDone = false,
  setUploadStatus,
  cardType,
  uploadPictures,
  initialPictures = [],
  deletePictures,
  deleteLoading,
  deleteDone = false,
  setDeleteDone,
  setAvatar,
  isAvatarDone,
  setAvatarDone
}: CroppedPanelProps) => {
  const classes = useStyles();
  const fileInput: any = useRef(null);
  const [state, dispatch] = useReducer(reducers, intialState);
  const { uidispatch } = useContext(RootContext);

  const addToCropQueue = async (e: any) => {
    const { files } = e.target;
    const imgLinks = await filesToReader(files);
    dispatch({ type: TYPES.SAVE_LINKS_UPLOAD, payload: imgLinks });
  }

  useEffect(() => {
    if (uploadDone) {
      dispatch({ type: TYPES.CLEAR_ALL });
      setUploadStatus(false);
    }
  }, [uploadDone]);

  useEffect(() => {
    if (deleteDone) {
      dispatch({ type: TYPES.CLEAR_ALL });
      setDeleteDone(false)
    }
  }, [deleteDone]);

  useEffect(() => {
    // if (isAvatarDone) {
    //   setAvatarDone(false);
    // }
    if (state.selectedAvatar) {
      setAvatar(state.selectedAvatar);
    }
  }, [state.selectedAvatar]);

  useEffect(() => {
    if (isAvatarDone) {
      setAvatarDone(false);
    }
  }, [isAvatarDone]);

  useEffect(() => {
    console.log("selectedPictures LOG", state.selectedPictures);
  }, [state.selectedPictures]);

  return (
    <>
      <AppBar position="static" color="default">
        <Toolbar className={classes.toolbar}>
          <Typography variant="h4">{title}</Typography>
          <div>
            <input
              style={{ display: 'none' }}
              id="upload-photo"
              name="multiplefiles"
              type="file"
              multiple={false}
              ref={fileInput}
              onChange={addToCropQueue}
            />
            <ButtonGroup disableElevation variant="contained">
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={() => fileInput.current.click()}
                startIcon={!loading ? <Icon style={{ fontSize: 30 }}>add_circle</Icon> : <CircularProgress size={20} />}
              >
                Ajouter
              </Button>

              <Button
                disabled={state.uploadQueue.length < 1}
                color="secondary"
                size="large"
                variant="contained"
                onClick={() => uploadPictures(state.uploadQueue.map(({ file }: any) => file))}
                startIcon={loading ? <CircularCustomed size={30} color="primary" /> : <Icon style={{ fontSize: 30 }}>cloud_upload</Icon>}
              >
                {loading ? "Chargement..." : "Charger"}
              </Button>
              <Button
                disabled={state.selectedPictures.length < 1}
                color="secondary"
                size="large"
                variant="contained"
                onClick={() => uidispatch({
                  type: UI_REDUCER_TYPES.OPEN_COMFIRM_MODAL,
                  payload: { open: true, method: () => deletePictures(state.selectedPictures) }
                })}
                startIcon={deleteLoading ? <CircularCustomed size={30} color="primary" /> : <Icon style={{ fontSize: 30 }}>delete_outline</Icon>}
              >
                {deleteLoading ? "Suppression..." : "Supprimer"}
              </Button>
            </ButtonGroup>
          </div>
        </Toolbar>
      </AppBar>
      {state.localImages.length > 0
        && state.localImages.map((img: any, key: number) =>
          <SingleCroppedComponent
            index={img.index}
            disable={!!state.uploadQueue.map((item: any) => item.index).includes(img.index)}
            dispatch={dispatch}
            imageSrc={img.link}
            cardType={cardType}
            loading={loading}
          />
        )}
      {state.uploadQueue.length > 0
        && <CropperQueueLayout
          dispatch={dispatch}
          pictures={state.uploadQueue}
          cardType={cardType}
        />}
      {initialPictures && initialPictures.length > 0
        && <CroppedPicturesLayout
          dispatch={dispatch}
          pictures={initialPictures}
          cardType={cardType}
        />}
    </>
  );
}

const useStyles = makeStyles({
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "transparent"
  }
});

export default memo(CroppedPanel);
