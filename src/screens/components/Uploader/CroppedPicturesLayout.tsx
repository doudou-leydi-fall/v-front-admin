import React, { useCallback, useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Alert from "@material-ui/lab/Alert";
import PictureModal from "../Modal/PictureModal";
import { Picture } from "../../../types";
import { CardMedia, CardActions, Checkbox, Card } from "@material-ui/core";
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import { Skeleton } from "@material-ui/lab";
import Fab from "@material-ui/core/Fab";
import { TYPES } from "./UploaderStore";

interface LayoutPicturesProps {
  pictures?: Picture[]
  cardType?: string
  dispatch?: any
}

export default ({ pictures = [], cardType, dispatch }: LayoutPicturesProps) => {
  const classes = useStyles();
  const [img, setImg]: any = useState(null);

  const handleChecked = (checked: boolean, id: string) => {
    if (checked) {
      dispatch({ type: "ADD_TO_SELECTION", payload: id })
    } else {
      dispatch({ type: "REMOVE_FROM_SELECTION", payload: id })
    }
  }

  const onClose = useCallback(() => {
    setImg(null);
  }, []);

  return (
    <>
      <div className={classes.container}>
        {pictures.length > 0 ? pictures.map((picture: any, key) => (
          picture && picture["mobile"] ? (
            <div key={picture.id} className={classes.card}>
              <PictureCard
                dispatch={dispatch}
                onChecked={(value: boolean) => handleChecked(value, picture.id)}
                selectImg={() => setImg(picture.mobile)}
                key={picture.id}
                picture={picture}
                cardType={cardType}
              />
            </div>
          ) : null)): <Alert severity="info">Pas d'images</Alert>}
      </div>
      <PictureModal img={img} onClose={onClose} />
    </>
  )
};

const PictureCard = ({ 
  picture,
  imageBackgroundSize = "contain",
  selectImg,
  onChecked,
  dispatch,
  setAvatar 
}: any) => {
  const classes = useStyles();
  const [checked, check] = useState(false);

  useEffect(() => onChecked(checked), [checked]);

  return (
    <Card className={classes.card}>
      {(picture && picture.mobile) ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={picture.mobile}
        title="Image title"
        onClick={selectImg}
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}></Skeleton>
      }
      <CardActions className={classes.buttonPanel} disableSpacing>
        <Fab onClick={() => dispatch({ type: TYPES.SET_AVATAR, payload: picture.id })} color={picture.main ? "secondary" : "default"} size="small" aria-label="share">
          <WallpaperIcon />
        </Fab>
        <Checkbox
          defaultChecked={false}
          checked={checked}
          onChange={(e) => check(e.target.checked)}
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      </CardActions>
    </Card>
  )
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  buttonPanel: {
    backgroundColor: "#F5F5F5",
    display: "flex",
    justifyContent: "space-between"
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
    width: 400
  },
  cardContent: {
    flexGrow: 1,
  },
  btnContainer: {
    display: "flex",
    flexDirection: "column"
  },
  footer: {
    display: "flex"
  },
  button: {
    width: "100%"
  }
}))