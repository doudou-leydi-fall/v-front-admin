const RatioMap:any = {
  Default: 1,
  LargeCardM: 16/9,
  LargeCardL: 16/9,
  SquareCardM: 1/1,
  SquareCardL: 1/1,
  SquareCardXL: 4/3,
  RoundedCardM: 1/1,
  Square: 1,
}

export default RatioMap;