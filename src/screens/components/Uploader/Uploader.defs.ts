export interface IMetaPicture {
  id: string;
  mobile: string;
  desktop: string;
  format?: string;
  provider?: string;
  CREATED_AT?: string
}

export interface IPictureCardProps {
  picture: IMetaPicture
  imageBackgroundSize?: string
}