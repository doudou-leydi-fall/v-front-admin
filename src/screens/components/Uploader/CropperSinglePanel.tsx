import React, { useState, useCallback, useEffect, useReducer, memo, useContext, createContext, useRef } from 'react'
import Cropper from 'react-easy-crop'
import Slider from '@material-ui/core/Slider'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { readSingleFile } from './ImageUtils'
import ImgDialog from './ImgDialog'
import { getCroppedImg, readFile } from './ImageUtils'
import { styles } from './styles'
import RatioMap from './RatioMap'
import { AppBar, Toolbar, ButtonGroup, Icon, Card, CardActions, CardMedia, Fab, makeStyles } from '@material-ui/core'
import CircularCustomed from '../Progress/CircularCustomed'
import { IMetaPicture, IPictureCardProps } from './Uploader.defs'
import { Alert, Skeleton } from '@material-ui/lab'

interface SingleCroppedPanelProps {
  title: string
  classes?: any
  cardType: string
  loading: boolean
  isUploaded: boolean
  uploadPictures: Function
  onRemove: Function
  initialPicture?: IMetaPicture
}

const SingleCropperPanel = ({
  title,
  cardType = "SquareCardM",
  loading = false,
  isUploaded=false,
  onRemove,
  uploadPictures,
  initialPicture
}: SingleCroppedPanelProps) => {
  const classes = panelStyles();
  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [zoom, setZoom]: any = useState(1)
  const fileInput: any = useRef(null);
  const [croppedAreaPixels, setCroppedAreaPixels]: any = useState(null)
  const [file, setFile] = useState(null);
  const [croppedImage, setCroppedImage]: any = useState(null)
  const [link, setLink]: any = useState(null)

  const reset = () => {
    fileInput.current.value = null;
    setFile(null);
    setLink(null);
  }

  useEffect(() => {
    if (isUploaded) {
      reset(); 
    }
  }, [isUploaded]);

  const handleFile = async (e: any) => {
    const file = e.target.files[0];
    if (file) {
      const link = await readSingleFile(file);
      setLink(link)
    }
  }

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels)
  }, [])

  const showCroppedImage = useCallback(async () => {
    try {
      const croppedImage: any = await getCroppedImg(
        link,
        croppedAreaPixels,
      )
      setCroppedImage(croppedImage.url)
    } catch (e) {
      console.error(e)
    }
  }, [link, croppedAreaPixels])

  const addToQueue = useCallback(async () => {
    try {
      const croppedImage: any = await getCroppedImg(
        link,
        croppedAreaPixels,
      )
      const newLink = await readFile(croppedImage.file);
      setFile(croppedImage.file)
      setLink(newLink);
    } catch (e) {
      console.error(e)
    }
  }, [link, croppedAreaPixels]);

  const onClose = useCallback(() => {
    setCroppedImage(null)
  }, [])

  return (
    <div>
      <AppBar position="static" color="default">
        <Toolbar className={classes.toolbar}>
          <Typography variant="h4">{title}</Typography>
          <div>
            <input
              style={{ display: 'none' }}
              id="upload-photo"
              name="multiplefiles"
              type="file"
              multiple={false}
              ref={fileInput}
              onChange={handleFile}
            />
            <ButtonGroup disableElevation variant="contained" color="primary">
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={() => fileInput.current.click()}
                startIcon={loading ? <CircularProgress size={20} /> : <Icon style={{ fontSize: 30 }}>add_circle</Icon>}
              >
                Ajouter
              </Button>
              <Button
                disabled={!file}
                color="secondary"
                size="large"
                variant="contained"
                onClick={() => uploadPictures(file)}
                startIcon={loading ? <CircularCustomed size={30} color="primary" /> : <Icon style={{ fontSize: 30 }}>cloud_upload</Icon>}
              >
                {loading ? "Chargement..." : "Charger"}
              </Button>
            </ButtonGroup>
          </div>
        </Toolbar>
      </AppBar>
      {link ?
        <div>
          <div className={classes.cropContainer}>
            <Cropper
              image={link}
              crop={crop}
              zoom={zoom}
              aspect={RatioMap[cardType]}
              onCropChange={setCrop}
              onCropComplete={onCropComplete}
              onZoomChange={setZoom}
            />
            {loading && <LinearProgress color="secondary" />}
          </div>
          <div className={classes.controls}>
            <div className={classes.sliderContainer}>
              <Typography
                variant="overline"
                classes={{ root: classes.sliderLabel }}
              >
                Zoom
              </Typography>
              <Slider
                value={zoom}
                min={1}
                max={3}
                step={0.1}
                onChange={(e, zoom) => setZoom(zoom)}
              />
            </div>
            <Button
              variant="contained"
              color="primary"
              classes={{ root: classes.cropButton }}
              onClick={() => reset()}
            >
              Supprimer
            </Button>
            <Button
              onClick={showCroppedImage}
              variant="contained"
              color="primary"
              classes={{ root: classes.cropButton }}
            >
              Aperçu
            </Button>
            <Button
              disabled={!!file}
              onClick={addToQueue}
              variant="contained"
              color="primary"
              classes={{ root: classes.cropButton }}
            >
              Valider
              {loading && <CircularProgress classes={{ root: classes.spinner }} color="inherit" size={20} thickness={4} />}
            </Button>
          </div>
        </div> : null}
        <div>
          {initialPicture ? <PictureCard picture={initialPicture}/> : (<Alert severity="info">Pas d'images</Alert>)}
        </div>
      <ImgDialog img={croppedImage} onClose={onClose} />
    </div>
  )
}

export default memo(withStyles(styles)(SingleCropperPanel));

const PictureCard = ({ 
  picture,
  imageBackgroundSize = "contain",
}: IPictureCardProps) => {
  const classes = cardStyles();

  return (
    <Card className={classes.card}>
      {(picture && picture.mobile) ? <CardMedia
        className={classes.cardMedia}
        style={{ backgroundSize: imageBackgroundSize }}
        image={picture.mobile}
        title="Image title"
      /> : <Skeleton variant="rect" width="100%" className={classes.cardMedia}></Skeleton>
      }
      <CardActions className={classes.buttonPanel} disableSpacing>
        <Fab size="small" aria-label="share">
          <Icon>remove</Icon>
        </Fab>
      </CardActions>
    </Card>
  )
};

const cardStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  buttonPanel: {
    backgroundColor: "#F5F5F5",
    display: "flex",
    justifyContent: "space-between"
  },
  card: {
    width: 400,
  },
  cardMedia: {
    paddingTop: '100%', // '56.25%' => 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    display: "flex"
  },
}))

export const panelStyles:any = makeStyles((theme:any) => ({
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  spinner: {
    marginLeft: 5
  },
  dialog: {
    background: "#FFFFFF",
    zIndex: 1000
  },
  cropContainer: {
    position: 'relative',
    width: '100%',
    height: 200,
    background: '#333',
    [theme.breakpoints.up('sm')]: {
      height: 400,
    },
  },
  cropButton: {
    flexShrink: 0,
    marginLeft: 16,
  },
  controls: {
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  sliderContainer: {
    display: 'flex',
    flex: '1',
    alignItems: 'center',
  },
  sliderLabel: {
    [theme.breakpoints.down('xs')]: {
      minWidth: 65,
    },
  },
  slider: {
    padding: '22px 0px',
    marginLeft: 16,
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'center',
      margin: '0 16px',
    },
  },
}))
