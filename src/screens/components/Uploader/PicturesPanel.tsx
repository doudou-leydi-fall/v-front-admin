import React, { useState } from "react";
import { useSnackbar } from "notistack";
import { CloudUpload } from "@material-ui/icons";
import {
  Button,
  Card,
  CardMedia,
  CardActions,
  makeStyles
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

interface PicturesUploaderProps {
  onSubmit: Function
}

const useStyles = makeStyles(theme => ({
  media: {
    height: 190,
    width: 200
  }
}));

export const PicturesUploader = ({ onSubmit }:PicturesUploaderProps) => {
  const classes = useStyles();
  let [files, setFiles]:any = useState([]);
  const [mainPicture, setMainPicture]:any = useState(0);

  const handleSubmit = async () => {
    const pictures:any = files.map((file:any) => {
      return file;
    });
    onSubmit(pictures);
  };

  const addFileToQueue = (newfiles:any) => {
    newfiles = newfiles.map((newfile:any) => {
      newfile.local = URL.createObjectURL(newfile);
      return newfile;
    });
    setFiles([...files, ...newfiles]);
  };

  const removeFromQueue = (e:any, fileToRmIndex:number) => {
    e.preventDefault();
    files = files.filter((file:any, index:number) => index !== fileToRmIndex);
    setFiles(files);
  };

  return (
    <form>
      <div style={imagePanelStyle}>
        {files ? (
          files.map((image:any, index:number) => (
            <UploaderThumbnail
              key={index}
              classes={classes}
              src={image.local}
              leftButtonAction={(e:Event) => removeFromQueue(e, index)}
              rightButtonAction={(e:Event) => setMainPicture(index)}
            />
          ))
        ) : (
          <span>Panel vide</span>
        )}
      </div>
      <input
        name="multiplefiles"
        type="file"
        multiple
        onChange={(e:any) => addFileToQueue(Array.from(e.target.files))}
      />
      <Button
        variant="contained"
        color="default"
        onClick={handleSubmit}
        startIcon={<CloudUpload />}
      >
        Upload
      </Button>
    </form>
  );
};

const imagePanelStyle:any = {
  display: "flex",
  flexWrap: "wrap"
};

const thumbnailStyle:any = {
  width: 200,
  marginLeft: 5
};

interface UploaderThumbnailProps {
  classes: any,
  src: string,
  height?: number,
  leftButtonAction: any,
  rightButtonAction: any
}

const UploaderThumbnail = ({
  classes,
  src,
  height,
  leftButtonAction,
  rightButtonAction
}: UploaderThumbnailProps) => (
  <Card>
    {src ? (
      <CardMedia className={classes.media} image={src} title="Paella dish" />
    ) : (
      <Skeleton
        variant="rect"
        className={classes.media}
        height={height || 180}
      />
    )}
    <CardActions>
      <Button size="small" color="primary" onClick={leftButtonAction}>
        Supprimer
      </Button>
      <Button size="small" color="primary" onClick={rightButtonAction}>
        Principale
      </Button>
    </CardActions>
  </Card>
);

/*const ThumbnailImage = ({ src, height, classes }) =>
  src ? (
    <CardMedia className={classes.media} image={src} title="Paella dish" />
  ) : (
    <Skeleton variant="rect" className={classes.media} height={height || 180} />
  );*/
