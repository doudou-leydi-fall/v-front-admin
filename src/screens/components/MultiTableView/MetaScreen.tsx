import React from "react";
import RootTabs from "./Tabs";
import  * as config from "./Table.config";

const MetaScreen = () => {
  return <RootTabs config={config}/>
}

export default MetaScreen;