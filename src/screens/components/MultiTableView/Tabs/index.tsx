import React, { createContext, useEffect, useReducer, useState } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TableLayout from '../Table';
import { Button, Grid, Icon, Toolbar } from '@material-ui/core';
import HighMetaForm from './Form';
import { useQuery } from 'react-apollo';

const a11yProps = (index: any) => ({
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  })

interface ITab {
  name?: string,
  label: string,
  component: React.ReactElement,
  model?: string
}

export const ROOT_TYPES = {
  SET_DISPLAY_MODE: "SET_DISPLAY_MODE"
}

const initState = {
  displayMode: {
    mode: 0,
    formType: null,
    refresh: false,
  },
  triggerRefetch: false
}

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case ROOT_TYPES.SET_DISPLAY_MODE:
      return { ...state, displayMode: action.payload }
    default:
      return state;
  }
}

export const TableContext: any = createContext({});
const TableProvider = TableContext.Provider;

const RootTabs = ({ config }:any) => {
  const [state, dispatchRoot] = useReducer(reducer, initState)
  const classes = useStyles();
  const [selectedTabItem, setSelectedTabItem]: any = React.useState(null);
  const [tabs, setTabs] = useState([]);
  const { query, model, criteria, operation } = config.SupremeLevelParams.fetch;

  const { data, refetch: refetchRoot } = useQuery(query, {
    variables: {
      model,
      criteria,
    }
  });

  useEffect(() => {
    if (data && data[operation]) {
      const tabs = data[operation].data;
      setTabs(tabs);
      setSelectedTabItem(tabs[0])
    }
  }, [data]);

  useEffect(() => {
    if(state.displayMode.refresh) {
      refetchRoot();
    }
  }, [state.displayMode]);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: any) => {
    setSelectedTabItem(newValue);
  };

  const renderContent = (displayMode: any) => {
    switch (displayMode) {
      case 0:
        return selectedTabItem && <TableLayout refetchRoot={refetchRoot} tabItem={selectedTabItem} />
      case 1:
        return <HighMetaForm />
    }
  }

  return (
    <TableProvider value={{ ...config, dispatchRoot, refetchRoot, ROOT_TYPES }}>
      <Grid container className={classes.root}>
        <Grid className={classes.tabsContainer} item sm={2}>
          <Button
            color="inherit"
            variant="outlined"
            size="large"
            startIcon={<Icon>add_variant</Icon>}
            onClick={() => dispatchRoot({ type: ROOT_TYPES.SET_DISPLAY_MODE, payload: { mode: 1 }})}
          >Nouveau</Button>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
          >
            {tabs.sort((x: any, y: any) => x.label - y.label).map((tab: ITab, key: number) => <Tab label={tab.name} value={tab} {...a11yProps(key)} />)}
          </Tabs>
        </Grid>
        <Grid className={classes.body} item sm={10}>
          {renderContent(state.displayMode.mode)}
        </Grid>
      </Grid>
    </TableProvider>
  );
}

interface CategoryFormProps {
  dispatch: any
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    paddingBottom: 30,
    paddingTop: 30,

  },
  body: {
    paddingLeft: 20,
    paddingRight: 20
  },
  tabsContainer: {
    display: "flex",
    flexDirection: "column"
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    flexGrow: 2,
    marginTop: 10,
  },
  toolbarContainer: {
    marginBottom: 20
  },
  toolbar: {

    display: "flex",
    justifyContent: "space-between"
  },
  appbar: {
    width: "auto",
    marginBottom: 20
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  }
}));

export default RootTabs;

