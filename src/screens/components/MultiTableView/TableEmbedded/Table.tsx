import React from 'react';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Checkbox } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { TYPES } from './index';

interface TableEmbeddedProps {
  list: any;
  allSelected: boolean;
  dispatch: any;
  state: any;
}

const TableEmbedded = ({ state, dispatch, allSelected, list = {} }: TableEmbeddedProps) => {
  const classes = useStyles();
  const { data, total } = list;

  return data && data.length > 0 ? (<Box margin={1}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>ID</TableCell>
            <TableCell>Nom</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          { data.map((row: any) => (
            <TableRow key={row.id}>
              <TableCell className={classes.checkCell}>
                <Checkbox
                  checked={state.selected.includes(row.id)}
                  onChange={(e, checked) => dispatch({
                    type: TYPES.SELECTED_ITEMS,
                    payload: {
                      id: row.id,
                      checked
                    }
                  })}
                  inputProps={{ 'aria-label': 'select all desserts' }} />
              </TableCell>
              <TableCell component="th" scope="row">
                {row.id}
              </TableCell>
              <TableCell>{row.name}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Box>) :  (<Alert severity="info">Pas de données</Alert>)
}

export default TableEmbedded;


const useStyles = makeStyles({
  checkCell: {
    width: 50
  },
})