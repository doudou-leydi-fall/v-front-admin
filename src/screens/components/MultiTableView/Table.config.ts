import { GET_META, ADD_META, DELETE_META } from "../../../network";

export const SupremeLevelParams: any = {
  entity: "Metadonnée",
  fetch: {
    query: GET_META,
    operation: "getMeta",
    model: "META",
    criteria: [{
      name: "level",
      value: "supremeLevel"
    }]
  },
  create: {
    mutation: ADD_META,
    operation: "addMeta",
    variable_name: "meta",
  }
}

export const HighLevelParams:any = {
  entity: "Metadonnée",
  fetch: {
    query: GET_META,
    operation: "getMeta",
    model: "META",
    getParams: (id:string) => ({
      variables: {
        model: "META",
        criteria: [],
        customSearch: {
          type: "getByParentID",
          level: "highLevel",
          parentID: id
        }
      }
    })
  },
  create: {
    mutation: ADD_META,
    operation: "addMeta",
    variable_name: "meta",
  },
  delete: {
    mutation: DELETE_META,
    operation: "deleteMeta",
    variable_name: "meta",
    deleteParams: (ids:any) => ({
      variables: {
        ids
      }
    }),
    success: "Metadonnée supprimées"
  }
}

export const EmbeddedLayoutParams:any = {
  fetch: {
    query: GET_META,
    operation: "getMeta",
    model: "META",
    getParams: (id:string) => ({
      variables: {
        model: "META",
        criteria: [],
        customSearch: {
          type: "getByParentID",
          level: "downLevel",
          parentID: id
        }
      }
    })
  },
  delete: {
    mutation: DELETE_META,
    operation: "deleteMeta",
    variable_name: "meta",
    deleteParams: (ids:any) => ({
      variables: {
        ids
      }
    }),
    success: "Metadonnée supprimées"
  },
  create: {
    mutation: ADD_META,
    operation: "addMeta",
    variable_name: "meta",
  },
} 
