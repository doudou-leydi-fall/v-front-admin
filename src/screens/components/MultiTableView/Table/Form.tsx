import { Button, Grid, makeStyles } from "@material-ui/core";
import * as Yup from 'yup';
import { Formik } from "formik";
import React, { useContext } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../../RootContext";
import { TextField } from "../..";
import BasisSelectMeta from "../../CustomForms/MetaFields/BasicSelectMeta";
import { TableContext } from "../Tabs";
import { HighContext, HIGH_TYPES } from "./index";

interface TableFormProps {
  parentID: string;
}

const TableForm = ({ parentID }: TableFormProps) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const { HighLevelParams } = useContext(TableContext);
  const { dispatchHigh }:any = useContext(HighContext);
  const { mutation, operation, variable_name } = HighLevelParams.create;
  const [applyMutation, { loading, error }]:any = useMutation(mutation);

  const handleSubmit = async (values: any, actions: any) => {
    try {
      let variables:any = {};
      variables[variable_name] = values;
      const { data } = await applyMutation({ variables })

      if (data[operation]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Données ajoutées",
            variant: "success"
          }
        });
        
        dispatchHigh({ type: HIGH_TYPES.SET_DISPLAY_MODE, payload: { mode: 0, refresh: true } })
      }

      console.log("result LOG", data);
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <Grid container spacing={1} className={classes.root}>
        <Grid item sm={4}>
          <TextField
            label="Nom"
            name="name"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.name}
            error={props.errors.name}
          />
        </Grid>
        <Grid item sm={4}>
          <TextField
            label="Label"
            name="label"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.label}
            error={props.errors.label}
          />
        </Grid>
        <Grid item sm={4}>
          <BasisSelectMeta
            name="childRequirement"
            label="Type de champ requis"
            setFieldValue={props.setFieldValue}
            meta="childRequirement"
            value={props.value}
          />
        </Grid>
        <Grid item sm={12}>
          <Button
            className={classes.button}
            variant="contained"
            onClick={() => dispatchHigh({ type: HIGH_TYPES.SET_DISPLAY_MODE, payload: { mode: 0 } })} color="primary">Annuler</Button>
          <Button
            className={classes.button}
            variant="contained"
            type="submit"
            color="primary">
            Creer
      </Button>
        </Grid>
      </Grid>
    </form>
  );

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    label: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
  });

  return (
    <Formik
      initialValues={{
        parentID,
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginRight: 5
  }
});

export default TableForm;