import React from "react";
import Button from "@material-ui/core/Button";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import { execute } from "graphql";

interface ConfirmModalProps {
  open: boolean
  action: Function
  buttonLabel?: string
  dispatch: any
}
export const ConfirmModal = ({ open = false, action, buttonLabel = "VALIDER", dispatch }: ConfirmModalProps) => {
  const executeAction = () => {
    action();
    dispatch({ type: "CLOSE_COMFIRM_MODAL" })
  }

  return (
    <div>
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Confirmation"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            ÊTES vous sur de bien vouloir supprimer ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => dispatch({
              type: "OPEN_COMFIRM_MODAL",
              payload: { open: false }
            })}
            color="primary">
            Annuler
          </Button>
          <Button color="primary" autoFocus onClick={executeAction}>
            {buttonLabel}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
