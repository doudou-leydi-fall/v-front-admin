import React from "react";
import { Typography, Divider, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export default ({
  label,
  component
}) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <Typography variant="h3">{label}</Typography>
      </div>
      <Divider />
      <Grid container>
        {component}
      </Grid>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    background: "#FFF",
    border: "1px solid #edf2f9",
    borderRadius: 3,
    width: "100%",
    marginTop: 20
  },
  header: {
    height: 50,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px 20px"
  },
  body: {
    display: "flex",
    justifyContent: "center",
    alignItems: "start",
    padding: 30
  }
}));
