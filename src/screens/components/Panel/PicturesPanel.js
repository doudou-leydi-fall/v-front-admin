import React from "react";
import {
  Button,
  Card,
  CardMedia,
  CardActions,
  makeStyles
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
  media: {
    height: 190,
    width: 200
  },
  card: {
    marginLeft: 5,
    marginRight: 5
  }
}));

export const ThumbnailsPanel = ({ pictures }) => {
  const classes = useStyles();

  const removeFromQueue = () => {};

  const setMainPicture = () => {};

  return (
    <div style={imagePanelStyle}>
      {pictures ? (
        pictures.map((picture, index) => (
          <PictureThumbnail
            key={index}
            classes={classes}
            style={thumbnailStyle}
            src={picture.small}
            leftButtonAction={e => removeFromQueue(e, index)}
            rightButtonAction={e => setMainPicture(index)}
          />
        ))
      ) : (
        <span>Panel vide</span>
      )}
    </div>
  );
};

const thumbnailStyles = makeStyles(theme => ({
  media: {
    height: 190,
    width: 200
  },
  card: {
    marginLeft: 5,
    marginRight: 5
  }
}));

export const PictureThumbnail = ({
  src,
  height,
  leftButtonAction,
  rightButtonAction
}) => {
  const classes = thumbnailStyles();
  return (
    <Card className={classes.card}>
      {src ? (
        <CardMedia className={classes.media} image={src} title="Paella dish" />
      ) : (
        <Skeleton
          variant="rect"
          className={classes.media}
          height={height || 180}
        />
      )}
      <CardActions>
        <Button size="small" color="primary" onClick={leftButtonAction}>
          Supprimer
        </Button>
        <Button size="small" color="primary" onClick={rightButtonAction}>
          Principale
        </Button>
      </CardActions>
    </Card>
  );
};

const thumbnailStyle = {
  width: 200,
  marginLeft: 5
};

const imagePanelStyle = {
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  flexDirection: "row",
  flexWrap: "wrap",
  flexFow: "row wrap",
  alignContent: "flex-end"
};
