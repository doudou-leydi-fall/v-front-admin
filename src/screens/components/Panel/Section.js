import React from "react";
import { Typography, Divider, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export default ({
  label,
  items=[],
  childComponent
}) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <Typography variant="h3">{label}</Typography>
      </div>
      <Divider />
      <div className={classes.body}>
        { items.map(item => <div className={classes.card}> { React.cloneElement(childComponent, { item }) }</div>) }
      </div>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    background: "#FFF",
    border: "1px solid #edf2f9",
    borderRadius: 3,
    width: "100%",
    marginTop: 20,
  },
  header: {
    height: 50,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px 20px"
  },
  body: {
    overflow: "auto",
    display: "flex",
    flexWrap: "wrap",
    padding: 20
  },
  card: {
    width: 270
  }
}));
