import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  root: {
    margin: "10px 0px",
    padding: 10,
    backgroundColor: "#f5f5f5",
    width: "100%",
    textAlign: "center",
    borderRadius: 0,
  }
}));

export const InfoPanel = ({ label }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Typography variant="h6">{label}</Typography>
    </Paper>
  );
};
