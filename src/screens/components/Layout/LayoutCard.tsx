import React from "react";
import { makeStyles } from '@material-ui/core/styles';

interface LayoutCardProps {
  items: any[]
  childrenComponent: any
}

export default ({ items=[], childrenComponent }: LayoutCardProps) => {
  console.log("items", items);
  const classes = useStyles();
  return (
    <div className={classes.container}>
      {items.map((dataCard, key) => (
        <div key={key} className={classes.card}>
          {React.cloneElement(childrenComponent, { key, item: dataCard })}
        </div>
      ))}
    </div>
  )
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  card: {
    width: 280,
    margin: 5
  }
}))