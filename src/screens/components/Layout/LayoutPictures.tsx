import React, { useCallback, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import PictureCard from "../Card/PictureCard";
import PictureModal from "../Modal/PictureModal";
import { Picture } from "../../../types";

interface LayoutPicturesProps {
  pictures?: Picture[]
  cardType?: string
  dispatch?: any
}

export default ({ pictures = [], cardType, dispatch }: LayoutPicturesProps) => {
  const classes = useStyles();
  const [img, setImg]: any = useState(null);

  const onClose = useCallback(() => {
    setImg(null);
  }, []);

  return (
    <>
      <div className={classes.container}>
        {pictures.map((picture, key) => (
          picture && picture["mobile"] ? (
            <div key={key} className={classes.card}>
              <PictureCard
                selectImg={() => setImg(picture.mobile)}
                key={key}
                item={picture.mobile}
                cardType={cardType}
              />
            </div>
          ) : null))}
      </div>
      <PictureModal img={img} onClose={onClose} />
    </>
  )
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  card: {
    width: 280,
    margin: 5
  }
}))