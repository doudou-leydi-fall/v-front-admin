import React, { useCallback, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import PictureCard from "../Card/PictureCard";
import PictureModal from "../Modal/PictureModal";
import { Picture } from "../../../types";

interface LayoutPicturesStandardProps {
  pictures?: Picture[]
  cardType?: string
}

export default ({ pictures=[], cardType }: LayoutPicturesStandardProps) => {
  const classes = useStyles();
  const [img, setImg]:any = useState(null);

  const onClose = useCallback(() => {
    setImg(null);
  }, []);

  return (
    <>
      <div className={classes.container}>
        {pictures.map((picture, key) => (
          <div key={key} className={classes.card}>
            <PictureCard selectImg={() => setImg(picture)} key={key} item={picture} cardType={cardType} />
          </div>
          ))}
      </div>
      <PictureModal img={img} onClose={onClose}/>
    </>
  )
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "5px 10px"
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  card: {
    width: 280,
    margin: 5
  }
}))