import React, { useState } from "react";
import {
  StepLabel,
  Stepper,
  Step,
  Divider,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useLocalStorage } from "../Store";
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

export const StepperComponent = ({ stepsConfig, finalLink, onFinish, ...props }) => {
  let history = useHistory();
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [store, setStore] = useLocalStorage(stepsConfig.name);
  const [loading, setLoading] = useState(false);

  const handleNext = () => {
    if (activeStep + 1 < stepsConfig.steps.length) {
      setActiveStep(prevActiveStep => prevActiveStep + 1);
    } else {
      if (onFinish) {
        onFinish();
      } else {
        history.push(`${stepsConfig.finalLink}/${store.id}`);
      }
    }
  };

  const handleCancel = () => {
    if (stepsConfig.cancelLink) {
      history.push(stepsConfig.cancelLink);
    }
    history.goBack();
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const addToStore = data => {
    const newData = { ...store, ...data };
    setStore(newData);
  };

  return stepsConfig && stepsConfig.steps && stepsConfig.steps.length > 0 ? (
    <div className={classes.root}>
    <Stepper alternativeLabel activeStep={activeStep}>
        {stepsConfig.steps.map((step) => (
          <Step key={step.name}>
            <StepLabel>{step.name}</StepLabel>
          </Step>
        ))}
      </Stepper>
      {loading && <LinearProgress />}
      <Divider/>
      <div className={classes.instructions}>
        {React.cloneElement(stepsConfig.steps[activeStep].component, {
          handleNext,
          handleBack,
          handleReset,
          handleCancel,
          store,
          addToStore,
          setLoading,
          ...props
        })}
      </div>
    </div>
  ) : (
      <h4>Mauvaise configuration du stepper</h4>
    );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  instructions: {
    backgroundColor: "#F5F5F5",
  }
});
