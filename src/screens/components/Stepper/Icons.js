import React from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import { CircularProgress } from "@material-ui/core";
import { useColorlibStepIconStyles } from "./styles";

export const CustomStepIcon = props => {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed
      })}
    >
      {React.cloneElement(props.icon, {fontSize: "small"})}
    </div>
  );
};

export const CustomCircularProgress = () => (<CircularProgress size={30} thickness={4}/>);

CustomStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node
};
