import React from "react";
import {
  Card,
  CardHeader,
  CardMedia,
  Avatar,
  IconButton,
  makeStyles
} from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles(theme => ({
  media: {
    height: 190
  },
  src: {
    backgroundColor: red[500]
  }
}));

export const Thumbnail = ({
  title,
  subtitle,
  src,
  goTo,
  style
}:any) => {
  const classes:any = useStyles();

  return (
    <Card style={style} onClick={goTo} className={classes.card}>
      {title && subtitle ? (
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.src}>
              R
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title={title || "NR"}
          subheader={subtitle || "NR"}
        />
      ) : null}
      <ThumbnailImage src={src} classes={classes} />
    </Card>
  );
};

const ThumbnailImage = ({ src, height, width, classes }:any) =>
  src ? (
    <CardMedia style={{height: (height || 190)}} image={src} title="Paella dish" />
  ) : (
    <Skeleton variant="rect" height={height || 190}/>
  );
