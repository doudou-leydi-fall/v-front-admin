import React from "react";
import { CardMedia, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

interface ThumbnailImageProps {
  src: string 
  width: number
  height: number
}

export const ThumbnailImage = ({ src, width, height }:ThumbnailImageProps) => {
  return src ? (
    <CardMedia
      style={{ width: width, height: height }}
      image={src}
      title="Paella dish"
    />
  ) : (
    <Skeleton variant="rect" height={height || 180} />
  );
};
