import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import PlaceListPage from "./List";
import PlaceDetail from "./Details/Root";
import PlaceForm from "./Create/PlaceForm";

export const PlaceScreen = () => {
  let { path }:any = useRouteMatch();

  return (
    <div>
      <Switch>
        <Route path={`${path}/create`}>
          <PlaceForm />
        </Route>
        <Route path={`${path}/list`}>
          <PlaceListPage />
        </Route>
        <Route path={`${path}/:id`} children={<PlaceDetail />} />
      </Switch>
    </div>
  );
};
