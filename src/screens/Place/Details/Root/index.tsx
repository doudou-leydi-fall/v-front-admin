import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSnackbar } from "notistack";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setBackdrop } from "../../../../actions/ui/global";
import { GET_PLACE } from "../../../../network/index";
import { PlaceDetailsTabs } from "./PlaceDetailsTabs";
import { PlaceProvider } from "./PlaceStore";
import { QueryResult } from "react-apollo";


const PlaceDetail = (props: any) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let { id }:any = useParams();
  const { loading, error, data, refetch }: QueryResult<any, Record<string, any>> = useQuery(GET_PLACE, {
    variables: { id },
    fetchPolicy: "cache-and-network"
  });

  useEffect(() => {
    if (props.displayMode === 0) {
      refetch();
    }
  }, [props.displayMode]);

  props.setBackdrop(loading);

  if (error) {
    if (error.networkError) {
      enqueueSnackbar(String(error.networkError), { variant: "error" });
    }
    if (error.graphQLErrors) {
      error.graphQLErrors.map(error => {
        enqueueSnackbar(error.message, { variant: "error" });
      });
    }
  }

  return data && data.getPlace ? (
    <PlaceProvider
      value={{
        place: data.getPlace,
        refetchPlace: refetch
      }}>
      <div className={classes.root}>
        {/* <DetailsBar data={data.getPlace} /> */}
        <PlaceDetailsTabs place={data && data["getPlace"]} />
      </div>
    </PlaceProvider>
  ) : (
      <span>Veuillez patientez svp</span>
    );
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    border: "1px solid #edf2f9",
    boxShadow: "0 2px 4px rgba(126,142,177,.12)",
    marginBottom: 30
  }
}));

const mapDispatchToProps = (dispatch: any) => bindActionCreators({ setBackdrop }, dispatch);

const mapStateToProps = (state: any) => {
  return {
    displayMode: state.placeDetails.displayMode,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceDetail);
