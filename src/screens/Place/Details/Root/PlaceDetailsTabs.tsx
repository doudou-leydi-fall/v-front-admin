import React from "react";
import { TabsPanel } from "../../../components";
import ProductTab from "../ProductTab";
import CatalogueTab from "../CatalogueTab/CatalogueSectionContainer";
import GlobalTab from "../GlobalTab";
import QRCodeTab from "../QRCodeTab";
import PicturesTab from "../PicturesTab";
import ClassificationTab from "../ClassificationTab";
import ProgramTab from "../ProgramTab";

const tabs = [
  {
    label: "Générale",
    component: <GlobalTab />
  },
  {
    label: "Products",
    component: <ProductTab />
  },
  {
    label: "Programmes",
    component: <ProgramTab />
  },
  {
    label: "Catalogue",
    component: <CatalogueTab />
  },
  {
    label: "Classement Produits",
    component: <ClassificationTab />
  },
  {
    label: "QRCode",
    component: <QRCodeTab />
  },
  {
    label: "PHOTOS",
    component: <PicturesTab />
  }
];

export const PlaceDetailsTabs = ({ place }:any) => {
  return <TabsPanel initStore={{ id: place.id }} tabs={tabs} header={{ title: place.name, subtitle: place.categories && place.categories.join("-"), logo: (place.logo && place.logo.mobile) || place.src }}/>;
};
