import React from "react";

const PlaceContext = React.createContext({});

export const PlaceProvider = PlaceContext.Provider;
export const PlaceConsumer = PlaceContext.Consumer;
export default PlaceContext;

export const reducers = (state:any, action:any) => {
  switch (action.type) {
    case 'SET_DISPLAY_MODE':
      return { ...state, displayMode: action.payload }
    case 'OPEN_PRODUCT_DETAIL':
      return { ...state, displayMode: 1, selectedProductId: action.payload }
    default:
      return state;
  }
}