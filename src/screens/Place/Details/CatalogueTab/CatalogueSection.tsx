import React from "react";
import { Droppable, Draggable, DroppableProvided } from "react-beautiful-dnd";
import Section from "../../../components/Panel/Section";
import ProductCard from "../../../_views/ProductCard";

export default (props:any) => <Section {...props} childComponent={<ProductCard />} />
