import React, { useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { makeStyles } from '@material-ui/core/styles';
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { setBackdrop } from "../../../../actions/ui/global";
import PlaceContext from "../Root/PlaceStore";
import CatalogueSection from "./CatalogueSection";
import { InfoPanel } from "../../../components/Panel/InfoPanel";

const CatalogueTab = (props:any) => {
  const classes = useStyles();
  const { place }:any = useContext(PlaceContext);

  const handleDragEnd = ({ destination, source }:any) => {
    if (!destination) {
      return;
    }
  };

  return (
    <div>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable
          droppableId={props.index}
          type={"CARD"}
          isCombineEnabled={false}
        >
          {
            dropProvided => (
              <div {...dropProvided.droppableProps}>
                <div className={classes.root}>
                  {place.catalogue ? place.catalogue.map((section:any, key:number) =>
                    <CatalogueSection key={key} index={key} label={section.name} items={section.items} />) : <InfoPanel label="Pas de données" />}
                </div>
              </div>
            )
          }
        </Droppable>
      </DragDropContext>
    </div>
  )
}

const mapDispatchToProps = (dispatch:any) => bindActionCreators({ setBackdrop }, dispatch);

export default connect(null, mapDispatchToProps)(CatalogueTab);

const useStyles = makeStyles((theme:any) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: "0px 20px"
  }
}));