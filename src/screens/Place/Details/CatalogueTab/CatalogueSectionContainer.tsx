import React, { useState, useContext, useEffect, useMemo } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {useTranslation} from "react-i18next";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Button from '@material-ui/core/Button';
import RefreshIcon from '@material-ui/icons/Refresh';
import TouchAppIcon from '@material-ui/icons/TouchApp';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { useMutation } from "@apollo/react-hooks";
import { useSnackbar } from "notistack";
import Typography from '@material-ui/core/Typography';
import LayoutCard from "../../../components/Layout/LayoutCard";
import PlaceContext from "../Root/PlaceStore";
import ProductCard from "../../../_views/ProductCard";
import { UPDATE_CATALOGUE_ORDER } from "../../../../network";

// a little function to help us with reordering the result
const reorder = (list:any[], startIndex:number, endIndex:number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const getItemStyle = (isDragging:boolean, draggableStyle:any) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  margin: '0 0 8px 0',

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = (isDraggingOver:string) => ({
  background: isDraggingOver ? "lightblue" : "transparent"
});

export default () => {
  const classes = useStyles();
  const { place }:any = useContext(PlaceContext);
  const [changed, setChanged] = useState(false);
  const [updateCatalogueOrder, { error, loading }] = useMutation(UPDATE_CATALOGUE_ORDER);
  const {t, i18n} = useTranslation('common');
  const { enqueueSnackbar } = useSnackbar();
  const [catalogue, setCatalogue] = useState(place.catalogue);

  const isChanged = (oldArray:any[], newArray:any[]) => {
    // if length is not equal 
    if (oldArray.length != newArray.length)
      return true;
    else {
      // comapring each element of array 
      for (var i = 0; i < oldArray.length; i++)
        if (oldArray[i].order != newArray[i].order)
          return true;
      return false;
    }
  }

  const onDragEnd = ({ draggableId, source, destination }:any) => {
    // dropped outside the list
    if (!destination) {
      return;
    }
    console.log("onDragEnd");
    const items = reorder(catalogue, source.index, destination.index);

    if (isChanged(catalogue, items)) {
      const ordered = items.map((collection:any, index) => ({ ...collection, order: index + 1 }))
      setCatalogue(ordered);
      updateOrder(ordered, catalogue);
    }
  }

  const updateOrder = async (catalogueOrder:any[], oldOrder:any[]) => {
    try {
      const { data } = await updateCatalogueOrder({
        variables: {
          sellerid: place.id,
          order: catalogueOrder.map(({ id, order }) => ({ id, order }))
        }
      });

      if (data && data.updateCatalogueOrder) {
        enqueueSnackbar("Mise à jour effectuée", { variant: "success" });
      }
    } catch (e) {
      setCatalogue(oldOrder);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map((error:any) => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
    }
  }

  useEffect(() => {
    if (changed) {

    }
  }, [catalogue]);

  return (
    <div>
      <div className={classes.catalogue}>
        <Alert
        className={classes.alert}
          icon={loading ? <CircularProgress className={classes.progress} size={25} /> : <TouchAppIcon />}
          action={
            <ButtonGroup disableElevation variant="outlined" size="small" color="secondary">
              <Button className={classes.button} variant="outlined" size="small" color="secondary" startIcon={<RefreshIcon />}>Rafraichir</Button>
              <Button>Two</Button>
            </ButtonGroup>
          } severity="info">Vous pouvez changer la position des éléments en déplaçant les blocs
        </Alert>
        {catalogue && Array.isArray(catalogue) ?
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided:any, snapshot:any) => (
                <div {...provided.droppableProps} ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)}>
                  {catalogue.map((category, index) => (
                    <Draggable key={category.id} draggableId={category.id} index={index} >
                      {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}>
                          <ExpansionPanel>
                            <ExpansionPanelSummary id="panel1a-header" expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content">
                              <Typography variant="h3">{category.header && category.header.label}{category.position}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                              {category.sections && Array.isArray(category.sections) &&
                                category.sections.map((section:any) => <LayoutCard items={section.cards} childrenComponent={<ProductCard />} />)}
                            </ExpansionPanelDetails>
                          </ExpansionPanel>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext> : (<div>loading</div>)}
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  catalogue: {
    padding: "0 20px 20px 20px"
  },
  section: {
    marginBottom: 10,
    marginTop: 10
  },
  button: {
    margin: "0 2px"
  },
  alert: {
    margin: "10px 0"
  },
  progress: {
    margin: "0 5px"
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  }
}))
