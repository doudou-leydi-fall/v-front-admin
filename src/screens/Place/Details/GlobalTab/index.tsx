import React, { useState, useContext, Fragment } from "react";
import moment from 'moment';
import { connect } from "react-redux";
import { setDisplayMode } from "../../../../actions/place/details";
import { setBackdrop } from "../../../../actions/ui/global";
import {
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Paper
} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import PlaceContext from "../Root/PlaceStore";
import PlaceUpdateForm from "./PlaceUpdateForm";
import { bindActionCreators } from "redux";

const GlobalTab = (props:any) => {
  //const [display, setDisplay] = useState(0);
  const { place }:any = useContext(PlaceContext);
  return (
    <Fragment>
      <PlaceUpdateForm place={place} setDisplay={props.setDisplayMode} />
    </Fragment>
  );
}

interface InfoItemProps {
  label: string 
  icon?: any
  value: any
}

const InfoItem = ({ label, icon, value }:InfoItemProps) => {
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar>{icon}</Avatar>
      </ListItemAvatar>
      <ListItemText primary={label} secondary={value} />
    </ListItem>
  );
};

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  toolbar: {
    alignItems: 'flex-end'
  },
}));

const mapStateToProps = (state:any) => {
  return {
    displayMode: state.placeDetails.displayMode,
  }
};

const mapDispatchToProps = (dispatch:any) => bindActionCreators(
  {
    setDisplayMode,
    setBackdrop,
  },
  dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GlobalTab);