import React, { useContext } from "react";
import { Formik } from "formik";
import * as Yup from 'yup';
import { useSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { UPDATE_PLACE } from "../../../../network";
import { TextField } from "../../../components";
import CountryField from "../../../components/CustomForms/CountryField";
import ZoneField from "../../../components/CustomForms/ZoneField";
import SwitchField from "../../../components/Forms/SwitchField";
import DistrictField from "../../../components/CustomForms/DistrictField";
import PlaceCategoryField from "../../../components/CustomForms/PlaceCategoryField";
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import { setBackdrop } from "../../../../actions/ui/global";
import { setDisplayMode } from "../../../../actions/place/details";
import { bindActionCreators } from "redux";
import { useMutation } from "@apollo/react-hooks";
import ColorField from "../../../components/CustomForms/ColorField";
import diff from "../../../../utils/diff";
import PlaceContext from "../Root/PlaceStore";
import { RootContext, UI_REDUCER_TYPES } from "../../../../RootContext";

const PlaceUpdateForm = ({ place, setDisplay, setBackdrop }: any) => {
  const classes = useStyles();
  const { uidispatch } = useContext(RootContext);
  const { refetchPlace }: any = useContext(PlaceContext);
  const [updatePlace, { error, loading }] = useMutation(UPDATE_PLACE);
  const { enqueueSnackbar } = useSnackbar();
  setBackdrop(loading);
  const handleSubmit = async (values: any, actions: any) => {
    const changes = diff(values, place);

    try {
      const { data }: any = await updatePlace({
        variables: { id: place.id, place: changes }
      });

      if (data && data.updatePlace) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Lieu ajouté",
            variant: "success"
          }
        });
      }

      setDisplay(0);
    } catch (e) {
      console.log("e LOG", e);
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    email: Yup.string()
      .email('Email invalide')
      .required('Ce champ est obligatoire'),
    phone: Yup.string()
      .min(2, 'Numero invalide')
      .max(50, 'Numero invalide')
  });

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate className={classes.root} onSubmit={props.handleSubmit}>
        <Grid container spacing={1}>
          <Grid item sm={3}>
            <TextField
              label="Référence Vendeur"
              name="id"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.id}
              error={props.errors.id}
              disabled={true}
            />
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
            <TextField
              label="Email"
              name="email"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.email}
              error={props.errors.email}
            />
          </Grid>
          <Grid item sm={3}>
            <TextField
              label="Téléphone"
              name="phone"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.phone}
              error={props.errors.phone}
            />
            <CountryField
              setFieldValue={props.setFieldValue}
              value={props.values.location.country}
            />
          </Grid>
          <Grid item sm={3}>
            <ZoneField
              setFieldValue={props.setFieldValue}
              value={props.values.location.zone}
            />
            <DistrictField
              setFieldValue={props.setFieldValue}
              value={props.values.location.district}
              zone={props.values.location && props.values.location.zone}
            />
          </Grid>
          <Grid item sm={3}>
            <SwitchField
              name="location.ocean"
              label="À proximité de l'océan"
              value={props.values.location.ocean}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={12}>
            <PlaceCategoryField
              value={props.values.categories}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={12}>
            <TextField
              label="Adresse compléte"
              name="location.address"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.location && props.values.location.address}
              error={props.errors.location && props.errors.location.address}
              multiline
              rows="3"
            />
          </Grid>
          <Grid item sm={12}>
            <TextField
              label="Description"
              name="description"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.description}
              error={props.errors.description}
              multiline
              rows="5"
            />
          </Grid>
          <Grid container spacing={1} sm={12}>
            <Grid item sm={3}>
              <ColorField
                label="Couleur primaire"
                name="THEME.primaryColor"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.THEME && props.values.THEME.primaryColor}

              />
            </Grid>
            <Grid item sm={3}>
              <ColorField
                label="Couleur secondaire"
                name="THEME.secondaryColor"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.THEME && props.values.THEME.secondaryColor}
              />
            </Grid>
            <Grid item sm={3}>
              <ColorField
                label="Couleur tertiaire"
                name="THEME.tertiaryColor"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.THEME && props.values.THEME.tertiaryColor}
              />
            </Grid>
          </Grid>
          <Grid sm={12}>
            <ButtonGroup variant="contained" color="primary">
              <Button onClick={() => setDisplay(0)}>Annuler</Button>
              <Button type="submit">Valider</Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
    );
  };

  const initialValues = {
    id: place.id,
    name: place.name || "",
    location: {
      country: place.location && place.location.country,
      zone: place.location && place.location.zone,
      district: place.location && place.location.district,
      address: place.location && place.location.address || "Non rens.",
      ocean: place.location && place.location.ocean,
    },
    email: place.email,
    phone: place.phone,
    categories: place.categories || [],
    THEME: {
      primaryColor: place.THEME && place.THEME.primaryColor,
      secondaryColor: place.THEME && place.THEME.secondaryColor,
      tertiaryColor: place.THEME && place.THEME.tertiaryColor
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  )
}

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({ setBackdrop, setDisplayMode }, dispatch);

export default connect(null, mapDispatchToProps)(PlaceUpdateForm);
