import React, { useContext } from "react";
import LayoutCard from "../../../components/Layout/LayoutCard";
import { Card } from "../../../components/Card";
import { makeStyles } from '@material-ui/core/styles';
import { InfoPanel } from "../../../components/Panel/InfoPanel";
import { ProductContext } from "./ProductStore";

export default () => {
  const classes = useStyles();
  const { products = [], dispatch }: any = useContext(ProductContext);

  const renderCard = (product: any) => (
    <div className={classes.cardContainer} onClick={() => dispatch({ type: "OPEN_PRODUCT_DETAIL", payload: product })}>
      <Card
        key={product.id}
        title={product.name}
        content={product.recipes && product.recipes.map((recipe:any) => recipe.name).join(" - ")}
        picture={product.src}
        chipTitle={product.collection && product.collection.name}
        chipFooterLeft={`${product.price} FCFA`}
        imageBackgroundSize="contain"
      />
    </div>
  )

  return (products.data && products.data.length > 0) ? (
    <div className={classes.container}>
      {products.data.map((product: any) => renderCard(product))}
    </div>
  ) : (
      <InfoPanel label="Pas de données" />
    );
};

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: "#f8f9fa !important",
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    paddingLeft: 5
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardContainer: {
    height: 330,
    width: 280,
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 5
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));
