import React, { useContext } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Pricing from './Pricing';
import Classification from './Classification';
import { AppBar, Toolbar, IconButton, Button, Icon, CircularProgress } from '@material-ui/core';
import Main from './Main';
import { ProductContext } from '../ProductStore';
import PhotoUploader from "./Uploader";
import Presentation from "./Presentation";
import Sheet from './Sheet';
import ButtonLoading from '../../../../components/Button/ButtonLoading';

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const tabs: any = [
  {
    label: "Presentation",
    component: Presentation,
    order: 1
  },
  {
    label: "Fiche descriptive",
    component: Sheet,
    order: 2
  },
  {
    label: "Bases",
    component: Main,
    order: 3
  },
  {
    label: "Classification",
    component: Classification,
    order: 4
  },
  {
    label: "Pricing",
    component: Pricing,
    order: 5
  },
  {
    label: "Photos",
    component: PhotoUploader,
    order: 6
  },
];

export default (props: any) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { placeid, dispatch }: any = useContext(ProductContext);
  const [loading, setLoading]:any = React.useState(false);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.container}>
      <AppBar position="static" color="primary">
        <Toolbar variant="dense" className={classes.toolbar}>
          <Typography variant="h6" color="inherit">{props.product.name}</Typography>
          <div>
            {loading && <ButtonLoading loading={true} color="secondary"/>}
            <Button
              variant="contained"
              className={classes.toolbarButton}
              startIcon={<Icon>delete_variant</Icon>}
              color="primary"
              onClick={() => dispatch({ type: "SET_DISPLAY_MODE", payload: 0 })}
            >Supprimer</Button>
            <Button
              variant="contained"
              className={classes.toolbarButton}
              startIcon={<Icon>close_circle</Icon>}
              color="primary"
              onClick={() => dispatch({ type: "SET_DISPLAY_MODE", payload: 0 })}
            >Fermer</Button>
          </div>
        </Toolbar>
      </AppBar>
      <div className={classes.root}>
        <div className={classes.tabsContainer}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            className={classes.tabs}
          >
            {tabs.map((tab: any, key: number) => (<Tab label={tab.label} {...a11yProps(key)} />))}
          </Tabs>
        </div>
        <div className={classes.tabsPanelContainer}>
          {tabs.map((tab: any, key: number) => (
            <Box p={3}>
              <Typography component="div">
                <TabPanel value={value} index={key}>
                  {React.createElement(tab.component, { ...props, setLoading, owner: placeid })}
                </TabPanel>
              </Typography>
            </Box>
          ))}
        </div>
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    marginTop: 10
  },
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  tabsPanelContainer: {
    background: "#FFFFFF",
    flexGrow: 1,
    padding: 20
  },
  tabsContainer: {
    flexBasis: 200
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    width: 200,
    backgroundColor: "#F5F5F5"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  toolbarButton: {
    marginLeft: 3,
    marginRight: 3
  }
}));