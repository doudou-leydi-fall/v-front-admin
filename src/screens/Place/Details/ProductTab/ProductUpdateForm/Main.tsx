import React, { useContext } from "react";
import { Formik } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "@apollo/react-hooks";
import { UPDATE_PRODUCT } from "../../../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../../../RootContext";
import GeneralForm from "../ProductForms/GeneralForm";
import { Grid, Button } from "@material-ui/core";
import ButtonLoading from "../../../../components/Button/ButtonLoading";

export default ({ product, refetch, owner, setLoading }: any) => {
  const { uidispatch } = useContext(RootContext);
  const [applyMutation, { error, loading: updating }]: any = useMutation(UPDATE_PRODUCT.MUTATION);
  setLoading(updating);

  const handleSubmit: any = async (values: any, actions: any) => {
    try {
      const { data } = await applyMutation({
        variables: {
          id: product.id,
          sellerid: owner,
          product: parseUploadProduct(values)
        }
      });
      if (data && data[UPDATE_PRODUCT.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Effectuée",
            variant: "success"
          }
        })
        refetch();
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <GeneralForm {...props} />
        <Grid>
          {/* <Button onClick={() => dispatch({ type: "SET_DISPLAY_MODE", payload: 0 })} color="primary">Annuler</Button> */}
          <Button onClick={props.handleReset} color="primary">Annuler</Button>
          <ButtonLoading
            type="submit"
            color="primary"
            label="Mettre à jour"
            loading={updating}
          />
        </Grid>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{
        id: product && product.id,
        name: product && product.name,
        type: product && product.type,
        programStartAt: product && product.program && product.program.start,
        programEndAt: product && product.program && product.program.end,
        spicy: product && product.spicy,
        recipes: product && product.recipes,
        stock: product && product.stock,
        gastronomy: product && product.gastronomy,
        description: product && product.description
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
    />
  );
};

const parseUploadProduct = ({
  name,
  type,
  description,
  gastronomy,
  stock,
  spicy,
  recipes,
  programStartAt,
  programEndAt
}: any) => ({
  name,
  type,
  description,
  gastronomy,
  stock,
  spicy,
  program: {
    start: programStartAt,
    end: programEndAt
  },
  recipes: Array.isArray(recipes) && recipes.map(({ id, name }: any) => ({ id, name }))
})

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    padding: 15,
    borderRadius: 8,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
});
