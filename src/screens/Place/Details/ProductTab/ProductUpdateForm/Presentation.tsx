import React from "react";
import Grid from "@material-ui/core/Grid";
import Divider from '@material-ui/core/Divider';
import copy from "copy-to-clipboard";
import { ListItem, ListItemText, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@material-ui/core";
import TooltipChip from "../../../../components/Forms/Chip/TooltipChip";
import Alert from "@material-ui/lab/Alert";

export default ({ product }: any) => {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item sm={6}>
        <Typography className={classes.title} variant="h4">Générale</Typography>
        <ListItem>
          <ListItemText primary="ID" />
          <TooltipChip color="PURPLE" value={product.id} onClick={() => copy(product.id)}/>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Model" />
          <TooltipChip value={product.model} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Type de produit" />
          <TooltipChip value={product.type} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Prix" />
          <TooltipChip value={product.price} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Devise" />
          <TooltipChip value={product.currency} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Quantité" />
          <span>{product.quantity}</span>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Taille" />
          <span>{product.size}</span>
        </ListItem>
        <Divider />
      </Grid>
      <Grid item sm={6}>
        <Typography className={classes.title} variant="h4">Classification</Typography>
        <ListItem>
          <ListItemText primary="Type de carte" />
          <TooltipChip color="ORANGE" value={product.cardType} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText
            primary="Catégorie"
            secondary={product.category && product.category.id} />
          <TooltipChip value={product.category && product.category.name} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Catégorie rattachée" secondary={product.categoryCatalogue && product.categoryCatalogue.id} />
          <TooltipChip value={product.categoryCatalogue && product.categoryCatalogue.name} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Nom Collection" secondary={product.collection && product.collection.id} />
          <TooltipChip value={product.collection && product.collection.name} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Collection rattachée" secondary={product.collectionCatalogue && product.collectionCatalogue.id} />
          <TooltipChip value={product.collectionCatalogue && product.collectionCatalogue.name} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary="Description" secondary={product.description || "Non Rens."} />
        </ListItem>
      </Grid>
      <Grid item sm={12}>
        <Typography className={classes.title} variant="h4">Pricing</Typography>
        {Array.isArray(product.pricing) ? <PricingTable pricing={product.pricing} /> : <Alert severity="error">Pas de prix</Alert>}
      </Grid>
      <Grid item sm={12} className={classes.sheetTableContainer}>
        <Typography className={classes.title} variant="h4">Fiche de detail</Typography>
        {Array.isArray(product.sheet) ? <SheetTable sheet={product.sheet} /> : <Alert severity="error">Pas de donnée</Alert>}
      </Grid>
    </Grid>
  )
}

const PricingTable = ({ pricing = [], classes }: any) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="left">Numero</TableCell>
            <TableCell align="left">Prix</TableCell>
            <TableCell align="left">Nom</TableCell>
            <TableCell align="left">Devise</TableCell>
            <TableCell align="left">Période</TableCell>
            <TableCell align="left">Quantité</TableCell>
            <TableCell align="left">Taille</TableCell>
            <TableCell align="left">Date début</TableCell>
            <TableCell align="left">Date fin</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {pricing.map((p: any) => (
            <TableRow key={p.name}>
              <TableCell align="left">{p.id}</TableCell>
              <TableCell align="left">{p.price}</TableCell>
              <TableCell align="left">{p.name}</TableCell>
              <TableCell align="left">{p.currency}</TableCell>
              <TableCell align="left">{p.period}</TableCell>
              <TableCell align="left">{p.quantity}</TableCell>
              <TableCell align="left">{p.size}</TableCell>
              <TableCell align="left">{p.startDate}</TableCell>
              <TableCell align="left">{p.endDate}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

const SheetTable = ({ sheet = [], classes }: any) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="left">Numero</TableCell>
            <TableCell align="left">Nom</TableCell>
            <TableCell align="left">Valeur</TableCell>
            <TableCell align="left">Commentaire</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sheet.map((p: any) => (
            <TableRow key={p.name}>
              <TableCell align="left">{p.id}</TableCell>
              <TableCell align="left">{p.name}</TableCell>
              <TableCell align="left">{p.value}</TableCell>
              <TableCell align="left">{p.comment}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

const useStyles = makeStyles(() => ({
  title: {
    marginBottom: 10
  },
  sheetTableContainer: {
    marginTop: 40
  }
}),
);