import React, { useContext, useState } from "react";
import { Formik } from "formik";
import { useMutation } from "react-apollo";
import * as Yup from 'yup';
import { UPDATE_PRODUCT_CLASSIFICATION } from "../../../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../../../RootContext";
import { ProductContext } from "../ProductStore";
import ClassificationForm from "../ProductForms/ClassificationForm";

export default ({ refetch, product, setLoading }: any) => {
  const { uidispatch } = useContext(RootContext);
  const { placeid: owner }: any = useContext(ProductContext);
  const [applyMutation, { loading:updating }]: any = useMutation(UPDATE_PRODUCT_CLASSIFICATION.MUTATION);
  setLoading(updating);

  const handleSubmit: any = async (values: any, actions: any) => {
    try {
      const { data } = await applyMutation({
        variables: parseClassification(values)
      });
      if (data && data[UPDATE_PRODUCT_CLASSIFICATION.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Effectuée",
            variant: "success"
          }
        })
        refetch()
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const ValidationSchema = Yup.object().shape({
    categoryCatalogue: Yup.string()
      .required('Champ obligatoire'),
    collectionCatalogue: Yup.string()
      .required('Champ obligatoire')
  });

  return (
    <Formik
      onSubmit={handleSubmit}
      render={ClassificationForm}
      initialValues={{
        owner,
        id: product && product.id,
        category: product && product.category,
        collection: product && product.collection,
        categoryCatalogue: product && product.categoryCatalogue,
        collectionCatalogue: product && product.collectionCatalogue,
      }}
      validationSchema={ValidationSchema}
    />
  );
}

const parseClassification = ({ id, category, categoryCatalogue, collection, collectionCatalogue }: any) => ({
  id,
  category: {
    id: category.id,
    name: category.name,
  },
  categoryCatalogue: {
    id: categoryCatalogue.id,
    name: categoryCatalogue.name
  },
  collection: {
    id: collection.id,
    name: collection.name
  },
  collectionCatalogue: {
    id: collectionCatalogue.id,
    name: collectionCatalogue.name
  }
})