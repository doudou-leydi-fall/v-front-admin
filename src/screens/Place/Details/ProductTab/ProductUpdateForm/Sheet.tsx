import React, { useContext } from "react";
import { Formik } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "react-apollo";
import { UPDATE_PRODUCT } from "../../../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../../../RootContext";
import SheetForm from "../ProductForms/SheetForm";
import ButtonLoading from "../../../../components/Button/ButtonLoading";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default ({ product, refetch, owner, setLoading }: any) => {
  const [applyMutation, { loading: submitting }]: any = useMutation(UPDATE_PRODUCT.MUTATION);
  const { uidispatch } = useContext(RootContext);
  setLoading(submitting);

  const handleSubmit: any = async (values: any, actions: any) => {
    try {
      const { data } = await applyMutation({
        variables: {
          id: product.id,
          sellerid: owner,
          product: { sheet: values.sheet.map(({ id, name, value, comment }: any) => ({ id, name, value, comment })) }
        }
      });
      if (data && data[UPDATE_PRODUCT.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Effectuée",
            variant: "success"
          }
        })
        refetch();
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <FieldBuilder
          name="sheet"
          fieldname="SHEET"
          value={props.values["sheet"]}
          errors={props.errors["sheet"]}
          setFieldValue={props.setFieldValue}
        />
        <div>
          <ButtonLoading
            type="submit"
            color="primary"
            variant="contained"
            label="Valider la tarification"
            loading={submitting}
          />
        </div>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{
        id: product.id,
        sheet: product && product.sheet
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  title: {
    marginBottom: 10,
    marginTop: 30
  }
});
