import React, { useContext } from "react";
import { Formik } from "formik";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";
import { useMutation } from "react-apollo";
import { UPDATE_PRODUCT_PRICING } from "../../../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../../../RootContext";
import ButtonLoading from "../../../../components/Button/ButtonLoading";
import PricingForm from "../ProductForms/PricingForm";

export default ({ product, refetch, setLoading }: any) => {
  const [applyMutation, { loading: submitting }]: any = useMutation(UPDATE_PRODUCT_PRICING.MUTATION);
  setLoading(submitting);
  const { uidispatch } = useContext(RootContext);

  const handleSubmit: any = async (values: any, actions: any) => {
    try {
      const { data } = await applyMutation({
        variables: { id: values.id, pricing: values.pricing.map((p: any) => parsePricing(p)) }
      });
      if (data && data[UPDATE_PRODUCT_PRICING.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Effectuée",
            variant: "success"
          }
        })
        refetch()
      }
    }
    catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <FieldBuilder
          name="pricing"
          fieldname="PRICES"
          value={props.values["pricing"]}
          errors={props.errors["pricing"]}
          setFieldValue={props.setFieldValue}
        />
        <div>
          <Button onClick={props.handleReset} color="primary">Annuler</Button>
          <ButtonLoading
            label="Mettre à jour"
            type="submit"
            color="primary"
            loading={submitting}
          />
        </div>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{
        id: product.id,
        pricing: product && product.pricing
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
    />
  );
};

const parsePricing = ({
  id,
  price,
  currency,
  startDate,
  endDate,
  period,
  size,
  quantity,
  main
}: any) => ({
  id,
  price,
  currency,
  startDate,
  endDate,
  period,
  size,
  quantity,
  main
})

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  title: {
    marginBottom: 10,
    marginTop: 30
  }
});
