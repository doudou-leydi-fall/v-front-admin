import React from "react";
import ButtonLoading from "../../../../components/Button/ButtonLoading";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default (props: any) => (
  <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
    <FieldBuilder
      name="sheet"
      fieldname="SHEET"
      value={props.values["sheet"]}
      errors={props.errors["sheet"]}
      setFieldValue={props.setFieldValue}
    />
    <div>
      <ButtonLoading type="submit" color="primary" variant="contained" label="Valider la tarification" />
    </div>
  </form>
);