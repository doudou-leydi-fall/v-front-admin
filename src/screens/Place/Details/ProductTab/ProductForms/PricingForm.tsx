import { Button } from "@material-ui/core";
import React from "react";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default (props: any) => {
  return (
    <FieldBuilder
      name="pricing"
      fieldname="PRICES"
      value={props.values["pricing"]}
      errors={props.errors["pricing"]}
      setFieldValue={props.setFieldValue}
    />
  );
}