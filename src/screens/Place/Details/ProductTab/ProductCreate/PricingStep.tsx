import React, { useContext } from "react";
import { Formik } from "formik";
import { Button } from "@material-ui/core";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";
import { useMutation } from "react-apollo";
import { CREATE_PRODUCT } from "../../../../../network";
import { RootContext, UI_REDUCER_TYPES } from "../../../../../RootContext";

export default ({ store, handleBack, saveAndNext }: any) => {
  const { uidispatch } = useContext(RootContext);
  const [applyMutation, { error, loading: updating }]: any = useMutation(CREATE_PRODUCT.MUTATION);
  
  const handleSubmit: any = async (values: any) => {
    try {
      const { data } = await applyMutation({
        variables: {
          product: { ...store, ...values }
        }
      });
      if (data && data[CREATE_PRODUCT.OPERATION]) {
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Effectuée",
            variant: "success"
          }
        })
        saveAndNext({ id: data[CREATE_PRODUCT.OPERATION] });
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
    }
  };

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <FieldBuilder
          name="pricing"
          fieldname="PRICES"
          value={props.values["pricing"]}
          errors={props.errors["pricing"]}
          setFieldValue={props.setFieldValue}
        />
        <div>
          <Button onClick={props.handleReset} color="primary">Retour</Button>
          <Button
            type="submit"
            color="primary"
          >Suivant</Button>
        </div>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{
        id: store.id,
        pricing: store && store.pricing
      }}
      onSubmit={handleSubmit}
      onReset={() => handleBack()}
      render={RenderForm}
    />
  );
};
