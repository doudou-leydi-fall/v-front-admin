import React from "react";
import Stepper from "../../../../../components/Stepper";
import GeneralStep from "./GeneralStep";
import PricingStep from "./PricingStep";
import SheetStep from "./SheetStep";
import SummaryStep from "./SummaryStep";
import UploadStep from "./UploadStep";

const steps:any = [
  {
    label: "Générale",
    component: GeneralStep
  },
  {
    label: "Fiche descriptive",
    component: SheetStep
  },
  {
    label: "Pricing",
    component: PricingStep
  },
  {
    label: "Récapitulatif",
    component: SummaryStep
  },
  {
    label: "Photos",
    component: UploadStep
  },
]

export default ({ sellerid }:any) => (<Stepper steps={steps} sellerid={sellerid}/>);
