import React, { useContext, useState, useMemo } from "react";
import { useQuery } from "react-apollo";
import { makeStyles } from "@material-ui/core";
import { GET_CARD } from "../../../../network";
import { ProductContext } from "./ProductStore";
import ProductUpdateForm from "./ProductUpdateForm";
//import ProductPanelUploader from "./ProductUpdateForm/Uploader";

export default () => {
  const classes = useStyle();
  const { state, dispatch }:any = useContext(ProductContext);
  const [product, setProduct]:any = useState(null);

  const { data, refetch }:any = useQuery(GET_CARD.QUERY, { 
    variables: { 
      model: "PRODUCT",
      id: state.selectedProduct.id 
    }, 
    fetchPolicy: "no-cache" 
  });

  useMemo(() => {
    if (data && data[GET_CARD.OPERATION]) {
      setProduct(data[GET_CARD.OPERATION]);
    }
  }, [data]);

  return (
    <div>
      {product ?
        <div className={classes.container}>
          <ProductUpdateForm refetch={refetch} product={product} />
          {/* <ProductPanelUploader refetch={refetch} {...product} /> */}
        </div> : null}
    </div>
  )
}

const useStyle = makeStyles({
  container: {

  },
  title: {
    marginLeft: 25
  },
  toolbar: {
    paddingLeft: 45,
    paddingRight: 45
  },
  button: {
    marginLeft: 3,
    marginRight: 3
  }
});