import { createContext } from "react";

export const ProductContext = createContext({});
export const ProductProvider = ProductContext.Provider;
export const ProductConsumer = ProductContext.Consumer;

export const reducers = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_DISPLAY_MODE':
      return { ...state, displayMode: action.payload }
    case 'OPEN_PRODUCT_DETAIL':
      return { ...state, displayMode: 1, selectedProduct: action.payload }
    default:
      return state;
  }
}

