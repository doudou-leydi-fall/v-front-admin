import React, { useContext } from "react";
import * as Yup from 'yup';
import { SEARCH_META, ADD_SELLER_CATEGORY, DELETE_ROOT_CLASSIFICATION, ADD_SELLER_COLLECTION, DELETE_SELLER_CLASSIFICATION } from "../../../../network";
import ClassificationTemplate from "../../../../templates/Classification";
import PlaceContext from "../Root/PlaceStore";

const config: any = {
  entity: "Metadonnée",

  FETCH_CATEGORY: {
    QUERY: SEARCH_META,
    OPERATION: "searchMeta",
    model: "CLASSIFICATION",
    parseVariables: ({ owner }: any) => ({
      model: "Category",
      type: "search",
      criteria: [
        {
          name: "model",
          value: "Category"
        },
        {
          name: "owner",
          value: owner
        },
      ]
    })
  },

  ADD_CATEGORY: {
    MUTATION: ADD_SELLER_CATEGORY,
    OPERATION: "addSellerCategory",
    FIELDS: [{ name: "rootCategory", fieldName: "ROOT_CATEGORY"}],
    VALIDATION_SCHEMA: Yup.object().shape({
      name: Yup.string()
        .min(2, 'Ce nom est trop court')
        .max(50, 'Ce nom dépasse la taille maximale')
        .required('Champ obligatoire'),
      rootCategory: Yup.string()
        .required('Champ obligatoire'),
    })
  },

  DELETE_CATEGORY: {
    MUTATION: DELETE_SELLER_CLASSIFICATION,
    OPERATION: "deleteSellerClassification",
  },

  FETCH_COLLECTION: {
    QUERY: SEARCH_META,
    OPERATION: "searchMeta",
    MODEL: "CLASSIFICATION",
    getVariables: ({ id }: any) => ({
      model: "Collection",
      type: "search",
      criteria: [
        {
          name: "model",
          value: "Collection"
        },
        {
          name: "parentID",
          value: id
        },
      ],
    })
  },

  ADD_COLLECTION: {
    MUTATION: ADD_SELLER_COLLECTION,
    OPERATION: "addSellerCollection",
    FIELDS: [{ name: "rootCollection", fieldName: "ROOT_COLLECTION"}],
    VALIDATION_SCHEMA: Yup.object().shape({
      name: Yup.string()
        .min(2, 'Ce nom est trop court')
        .max(50, 'Ce nom dépasse la taille maximale')
        .required('Champ obligatoire'),
      rootCollection: Yup.string()
        .required('Champ obligatoire'),
    })
  },

  DELETE_COLLECTION: {
    MUTATION: DELETE_SELLER_CLASSIFICATION,
    OPERATION: "deleteSellerClassification",
  },
}

export default () => {
  const { place }:any = useContext(PlaceContext);

  return (
    <ClassificationTemplate config={config} owner={place && place.id}/>
  )
}