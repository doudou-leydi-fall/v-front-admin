import React, { Fragment, useContext, useReducer } from "react";
import { Query, useQuery } from "react-apollo";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core";
import PlaceContext from "../Root/PlaceStore";
import ProductListPanel from "./ProductListPanel";
import AddIcon from '@material-ui/icons/Add';
import { SEARCH_PRODUCT } from "../../../../network";
import ProductFilterMenu from "./ProductFilterMenu";
import ProductDetailPanel from "./ProductDetailPanel";
import ProductCreate from "./ProductCreate";
import { ProductProvider, ProductContext, reducers } from "./ProductStore";
import { Alert } from "@material-ui/lab";

const initState = {
  createMode: 0,
  displayMode: 0,
  selectedProductId: "",
}

export default () => {
  const [state, dispatch] = useReducer(reducers, initState);
  const { place }: any = useContext(PlaceContext);
  const { data, refetch } = useQuery(SEARCH_PRODUCT, {
    variables: {
      size: 100,
      from: 0,
      criteria: [{
        name: "seller.id",
        value: place.id
      },
      {
        name: "type",
        value: "PROGRAM"
      }]
    }
  }
  );

  const renderContent = (mode: number) => {
    switch (mode) {
      case 0:
        return <ProductSearchPanel />;
      case 1:
        return <ProductDetailPanel />;
      case 2:
        return <ProductCreate sellerid={place.id}/>;
      default:
        return null;
    }
  }

  return (
    <ProductProvider value={{
      placeid: place.id,
      sellerid: place.id,
      products: data && data["searchProduct"],
      state,
      dispatch,
      refetchProducts: refetch
    }}>
      {renderContent(state.displayMode)}
    </ProductProvider>
  );
};

const ProductSearchPanel = () => {
  const classes = useStyle();
  const { placeid, dispatch, products }: any = useContext(ProductContext);

  return (
    <Fragment>
      <AppBar
        position="static"
        color="default">
        <Toolbar>
          <Button
            color="inherit"
            startIcon={<AddIcon />}
            onClick={() => dispatch({ type: "SET_DISPLAY_MODE", payload: 2 })}
          >Ajouter</Button>
        </Toolbar>
      </AppBar>
      {
        products && products.data.length > 0 ?
          <div className={classes.bodyList}>
            <ProductFilterMenu setCategories={() => console.log("to bo given")} placeid={placeid} />
            <ProductListPanel />
          </div> : <Alert severity="info">Pas de produit</Alert>
      }
    </Fragment>
  );
}

const useStyle = makeStyles({
  bodyList: {
    display: "flex"
  },
});
