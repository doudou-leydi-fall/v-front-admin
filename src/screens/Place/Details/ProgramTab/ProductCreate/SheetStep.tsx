import React from "react";
import { Formik } from "formik";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";
import Button from "@material-ui/core/Button";

export default ({ store, saveAndNext, handleBack }: any) => {

  const RenderForm = (props: any) => {
    return (
      <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
        <FieldBuilder
          name="sheet"
          fieldname="SHEET"
          value={props.values["sheet"]}
          errors={props.errors["sheet"]}
          setFieldValue={props.setFieldValue}
        />
        <div>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            onClick={props.handleReset}
          >Retour</Button>
          <Button
            type="submit"
            color="primary"
            variant="contained"
          >Suivant</Button>
        </div>
      </form>
    );
  }

  return (
    <Formik
      initialValues={{
        sheet: store.sheet
      }}
      onSubmit={(values) => saveAndNext(values)}
      onReset={() => handleBack()}
      render={RenderForm}
    />
  );
};
