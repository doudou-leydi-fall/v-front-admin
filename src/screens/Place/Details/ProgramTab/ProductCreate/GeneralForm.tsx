import React, { useState } from "react";
import { FormControlLabel, Grid, Radio, RadioGroup } from "@material-ui/core";
import { TextField } from "../../../../components";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default (props: any) => {
  const [type, setType]: any = useState("FOOD");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setType((event.target as HTMLInputElement).value);
    props.setFieldValue("type", event.target.value);
  };

  return (
    <Grid container spacing={1}>
      <Grid item sm={12}>
        <TextField
          label="Nom"
          name="name"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.name}
          error={props.errors.name}
        />
      </Grid>
      <Grid sm={12}>
        <RadioGroup row aria-label="position" name="position" onChange={handleChange}>
          <FormControlLabel value="FOOD" control={<Radio color="primary" />} label="FOOD" />
          <FormControlLabel value="PROGRAM" control={<Radio color="primary" />} label="PROGRAM" />
          <FormControlLabel value="SUBSCRIPTION" control={<Radio color="primary" />} label="SUBSCRIPTION" />
          <FormControlLabel value="REALSTATE" control={<Radio color="primary" />} label="REALSTATE" />
          <FormControlLabel value="DEVICES_RENT" control={<Radio color="primary" />} label="DEVICES_RENT" />
        </RadioGroup>
      </Grid>
      <Grid item sm={12}>
        <FieldBuilder
          name="classification"
          fieldname="PRODUCT_CLASSIFICATION"
          value={props.values}
          errors={props.errors}
          setFieldValue={props.setFieldValue}
          owner={props.owner}
          type={type}
          displayParent={false}
        />
      </Grid>
      { React.createElement(FormType[type], { ...props })}
      <Grid sm={12}>
        <FieldBuilder
          name="recipes"
          fieldname="RECIPES"
          value={props.values["recipes"]}
          errors={props.errors["recipes"]}
          setFieldValue={props.setFieldValue}
          owner={props.owner}
          type={type}
        />
      </Grid>
      <Grid sm={12}>
        <TextField
          label="Description"
          name="description"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.description}
          error={props.errors.description}
          multiline
          rows="5"
        />
      </Grid>
    </Grid>
  )
}

const Food = (props: any) => (<div></div>);


const Program = (props: any) => (
  <Grid container sm={12} spacing={1}>
    <Grid item sm={6}>
      <TextField
        id="start-date"
        name="program.start"
        placeholder="Date de début"
        label="Date de début"
        fullWidth
        variant="filled"
        type="datetime-local"
        defaultValue=""
        value={props.values.program && props.values.program.start}
        onChange={props.handleChange}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </Grid>
    <Grid item sm={6}>
      <TextField
        id="end-date"
        name="program.end"
        placeholder="Date de fin"
        label="Date de fin"
        fullWidth
        variant="filled"
        type="datetime-local"
        defaultValue=""
        value={props.values.program && props.values.program.end}
        onChange={props.handleChange}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </Grid>
  </Grid>
)

const Subscription = (props: any) => (<div></div>);

const RealState = (props: any) => (<div></div>);

const DevicesRent = (props: any) => (<div></div>);

const FormType: any = {
  "FOOD": Food,
  "PROGRAM": Program,
  "SUBSCRIPTION": Subscription,
  "REALSTATE": RealState,
  "DEVICES_RENT": DevicesRent
}