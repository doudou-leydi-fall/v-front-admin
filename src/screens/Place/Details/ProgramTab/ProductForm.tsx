import React, { useContext } from "react";
import { Formik } from "formik";
import {
  Grid,
  Button
} from "@material-ui/core";
import * as Yup from 'yup';
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_PRODUCT } from "../../../../network";
import { TextField } from "../../../components";
import { ProductContext } from "./ProductStore";
import { useSnackbar } from "notistack";
import SizeField from "../../../components/CustomForms/SizeField";
import SpicyField from "../../../components/CustomForms/SpicyField";
import CurrencyField from "../../../components/CustomForms/CurrencyField";
import SwitchField from "../../../components/Forms/SwitchField";
import FieldBuilder from "../../../components/Forms/FormFields/FieldBuilder";

export default () => {
  const classes = useStyles();
  const { placeid, refetchProducts, dispatch }: any = useContext(ProductContext);
  const { enqueueSnackbar } = useSnackbar();
  const [createProduct, { error, loading }] = useMutation(CREATE_PRODUCT.MUTATION);

  const handleSubmit = async (values: any, actions: any) => {
    console.log("handleSubmit LOG", values);

    try {
      const { data } = await createProduct({
        variables: { product: values }
      });
      if (data && data.createProduct) {
        enqueueSnackbar("Mise à jour effectuée", { variant: "success" });
        refetchProducts()
        dispatch({ type: "OPEN_PRODUCT_DETAIL", payload: data.createProduct })
      }
    } catch (e) {
      console.log("e LOG", e);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map((error: any) => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
    }
  };

  const renderSubmit = () => (
    <div>
      <Button onClick={() => dispatch({ type: "SET_DISPLAY_MODE", payload: 0 })} color="primary">Annuler</Button>
      <Button type="submit" color="primary">
        Creer
      </Button>
    </div>
  );

  const RenderForm = (props: any) => (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <Grid container spacing={1} className={classes.root}>
        <Grid item sm={3}>
          <TextField
            label="Nom"
            name="name"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.name}
            error={props.errors.name}
          />
          <TextField
            label="Quantité"
            name="quantity"
            type="number"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.quantity}
            error={props.errors.quantity}
          />
          <SizeField
            value={props.values.size}
            error={props.errors.size}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <TextField
            label="Prix"
            name="price"
            type="number"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.price}
            error={props.errors.price}
          />
          <CurrencyField
            value={props.values.currency}
            error={props.errors.currency}
            setFieldValue={props.setFieldValue}
          />
          <TextField
            label="Reduction"
            name="discount"
            type="number"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.discount}
            error={props.errors.discount}
            adornment={"%"}
          />
        </Grid>
        <Grid item sm={3}>
          <SpicyField
            value={props.values.spicy}
            error={props.errors.spicy}
            setFieldValue={props.setFieldValue}
          />
          <FieldBuilder
            name="catcol"
            fieldname="CAT_COL"
            value={{
              categoryCatalogue: props.values && props.values.categoryCatalogue,
              collectionCatalogue: props.values && props.values.collectionCatalogue
            }}
            errors={props.errors}
            setFieldValue={props.setFieldValue}
            params={{ owner: props.values && props.values.sellerid }}
          />

          <SwitchField
            name="stock"
            label="En stock"
            value={props.values.stock}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>

        </Grid>
        <Grid sm={12}>
          <FieldBuilder
            name="recipes"
            fieldname="RECIPES"
            value={props.values["recipes"]}
            errors={props.errors["recipes"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid sm={12}>
          <TextField
            label="Description"
            name="description"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.description}
            error={props.errors.description}
            multiline
            rows="5"
          />
        </Grid>
        {renderSubmit()}
      </Grid>
    </form>
  );

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    price: Yup.number()
      .required('Champ obligatoire')
      .integer('Ce champ doit être numérique'),
    currency: Yup.string()
      .required('Champ obligatoire'),
    categoryCatalogue: Yup.string()
      .required('Champ obligatoire'),
    collectionCatalogue: Yup.string()
      .required('Champ obligatoire')
  });

  return (
    <Formik
      initialValues={{
        sellerid: placeid,
        currency: "FCFA",
        stock: true
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20,
    margin: 0
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
});
