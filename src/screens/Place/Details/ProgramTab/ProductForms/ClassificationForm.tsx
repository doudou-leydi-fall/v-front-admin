import { Button } from "@material-ui/core";
import React from "react";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default (props: any) => {
  console.log("props.owner LOG", props.values);
  return (
    <form autoComplete="off" noValidate onSubmit={props.handleSubmit}>
      <FieldBuilder
        name="classification"
        fieldname="PRODUCT_CLASSIFICATION"
        value={props.values}
        errors={props.errors}
        setFieldValue={props.setFieldValue}
        owner={props.values && props.values.owner}
      />
      <div>
        <Button onClick={props.handleReset} color="primary">Annuler</Button>
        <Button type="submit" color="primary">Mettre à jour</Button>
      </div>
    </form>
  );
}