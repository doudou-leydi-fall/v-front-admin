import React from "react";
import { Grid } from "@material-ui/core";
import { TextField } from "../../../../components";
import SwitchField from "../../../../components/Forms/SwitchField";
import FieldBuilder from "../../../../components/Forms/FormFields/FieldBuilder";

export default (props: any) => {

  return (
      <Grid container spacing={1}>
        <Grid item sm={3}>
          <TextField
            label="ID"
            name="id"
            disabled={true}
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.id}
            error={props.errors.id}
          />
        </Grid>
        <Grid item sm={3}>
          <TextField
            label="Nom"
            name="name"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.name}
            error={props.errors.name}
          />
        </Grid>
        <Grid item sm={3}>
          <FieldBuilder
            name="type"
            label="Type de product"
            fieldname="TYPE_PRODUCT"
            value={props.values["type"]}
            errors={props.errors["type"]}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={3}>
          <SwitchField
            name="stock"
            label="En stock"
            value={props.values.stock}
            setFieldValue={props.setFieldValue}
          />
        </Grid>

        <Grid item sm={6}>
          <TextField
            id="start-date"
            name="programStartAt"
            placeholder="Date de début"
            label="Date de début"
            fullWidth
            variant="filled"
            type="datetime-local"
            defaultValue={props.values.programStartAt}
            value={props.values.programStartAt}
            onChange={props.handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item sm={6}>
          <TextField
            id="end-date"
            name="programEndAt"
            placeholder="Date de fin"
            label="Date de fin"
            fullWidth
            variant="filled"
            type="datetime-local"
            defaultValue={props.values.programEndAt}
            value={props.values.programEndAt}
            onChange={props.handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>

        <Grid sm={12}>
          <FieldBuilder
            name="recipes"
            fieldname="RECIPES"
            value={props.values["recipes"]}
            errors={props.errors["recipes"]}
            setFieldValue={props.setFieldValue}
            type={props.values["type"]}
          />
        </Grid>
        <Grid sm={12}>
          <TextField
            label="Description"
            name="description"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.description}
            error={props.errors.description}
            multiline
            rows="5"
          />
        </Grid>
      </Grid>
  )
}