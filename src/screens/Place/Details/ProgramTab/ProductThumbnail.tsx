import React from "react";
import { Thumbnail } from "../../../components";

export const ProductThumbnail = ({ product }:any) => {

  return (
    <Thumbnail
      style={cardStyle}
      key={product.id}
      title={product.name}
      subtitle={"holl"}
      src={product.src}
    />
  );
};

const cardStyle = {
  border: "1px solid rgba(0,0,0,.125)",
  transition: "none",
  width: 300,
  marginLeft: 10,
  marginBottom: 10,
  backgroundColor: "#FFFFFF",
  boxShadow: "0 .125rem .25rem rgba(0,0,0,.075)",
  backgroundClip: "border-box",
  borderRadius: ".25em"
};
