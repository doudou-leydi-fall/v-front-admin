import React, { useState, useEffect, useContext } from "react";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { UPLOAD_PICTURES, DELETE_PICTURES, SET_AVATAR } from "../../../../../network";
import CroppedPanel from "../../../../components/Uploader/CropperPanel";
import { Box } from "@material-ui/core";

interface ProductPanelUploaderProps {
  cardType: string
  id: string
  refetch: any
  pictures: any[]
}

export default ({ product: { cardType, id, pictures }, refetch }: any) => {
  const { enqueueSnackbar } = useSnackbar();
  const [uploadPictures, { loading }] = useMutation(UPLOAD_PICTURES);
  const [deletePictures, { loading: deleteLoading }] = useMutation(DELETE_PICTURES);
  const [setAvatar, { loading: setAvatarLoading }]: any = useMutation(SET_AVATAR);
  //const { refetch }:any = useContext(ProductContext);
  const [uploadDone, setUploadStatus] = useState(false)
  const [deleteDone, setDeleteDone] = useState(false)
  const [isAvatarDone, setAvatarDone] = useState(false)

  const upload = async (pictures: any) => {
    try {
      await uploadPictures({ variables: { model: "PRODUCT", id, pictures, formatType: cardType } });
      enqueueSnackbar("Photos ajoutées", { variant: "success" });
      setUploadStatus(true);
      refetch();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  const deleteSelectedPictures = async (selectedPictures: string[]) => {
    try {
      await deletePictures({ variables: { model: "PRODUCT", modelId: id, picturesIds: selectedPictures } });
      enqueueSnackbar("Photos supprimées", { variant: "success" });
      setDeleteDone(true);
      refetch();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  const setSelectedAvatar = async (selectedAvatar: string) => {
    console.log("setSelectedAvatar LOG", selectedAvatar);
    try {
      await setAvatar({ variables: { model: "PRODUCT", modelId: id, pictureId: selectedAvatar } });
      enqueueSnackbar("Changement image principale Effectué", { variant: "success" });
      setAvatarDone(true);
      refetch();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  return (
    <Box>
      <CroppedPanel
        initialPictures={pictures}
        loading={loading}
        uploadPictures={upload}
        uploadDone={uploadDone}
        setUploadStatus={setUploadStatus}
        refetch={refetch}
        cardType="Square"
        deletePictures={deleteSelectedPictures}
        deleteLoading={deleteLoading}
        deleteDone={deleteDone}
        setDeleteDone={setDeleteDone}
        setAvatar={setSelectedAvatar}
        isAvatarDone={isAvatarDone}
        setAvatarDone={setAvatarDone}
        setAvatarLoading={setAvatarLoading}
      />
    </Box>
  );
}