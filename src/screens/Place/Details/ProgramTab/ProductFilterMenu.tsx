import React from "react";
import { useQuery } from "@apollo/react-hooks";
import DoubleCheckMenu from "../../../components/Menu/DoubleCheckMenu";
import { AGG_PRODUCT_NESTEDFIELD } from "../../../../network";

interface ProductFilterMenuProps {
  setCategories: any
  placeid: string
}

export default ({ setCategories, placeid }: ProductFilterMenuProps) => {
  const { data, refetch }:any = useQuery(AGG_PRODUCT_NESTEDFIELD, { variables: { criteria: [{ name: "seller.id", value: placeid }], parentField: "category.name", childField: "collection.name"}});

  const handleSelected = (items:any) => {
    //setCategories(items);
  }

  return data && data.aggProductByNestedField ? <DoubleCheckMenu onSelected={handleSelected} defaultOpen={true} label="Categories" listItems={data.aggProductByNestedField}/>: <div>Chargement en cours</div>;
}