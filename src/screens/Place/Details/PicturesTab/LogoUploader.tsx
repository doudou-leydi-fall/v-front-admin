import React, { useContext, useState } from "react";
import { useMutation } from "react-apollo";
import { RootContext, UI_REDUCER_TYPES } from "../../../../RootContext";
import { UPLOAD_PICTURE } from "../../../../network";
import CropperSinglePanel from "../../../components/Uploader/CropperSinglePanel";
import PlaceContext from "../Root/PlaceStore";

const LogoUploader = () => {
  const { uidispatch } = useContext(RootContext);
  const { place, refetchPlace }: any = useContext(PlaceContext);
  const [uploadPicture, { loading, error }] = useMutation(UPLOAD_PICTURE);
  const [isUploaded, setUploaded] = useState(false);
  const uploadLogo = async (file: any) => {
    try {
      const { id } = place;
      const { data } = await uploadPicture({
        variables: {
          model: "PLACE",
          id,
          field: "logo",
          picture: file
        }
      });

      if (data.uploadPicture) {
        refetchPlace();
        uidispatch({
          type: UI_REDUCER_TYPES.TRIGGER_SNACK_BAR,
          payload: {
            msg: "Métadonnée ajoutée",
            variant: "success"
          }
        });
        setUploaded(true);
      }
    } catch (e) {
      uidispatch({
        type: UI_REDUCER_TYPES.HANDLE_GQL_ERROR,
        payload: e,
      });
      setUploaded(false);
    }
  }

  const handleRemove = () => {
    console.log("handleRemove");
  }

  return <CropperSinglePanel
    onRemove={handleRemove}
    title="LOGO"
    uploadPictures={uploadLogo}
    loading={loading}
    isUploaded={isUploaded}
    cardType="SquareCardM"
    initialPicture={place.logo}
  />
}

export default LogoUploader;