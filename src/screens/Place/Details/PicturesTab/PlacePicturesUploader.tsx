import React, { useState, useEffect, useContext } from "react";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { UPLOAD_PICTURES, DELETE_PICTURES, SET_AVATAR } from "../../../../network";
import CroppedPanel from "../../../components/Uploader/CropperPanel";
import PlaceContext from "../Root/PlaceStore";
import Box from "@material-ui/core/Box";

const PicturesTab = () => {
  const { place, refetchPlace }: any = useContext(PlaceContext);
  const { pictures, id } = place;
  const { enqueueSnackbar } = useSnackbar();
  const [uploadPictures, { error, loading }] = useMutation(UPLOAD_PICTURES);
  const [deletePictures, { error: deleteError, loading: deleteLoading }] = useMutation(DELETE_PICTURES);
  const [setAvatar, { error: setAvatarError, loading: setAvatarLoading }]: any = useMutation(SET_AVATAR);
  const [uploadDone, setUploadStatus] = useState(false)
  const [deleteDone, setDeleteDone] = useState(false)
  const [isAvatarDone, setAvatarDone] = useState(false)

  const upload = async (pictures: any) => {
    try {
      await uploadPictures({ variables: { model: "PLACE", id, pictures, formatType: "SquareCardXL" } });
      enqueueSnackbar("Photos ajoutées", { variant: "success" });
      setUploadStatus(true);
      refetchPlace();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  const deleteSelectedPictures = async (selectedPictures: string[]) => {
    try {
      await deletePictures({ variables: { model: "PLACE", modelId: id, picturesIds: selectedPictures } });
      enqueueSnackbar("Photos supprimées", { variant: "success" });
      setDeleteDone(true);
      refetchPlace();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  const setSelectedAvatar = async (selectedAvatar: string) => {
    console.log("setSelectedAvatar LOG", selectedAvatar);
    try {
      await setAvatar({ variables: { model: "PLACE", modelId: id, pictureId: selectedAvatar } });
      enqueueSnackbar("Changement image principale Effectué", { variant: "success" });
      setAvatarDone(true);
      refetchPlace();
    } catch (e) {
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  return (
    <Box p={20}>
      <CroppedPanel
        title={"PHOTOS"}
        initialPictures={pictures}
        loading={loading}
        uploadPictures={upload}
        uploadDone={uploadDone}
        setUploadStatus={setUploadStatus}
        refetch={refetchPlace}
        cardType={"SquareCardXL"}
        deletePictures={deleteSelectedPictures}
        deleteLoading={deleteLoading}
        deleteDone={deleteDone}
        setDeleteDone={setDeleteDone}
        setAvatar={setSelectedAvatar}
        isAvatarDone={isAvatarDone}
        setAvatarDone={setAvatarDone}
        setAvatarLoading={setAvatarLoading}
      />
    </Box>
  );
}

export default PicturesTab;