import Box from "@material-ui/core/Box";
import React from "react";
import LogoUploader from "./LogoUploader";
import PlacePicturesUploader from "./PlacePicturesUploader";

export default () => (
  <Box>
    <PlacePicturesUploader/>
    <LogoUploader/>
  </Box>
)