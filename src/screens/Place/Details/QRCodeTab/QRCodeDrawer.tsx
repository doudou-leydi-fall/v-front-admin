import React, { useEffect, useRef, useState } from "react";

interface QRCodeCanvasProps {

}

export default (props: any) => {
  const canvasRef = useRef(null)
  const [context, setContext]:any = useState(null);

  // const draw = (ctx: any) => {
  //   ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  //   ctx.fillStyle = '#000000'
  //   ctx.beginPath()
  //   ctx.arc(50, 100, Math.PI * 2, 0, 2 * Math.PI)
  //   ctx.fill()
  // }

  const draw = () => {
    let img = new Image();
    console.log("img", context);
    //img.onload = drawImage;
    img.src = props.qrcode;
    // context.drawImage(img, 110, 0, 180, 200);
    context.beginPath();
    context.fillStyle = '#1A3D22'
    context.fillRect(0, 0, context.canvas.width, context.canvas.height)
    context.drawImage(img, 1000, 0);
    context.globalCompositeOperation = 'destination-in';
    context.arc(context.canvas.width / 2, context.canvas.width / 2, context.canvas.width / 2, 0, Math.PI * 2);
    context.closePath();
    context.fill();
  }

  const drawImage = () => {

  }

  useEffect(() => {
    const canvas: any = canvasRef.current
    const cw = canvas.width;
    const ch = canvas.height;
    const context = canvas.getContext('2d')
    setContext(context);
    //Our first draw
    // draw(context)
  }, [])

  useEffect(() => {
    console.log("context useEFfect LOG", context);
    if(context) {
      draw();
    }
    
  }, [context]);

  return (
    <canvas ref={canvasRef} {...props} height="500" width="500"/>
  )
}