import { AppBar, Button, CircularProgress, Fab, Grid, Icon, makeStyles, Toolbar } from "@material-ui/core";
import React, { Fragment, useContext } from "react";
import { useMutation, useQuery } from "react-apollo";
import { useSnackbar } from "notistack";
import { GENERATE_QRCODE, GET_QRCODE } from "../../../../network";
import PlaceContext from "../Root/PlaceStore";

export default ({ qrcode }: any) => {
  const { place }: any = useContext(PlaceContext);
  const { data, refetch }:any = useQuery(GET_QRCODE.QUERY, { variables: { id: place.id }});
  const [generateQRCode, { loading }] = useMutation(GENERATE_QRCODE.MUTATION);
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleRefresh = async () => {
    try {
      const { data } = await generateQRCode({
        variables: {
          model: "Place",
          id: place && place.id,
        }
      });

      if (data && data[GENERATE_QRCODE.OPERATION]) {
        enqueueSnackbar("Mise à jour effectuée", { variant: "success" });
        refetch();
        console.log("result", data);
      }
    } catch (e) {
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map((error: any) => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
    }
  };

  return (
    <Fragment>
      <AppBar
        position="static"
        color="default">
        <Toolbar className={classes.toolbar}>
          
        </Toolbar>
      </AppBar>
      <Grid container sm={12}>
        <Grid className={classes.grid} item sm={4}>
          <div className={classes.qrContainer} style={{ backgroundColor: (place.THEME && place.THEME.secondaryColor) || "#4A148C" }}>
            { (data && data[GET_QRCODE.OPERATION]) ? (<img className={classes.qrcode} src={data[GET_QRCODE.OPERATION].url} />) : <Fab onClick={() => handleRefresh()}>{loading ? <CircularProgress size={50} /> : <Icon>refresh</Icon>}</Fab>}
          </div>
        </Grid>
        <Grid item className={classes.grid} sm={8}>

        </Grid>
      </Grid>
      <div className={classes.container}>
        {/* <QRCodeDrawer qrcode={place && place.qrcode && place.qrcode.mobile}/> */}
      </div>
    </Fragment>
  )
}

const useStyles = makeStyles({
  container: {
    display: "flex",
    alignItems: "center"
  },
  grid: {
    marginBottom: 10,
    display: "flex"
  },
  qrContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: 400,
    width: 400,
    minHeight: 400,
    minWidth: 400,
    padding: 50,
    borderRadius: 200,
    marginLeft: 10
  },
  qrcode: {
    height: 300,
    width: 300,
  },
  logoContainer: {
    height: 400,
    width: 400,
    borderRadius: 200,
    marginLeft: 20
  },
  logo: {
    height: 400,
    width: 400,
    borderRadius: 300
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  }
});