import React from "react";
import { Formik } from "formik";
import {
  Grid,
  Button,
  makeStyles,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import * as Yup from 'yup';
import { TextField } from "../../components";
import CountryField from "../../components/CustomForms/CountryField";
import ZoneField from "../../components/CustomForms/ZoneField";
import SwitchField from "../../components/Forms/SwitchField";
import DistrictField from "../../components/CustomForms/DistrictField";
import PlaceCategoryField from "../../components/CustomForms/PlaceCategoryField";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_PLACE_DETAIL } from "../../../network";

export default () => {
  const [createPlace, { error, loading }] = useMutation(CREATE_PLACE_DETAIL);
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyle();
  const handleSubmit = async (values: any, actions: any) => {
    console.log(values);
    try {
      await createPlace({
        variables: { placeInput: values }
      });
    } catch (e) {
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map((error: any) => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
      //console.log(errorsMessage);
    }
  };

  const RenderForm = (props: any) => (
    <div className={classes.container}>
      <Typography variant="h3">Ajouter place</Typography>
      <form className={classes.form} onSubmit={props.handleSubmit}>
        <Grid container>
          <Grid item sm={3}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
            <TextField
              label="Email"
              name="email"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.email}
              error={props.errors.email}
            />
          </Grid>
          <Grid item sm={3} style={styleGridItem}>
            <TextField
              label="Téléphone"
              name="phone"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.phone}
              error={props.errors.phone}
            />
            <CountryField
              setFieldValue={props.setFieldValue}
              value={props.values.location.country}
              error={props.errors.location && props.errors.location.country}
            />

          </Grid>
          <Grid item sm={3} style={styleGridItem}>
            <ZoneField
              setFieldValue={props.setFieldValue}
              value={props.values.location.zone}
              error={props.errors.location && props.errors.location.zone}
            />
            <DistrictField
              setFieldValue={props.setFieldValue}
              value={props.values.location.district}
              zone={props.values.location.zone}
              error={props.errors.location && props.errors.location.district}
            />
          </Grid>
          <Grid item sm={3} style={styleGridItem}>
            <SwitchField
              name="location.ocean"
              label="À proximité de l'océan"
              value={props.values.location.ocean}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
        </Grid>
        <Grid item sm={12} style={styleGridItem}>
          <TextField
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.description}
            name="description"
            label="Description"
            error={props.errors.description}
            placeholder="Description"
            variant="outlined"
            margin="normal"
            multiline={true}
            rows="10"
            fullWidth
          />
        </Grid>
        <Grid item sm={12}>
          <PlaceCategoryField
            value={props.values.categories}
            setFieldValue={props.setFieldValue}
            error={props.errors.categories}
          />
        </Grid>
        <Grid item sm={12}>
          <TextField
            label="Adresse compléte"
            name="location.address"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location && props.values.location.address}
            error={props.errors.location && props.errors.location.address}
            multiline
            rows="3"
          />
        </Grid>
        <Grid item sm={12}>
          <TextField
            label="Description"
            name="description"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.description}
            error={props.errors.description}
            multiline
            rows="5"
          />
        </Grid>
        <div>
          <Button color="primary">Annuler</Button>
          <Button type="submit" color="primary">Creer</Button>
        </div>
      </form>
    </div>
  );

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    email: Yup.string()
      .email('Email invalide')
      .required('Ce champ est obligatoire'),
    phone: Yup.string()
      .min(2, 'Numero invalide')
      .max(50, 'Numero invalide'),
    location: Yup.object().shape({
      country: Yup.string()
        .required('Ce champ est obligatoire'),
      zone: Yup.string()
        .required('Ce champ est obligatoire'),
      district: Yup.string()
        .required('Ce champ est obligatoire'),
      address: Yup.string()
        .required('Ce champ est obligatoire'),
    }),
    categories: Yup.array()
      .required('Champ obligatoire')
  });

  return (
    <Formik
      initialValues={{
        name: "",
        location: {
          address: "",
          country: "",
          ocean: false,
          zone: "",
          district: ""
        },
        phone: "",
        email: "",
        gastronomies: [],
        temporaly: false
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  );
};

const useStyle = makeStyles((theme: any) => ({
  container: {
    padding: 20
  },
  form: {
    marginTop: 20
  }
}));

const styleGridItem = {
  paddingRight: 5,
  paddingLeft: 5
};
