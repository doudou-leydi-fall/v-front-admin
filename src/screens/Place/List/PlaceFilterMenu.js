import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setCategories } from "../../../actions/place/list";
import MenuCkeckList from "../../components/Menu/CheckMenu";
import { AGG_PLACE_FIELD } from "../../../network";

const PlaceFilterMenu = props => {
  const { data } = useQuery(AGG_PLACE_FIELD, { variables: { criteria: [], fieldname: "categories"}});

  const handleSelected = (items) => {
    props.setCategories(items);
  }

  return (
    <MenuCkeckList label="Categories" listItems={data && data.aggPlaceByField} onSelected={handleSelected}/>
  );
}

const mapDispatchToProps = dispatch => bindActionCreators({ setCategories }, dispatch );

export default connect(null, mapDispatchToProps)(PlaceFilterMenu);