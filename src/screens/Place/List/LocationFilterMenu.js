import React from "react";
import DoubleCheckMenu from "../../components/Menu/DoubleCheckMenu";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setDistrict } from "../../../actions/place/list"; 

const data = [
  {
    "name": "Plateau",
    "value": 3,
    "children": [
      {
        "name": "sandaga",
        "value": 3
      }
    ]
  },
  {
    "name": "Corniche-Est",
    "value": 2,
    "children": [
      {
        "name": "Fann",
        "value": 2
      }
    ]
  },
  {
    "name": "Les Almadies",
    "value": 2,
    "children": [
      {
        "name": "Les Almadies",
        "value": 1
      },
      {
        "name": "almadies",
        "value": 1
      }
    ]
  },
  {
    "name": "SICAP",
    "value": 2,
    "children": [
      {
        "name": "Sacrè coeur",
        "value": 1
      },
      {
        "name": "Sacré Coeur 2",
        "value": 1
      }
    ]
  },
  {
    "name": "CORNICHE EST",
    "value": 1,
    "children": [
      {
        "name": "almadies",
        "value": 1
      }
    ]
  },
  {
    "name": "Corniche-Ouest",
    "value": 1,
    "children": [
      {
        "name": "Les mamelles",
        "value": 1
      }
    ]
  },
  {
    "name": "YOFF",
    "value": 1,
    "children": [
      {
        "name": "virage",
        "value": 1
      }
    ]
  }
];

const LocationFilterMenu = props => (
  <DoubleCheckMenu onSelected={props.setDistrict} label="localité" listItems={data}/>
);

const mapDispatchToProps = dispatch => bindActionCreators({ setDistrict }, dispatch);

export default connect(null, mapDispatchToProps)(LocationFilterMenu);