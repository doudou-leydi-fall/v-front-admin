import React, { useEffect } from "react";
import LayoutCard from "../../components/Layout/LayoutCard";
import PlaceCard from "../_views/PlaceCard";
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { SEARCH_PLACE } from "../../../network";
import { useQuery } from "@apollo/react-hooks";
import { InfoPanel } from "../../components/Panel/InfoPanel";
import PaginationPanel from "./PaginationPanel";
import { setBackdrop } from "../../../actions/ui/global";
import { bindActionCreators } from "redux";

const PlaceListPanel = props => {
  const classes = useStyles();
  const { data, loading } = useQuery(SEARCH_PLACE, { variables: { size: props.size, from: props.from, criteria: [ { name: "categories", collection: props.categories }, { name: "location.district", collection: props.district}]}});   
  props.setBackdrop(loading);

  return data && data.searchPlace && data["searchPlace"].data ? (
    <div style={placeContainerStyle}>

        <LayoutCard
          items={data.searchPlace.data}
          childrenComponent={<PlaceCard />}
        />
    
      <PaginationPanel total={data.searchPlace.total}/>
    </div>
  ) : (
      <InfoPanel label="Pas de données"/>
    );
};

const mapStateToProps = state => {
  return { 
    size: state.placeList.pager.size,
    from: state.placeList.pager.from,
    categories: state.placeList.categories,
    district: state.placeList.district
  };
};

const mapDispatchToProps = dispatch => bindActionCreators(
  { 
    setBackdrop,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(PlaceListPanel);

const placeContainerStyle = {
  backgroundColor: "#f8f9fa !important",
  display: "flex",
  flexWrap: "wrap"
};

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));
