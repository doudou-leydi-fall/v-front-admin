import React from "react";
import TablePagination from "@material-ui/core/TablePagination";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setSize, setOffset, setPager } from "../../../actions/place/list";

const mapStateToProps = state => {
  return {
    pager: state.placeList.pager
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ setSize, setOffset, setPager }, dispatch );

export default connect(mapStateToProps, mapDispatchToProps)(props => {
  const handleChangePage = (event, page) => {
    const offset = page * props.pager.size;
    console.log("handleChangePage LOG", props.pager);
    props.setPager({ ...props.pager, page: page, from: offset });
  };

  const handleChangeRowsPerPage = event => {
    props.setPager({ from: 0, page: 0, size: parseInt(event.target.value, 10) })
  };

  return (
    <TablePagination
      rowsPerPageOptions={[1, 5, 10, 25]}
      component="div"
      count={props.total}
      rowsPerPage={props.pager && props.pager.size}
      page={props.pager && props.pager.page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
  )
});
