import React from "react";
import { SearchNavBar } from "./SearchNavBar";
import PlaceListPanel from "./PlaceListPanel";
import PlaceFilterMenu from "./PlaceFilterMenu";
import LocationFilterMenu from "./LocationFilterMenu";
import { makeStyles } from "@material-ui/core";

const PlaceListPage = props => {
  const classes = useStyle();

  return (
    <div>
      <SearchNavBar />
      <div className={classes.bodyList}>
        <div className={classes.menu}>
          <PlaceFilterMenu/>
          <LocationFilterMenu/>
        </div>
        <div className={classes.list}>
          <PlaceListPanel/>
        </div>
      </div>
    </div>
)};
export default PlaceListPage;

const useStyle = makeStyles({
  bodyList: {
    display: "flex"
  },
  menu: {

  },
  list: {
    flexGrow: 1,
  }
});