import React from "react";
import { Card } from "../../components/Card";
import { useHistory } from "react-router-dom";

export default ({ item }) => {
  let history = useHistory();
  return (
  <div onClick={() => history.push(`/place/${item.id}`)}>
    <Card 
      title={item.name}
      content={item.categories.join(" - ")}
      picture={item.src}
      chipFooterLeft={item.location && item.location.zone}
      chipFooterRight={item.location && item.location.district}
    />
  </div>
  )
}