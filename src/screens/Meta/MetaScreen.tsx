import React from "react";
import { TabsPanel } from "../components";
import ClassificationTab from "./ClassificationTab";
import CompositionTab from "./CompositionTab";

const tabs = [
  {
    label: "Classification",
    component: <ClassificationTab />
  },
  {
    label: "Composition",
    component: <CompositionTab />
  },
  {
    label: "Meta",
    component: <ClassificationTab />
  },
];

export default ({ place }:any) => {
  return <TabsPanel tabs={tabs} header={{ title: "Metadonnées"}} avatarEnabled={false}/>;
};
