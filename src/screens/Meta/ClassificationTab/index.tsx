import React from "react";
import { SEARCH_META, ADD_ROOT_CATEGORY, DELETE_ROOT_CLASSIFICATION, ADD_ROOT_COLLECTION } from "../../../network";
import ClassificationTemplate from "../../../templates/Classification";

const config: any = {
  entity: "Metadonnée",

  FETCH_CATEGORY: {
    QUERY: SEARCH_META,
    OPERATION: "searchMeta",
    model: "CLASSIFICATION",
    parseVariables: (params: any) => ({
      model: "Category",
      type: "search",
      criteria: [
        {
          name: "model",
          value: "Category"
        },
      ]
    })
  },

  ADD_CATEGORY: {
    MUTATION: ADD_ROOT_CATEGORY,
    OPERATION: "addRootCategory",
    FIELDS: [{ name: "type", fieldName: "CLASSIFICATION_TYPE"}]
  },

  DELETE_CATEGORY: {
    MUTATION: DELETE_ROOT_CLASSIFICATION,
    OPERATION: "deleteRootClassification",
    variable_name: "meta",
  },

  FETCH_COLLECTION: {
    QUERY: SEARCH_META,
    OPERATION: "searchMeta",
    MODEL: "CLASSIFICATION",
    getVariables: ({ id }: any) => ({
      model: "Collection",
      type: "search",
      criteria: [
        {
          name: "model",
          value: "Collection"
        },
        {
          name: "parentID",
          value: id
        },
      ],
    })
  },

  ADD_COLLECTION: {
    MUTATION: ADD_ROOT_COLLECTION,
    OPERATION: "addRootCollection",
    FIELDS: [{ name: "cardType", fieldName: "CARD_TYPE"}]
  },

  DELETE_COLLECTION: {
    MUTATION: DELETE_ROOT_CLASSIFICATION,
    OPERATION: "deleteRootClassification",
  },
}

export default () => (<ClassificationTemplate config={config} />)