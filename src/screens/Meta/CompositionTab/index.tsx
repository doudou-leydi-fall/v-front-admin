import React, { useContext } from "react";
import { SEARCH_META, ADD_ELEMENT, DELETE_ELEMENT } from "../../../network";
import MetaTemplate from "../../../templates/MetaTable";
import PlaceContext from "../../Place/Details/Root/PlaceStore";

const config: any = {
  entity: "Metadonnée",

  FETCH_ELEMENT: {
    QUERY: SEARCH_META,
    OPERATION: "searchMeta",
    parseVariables: (params: any) => ({
      model: "Composition",
      type: "search",
      criteria: [
        {
          name: "model",
          value: "Element"
        },
      ]
    })
  },

  ADD_ELEMENT: {
    MUTATION: ADD_ELEMENT,
    OPERATION: "addElement",
    FIELDS: [{ name: "type", fieldname: "CLASSIFICATION_TYPE"}]
  },

  DELETE_ELEMENT: {
    MUTATION: DELETE_ELEMENT,
    OPERATION: "deleteElement"
  },
}

export default () => {
  const {} = useContext(PlaceContext);

  return (<MetaTemplate config={config} />);
}