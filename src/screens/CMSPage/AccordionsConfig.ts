import { GET_CMS_PAGE } from "../../network";

export default {
  FETCH_HIGH_LEVEL: {
    QUERY: GET_CMS_PAGE,
    OPERATION: "getCmsPage",
  }
}