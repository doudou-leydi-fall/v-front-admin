import React from 'react';
import SectionGroup from '../../templates/SectionGroup';
import config from "./AccordionsConfig";

export default () => (<SectionGroup config={config}/>);