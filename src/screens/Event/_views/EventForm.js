import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Grid, Button } from "@material-ui/core";
import { Formik } from "formik";
import { useSnackbar } from "notistack";
import { CREATE_EVENT } from "../../../network";
import {
  TextField,
  SelectField,
  PricesField,
  SuggestField,
  DateField,
  SwitchField
} from "../../components";
import PlaceContext from "../../Place/Details/Context";

const country = require("./country.json");

const categories = [
  {
    value: "concert",
    label: "Concert"
  },
  {
    value: "Soirée",
    label: "Soirée"
  },
  {
    value: "Piscine Party",
    label: "Piscine Party"
  }
];

const collections = [
  {
    value: "",
    label: ""
  },
  {
    value: "concert",
    label: "Concert"
  },
  {
    value: "Brunch",
    label: "brunch"
  },
  {
    value: "soirée",
    label: "Soirée"
  }
];

export const EventForm = props => (
  <form autoComplete="no-completion" onSubmit={props.handleSubmit}>
    <Grid style={styleGrid}>
      <Grid style={styleGridItem} sm={4}>
        <TextField
          type="text"
          name="name"
          label="Nom"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.name}
          error={props.errors.name}
        />
        <DateField
          name="start"
          label={"Date de début"}
          onChange={props.handleChange}
          setFieldValue={props.setFieldValue}
          onBlur={props.handleBlur}
          value={props.values.start}
          error={props.errors.start}
          minDate={new Date()}
        />
        <DateField
          name="end"
          label={"Date de fin"}
          onChange={props.handleChange}
          setFieldValue={props.setFieldValue}
          onBlur={props.handleBlur}
          value={props.values.end}
          error={props.errors.end}
          minDate={new Date()}
        />
      </Grid>
      <Grid xs={4}>
        <TextField
            name="location.address"
            label="Adresse complète"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location.address}
            error={props.errors.location && props.errors.location.address}
          />
          <TextField
            type="text"
            name="location.zone"
            label="Zone"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location.zone}
            error={props.errors.location && props.errors.location.zone}
          />
          <SelectField
            type="text"
            name="location.country"
            label="Pays"
            options={country}
            optionLabelName={"name"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            onBlur={props.handleBlur}
            value={props.values.location.country}
            error={props.errors.location && props.errors.location.country}
          />
          <SwitchField 
            name="location.ocean"
            label="À proximité"
            value={props.values.location.ocean}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
          />
      </Grid>
      <Grid item sm={4} style={styleGridItem}>
          <PricesField value={props.values.prices} setFieldValue={props.setFieldValue} onChange={props.handleChange} onBlur={props.handleBlur}/>
      </Grid>
    </Grid>
    <Grid item xs={12}>
      <TextField
        name="description"
        label="Description"
        onChange={props.handleChange}
        onBlur={props.handleBlur}
        value={props.values.description}
        error={props.errors.description}
        multiline
        rows="4"
      />
    </Grid>
    <Grid item xs={12}>
      <SuggestField
        name="categories"
        label="Categories"
        onChange={props.handleChange}
        setFieldValue={props.setFieldValue}
        value={props.values.categories}
        options={categories}
        filterSelectedOptions
        handleValue={value => value.value}
        getOptionLabel={option => option.label}
        error={props.errors.categories}
        onBlur={props.handleBlur}
      />
    </Grid>
    <Grid item xs={12}>
      <SuggestField
        name="tags"
        label="Tags"
        onChange={props.handleChange}
        setFieldValue={props.setFieldValue}
        value={props.values.tags}
        options={categories}
        filterSelectedOptions
        handleValue={value => value.value}
        getOptionLabel={option => option.label}
        error={props.errors.tags}
        onBlur={props.handleBlur}
      />
    </Grid>
    <div>
      <Button disabled={props.activeStep === 0} onClick={props.handleBack}>
        Retour
      </Button>
      <Button type="submit" color="primary">
        Creer
      </Button>
    </div>
  </form>
);

export const AddEventToPlace = ({ handleNext, addToStore, setLoading }) => {
  const place = useContext(PlaceContext);
  const [createEvent, error, loading] = useMutation(CREATE_EVENT);
  let { id } = useParams();
  const { enqueueSnackbar } = useSnackbar();
  const handleSubmit = async (values, actions) => {
    console.log("handleSubmit values", values);
    try {
      setLoading(true);
      const result = await createEvent({
        variables: { event: values }
      });
      setLoading(false);
      addToStore({ id: result.data.createEvent });
      handleNext();
    } catch (e) {
      setLoading(false);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map(error => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
      //console.log(errorsMessage);
    }
  };
  const initialValues = {
    name: "",
    description: "",
    categories: [],
    place: {
      id: place.id,
      name: place.name
    },
    location: {
      address: "",
      country: "",
      zone: "",
      ocean: false
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      render={EventForm}
      onSubmit={handleSubmit}
    />
  );
};

const styleGrid = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-between"
};

const styleGridItem = {
  paddingRight: 5,
  paddingLeft: 5
};
