import React, { useEffect, useReducer, useContext, useState } from "react";
import { useSnackbar } from "notistack";
import { useQuery } from "@apollo/react-hooks";
import { makeStyles } from "@material-ui/core/styles";
import PlaceContext from "../../Place/Details/Context";
import { EventMenu } from "./EventMenu";
import { EventSearchResult } from "./EventSearchResult";
import { SEARCH_EVENT } from "../../../network";

export const styles = makeStyles(theme => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "row"
  },
  eventsContainer: {
    flexGrow: 2
  }
}));

const searchParamsReducer = (state, action) => {
  switch(action.type) {
    case 'ADD_COLLECTION':
      //const filters
      console.log("ADD_COLLECTION", state.filters);
      const collection = [...collection, action.payload]
    case 'SIZE_CHANGE':
      return {...state, size: action.payload};
    default:
      return state;
  }
}

export const EventSearchPanel = ({ placeid, template, setTemplate }) => {
  const classes = styles();
  const place = useContext(PlaceContext);
  const { enqueueSnackbar } = useSnackbar();
  const [size, setSize] = useState(10);
  const [collections, setCollection] = useState([]);
  const [params, dispatch] = useReducer(searchParamsReducer, {criteria: [], size: 10, from: 0}); 
  const { loading, error, data, refetch } = useQuery(SEARCH_EVENT, {
    variables: { criteria: [{name: "place.id", value: place.id}], size: 10, from: 0 }
  });

  if (error) {
    if (error.networkError) {
      enqueueSnackbar(String(error.networkError), { variant: "error" });
    }
    if (error.graphQLErrors) {
      error.graphQLErrors.map(error => {
        enqueueSnackbar(error.message, { variant: "error" });
      });
    }
  }

  return (
    <div className={classes.root}>
      <EventMenu
        setCollection={setCollection}
        dispatch={dispatch}
        placeid={placeid}
        setTemplate={setTemplate}
        className={classes.menu}
      />
      <EventSearchResult
        events={data && data.searchEvent && data.searchEvent.data}
        className={classes.eventsContainer}
      />
    </div>
  );
};
