import React from 'react';
import * as moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Alarm } from "@material-ui/icons";
import { Skeleton } from "@material-ui/lab";

const useStyles = makeStyles({
  root: {
    width: "100%",
    marginBottom: 10
  },
  startContainer: {
    display: "flex"
  },
  startIcon: {
    marginLeft: 10,
    color: "#F76B1E"
  }
});

const EventDetailsPanel = ({event}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        { event && event.avatar ? (<CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="340"
          image={event.avatar}
          title="Contemplative Reptile"
        />) : (
          <Skeleton variant="rect" height={340} />
        )}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {event.name || "Non rens"}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
      <div className={classes.startContainer}>
            <Alarm color={"#F76B1E"}/>
        <Typography className={classes.startIcon} variant="body2" color="textSecondary" component="p">{event.start ? moment(event.start).format("DD/MM/YYYY") : "Non Rens."}</Typography>
          </div>
      </CardActions>
    </Card>
  );
}

export default EventDetailsPanel;