import React, { Fragment, useState, useMemo } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useQuery } from "@apollo/react-hooks";
import * as moment from 'moment';
import { GROUP_BY_DATE_EVENT } from "../../../network";
//import ProductStore from "../Store/Search";
import {
  Divider,
  ListSubheader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  ListItemSecondaryAction,
  Checkbox,
  Button
} from "@material-ui/core";

export const EventMenu = ({ setCollection, placeid, setTemplate }) => {
  //const [criteria, dispatch] = ProductStore;
  const classes = useStyles();
  const [collections, setCollections] = React.useState([{}]);
  const { data, refetch } = useQuery(GROUP_BY_DATE_EVENT, { variables: { criteria: [{"name": "place.id", "value": placeid}] } });
  const [items, setItems] = useState([]);
  console.log("items LOG", items);
  const handleChecked = (checked, value) => {
    console.log("checked", checked);
    console.log("value", value);
    if (checked) {
      setCollections([...collections, value]);
    } else {
      setCollections(collections.filter(c => c != value));
    } 
  }

  useMemo(()=> {
    console.log("data LOG", data);
    if (data && data.groupByDateEvent) {
      setItems(data.groupByDateEvent);
    }
  }, [data]);

  console.log("data criteria log", items);
  return (
    <div className={classes.root}>
      <Button
        variant="outlined"
        color="secondary"
        className={classes.buttonCreate}
        onClick={() => setTemplate(1)}
      >
        Nouvel évènement
      </Button>
          <Fragment>
            <Divider />
            <List
              component="nav"
              aria-labelledby="nested-list-subheader"
              subheader={
                <ListSubheader className={classes.categoryName} component="div">
                  Calendrier d'évènement
                </ListSubheader>
              }
              className={classes.root}
            >
            {data && data.groupByDateEvent && data.groupByDateEvent && Array.isArray(data.groupByDateEvent) && data.groupByDateEvent.map((category, key) => (
              <ListItem key={key} button>
                    <ListItemIcon>
                      <span className={classes.count}>{category.value}</span>
                    </ListItemIcon>
                    <ListItemText
                      className={classes.collectionName}
                      primary={category.name ? moment(category.name).format("DD/MM/YYYY") : "Non Rens."}
                      variant="menu"
                    />
                    <ListItemSecondaryAction>
                      <Checkbox
                        edge="end"
                        checked={collections.includes(category.name)}
                        onChange={(e, checked) => handleChecked(checked, category.name)}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                  ))}
            </List>
          </Fragment>
        
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: 260,
    maxWidth: 260,
    minWidth: 260,
    backgroundColor: "#F5F5F5",
    textAlign: "center"
  },
  buttonCreate: {
    margin: "20px 0px"
  },

  nested: {
    paddingLeft: theme.spacing(4)
  },
  count: {
    background: "#F76C1E",
    height: 20,
    borderRadius: 30,
    padding: "2px 7px",
    fontSize: 12,
    fontWeight: 700,
    color: "#FFF"
  },
  categoryName: {
    textTransform: "capitalize"
  },
  collectionName: {
    textTransform: "capitalize",
    fontSize: 13
  }
}));
