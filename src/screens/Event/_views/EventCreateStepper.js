import React from "react";
import { FormatAlignJustify, AddPhotoAlternate } from "@material-ui/icons";
import { StepperComponent } from "../../components/Stepper";
import { AddEventToPlace } from "./EventForm";
import { EventPicturesUpload } from "./EventPicturesUpload";

const stepsConfig = {
  name: "eventtStepper",
  label: "Nouvel évènement",
  finalLink: "/place",
  cancelLink: "/place/list",
  steps: [
    {
      name: "Fiche évènement",
      component: <AddEventToPlace />,
      icon: <FormatAlignJustify />
    },
    {
      name: "Photos",
      component: <EventPicturesUpload />,
      icon: <AddPhotoAlternate />
    }
  ]
};

export const EventCreateStepper = ({ setTemplate }) => {
  return (
    <div>
      <StepperComponent
        stepsConfig={stepsConfig}
        onFinish={() => setTemplate(0)}
      />
    </div>
  );
};
