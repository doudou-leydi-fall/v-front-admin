import React from 'react';
import * as moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Alarm } from "@material-ui/icons";
import { Skeleton } from "@material-ui/lab";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    width: "100%",
    marginBottom: 10
  },
  startContainer: {
    display: "flex"
  },
  startIcon: {
    marginLeft: 10,
    color: "#F76B1E"
  }
});

const EventCard = ({event, setSelectedItem}) => {
  const classes = useStyles();

  return (
    <Card onClick={() => setSelectedItem(event)} className={classes.root}>
      <CardActionArea>
        { event.avatar ? (<CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="140"
          image={event.avatar}
          title="Contemplative Reptile"
        />) : (
          <Skeleton variant="rect" height={140} />
        )}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {event.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {event.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
      <div className={classes.startContainer}>
            <Alarm color={"#F76B1E"}/>
            <Typography className={classes.startIcon} variant="body2" color="textSecondary" component="p">{event.start ? moment(event.start).format("DD/MM/YYYY") : "Non Rens."}</Typography>
          </div>
      </CardActions>
    </Card>
  );
}

export default EventCard;