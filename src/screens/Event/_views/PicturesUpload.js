import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { useSnackbar } from "notistack";
import { PicturesUploader } from "../../../components";
import { UPLOAD_PICTURES_EVENT } from "../../../../network";

const EventPicturesUpload = props => {
  const { enqueueSnackbar } = useSnackbar();
  const [uploadPicturesEvent, error, loading] = useMutation(UPLOAD_PICTURES_EVENT);

  const handleSubmit = async (pictures) => {
    try {
      props.setLoading(true);
      const response = await uploadPicturesEvent({variables: {id: props.store.id, pictures: pictures}});
      enqueueSnackbar("Photos ajoutées", { variant: "success" });
        //addToStore(files);
      props.setLoading(false);
      props.handleNext();
    } catch (e) {
      props.setLoading(false);
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }
  return <PicturesUploader {...props} onSubmit={handleSubmit}/>
}

export default EventPicturesUpload;
