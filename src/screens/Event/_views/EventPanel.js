import React, { useState } from "react";
import { EventCreateStepper } from "./EventCreateStepper";
import { EventSearchPanel } from "./EventSearchPanel";
//import { ProductToolbar } from "./ProductToolbar";

export const EventPanel = ({ tabStore }) => {
  const [tpl, setTpl] = useState(0);

  return (
    <div>
      {tpl === 0 ? (
        <EventSearchPanel
          placeid={tabStore.placeid}
          template={tpl}
          setTemplate={setTpl}
        />
      ) : (
        <EventCreateStepper template={tpl} setTemplate={setTpl} />
      )}
    </div>
  );
};
