import React, { useEffect, useState } from "react";
import { useSnackbar } from "notistack";
import { Typography, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { ExpandMore } from "@material-ui/icons";
import { useQuery } from "@apollo/react-hooks";
import { SEARCH_PRODUCT } from "../../../network";
import EventCard from "./EventCard";
import { ThumbnailImage } from "../../components";
import EventDetailsPanel from "./EventDetailsPanel";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    padding: 20
  },
  listContainer: {
    width: 360,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    paddingRight: 10
  },
  card: {
    width: 360
  }
}));

export const EventSearchResult = ({ events = [] }) => {
  const classes = useStyles();
  const [selectedItem, setSelectedItem] = useState({});
  console.log("selectedItem LOG", selectedItem);
  return (
    <div className={classes.root}>
      <div className={classes.listContainer}>
        { events.map((event, key) => <EventCard key={key} event={event} setSelectedItem={setSelectedItem}/>)}
      </div>
      <div>
        <EventDetailsPanel event={selectedItem}/>
      </div>
    </div>
  );
};
