import React, { Fragment } from 'react';
import clsx from 'clsx';
import {
  useRouteMatch,
  Link,
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { Icon } from '@material-ui/core';
import CMSPage from './CMSPage';
import MetaScreen from './Meta/MetaScreen';
import { MetricScreen } from './Metric/MetricScreen';
import { PlaceScreen } from './Place/PlaceScreen';
import { ProfileScreen } from './Profile/ProfileScreen';

const drawerWidth = 240;

const MenuItemData = [
  {
    name: "common",
    children: [
      {
        label: "Analytics",
        to: "/metric",
        icon: "trending_up"
      },
      {
        label: "Recherche",
        to: "/metric",
        icon: "search"
      },
      {
        label: "Place",
        to: "/place/list",
        icon: "home_work"
      },
      {
        label: "CMS",
        to: "/cmspage",
        icon: "important_devices"
      },
      {
        label: "Métadonnées",
        to: "/meta",
        icon: "ballot"
      },
      {
        label: "Gestion utilisateur",
        to: "#",
        icon: "account_circle"
      },
      {
        label: "Contrats",
        to: "/profile/list",
        icon: "account_balance"
      }
    ]
  }
]

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: 10,
    },
    logo: {
      marginRight: 10
    },
    link: {
      textDecoration: "unset"
    }
  }),
);

export default () => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Router basename="/v-backoffice">
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MenuIcon />
            </IconButton>
            <img
              height="50"
              width="50"
              src={"/assets/img/dkrv-logo-3.png"}
              alt="dkrv-logo-3"
              className={classes.logo}
            />
            <Typography variant="h6" noWrap>
              v-admin
          </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <Menu items={MenuItemData} classes={classes} />
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <LayoutContent classes={classes} />
        </main>
      </div>
    </Router>
  );
}

const LayoutContent = ({ classes }: any) => {
  let { path } = useRouteMatch();
  return (
    <main className={classes.content}>
      <Switch>
        <Route path="/metric">
          <MetricScreen />
        </Route>
        <Route path="/cmspage">
          <CMSPage />
        </Route>
        <Route path="/meta">
          <MetaScreen />
        </Route>
        <Route path="/place">
          <PlaceScreen />
        </Route>
        <Route path="#">
          <ProfileScreen />
        </Route>
      </Switch>
    </main>
  );
};

const menuStyles = makeStyles(() => ({
  link: {
    textDecoration: "unset"
  }
}));

const Menu = ({ items = [] }: any) => {
  const classes = menuStyles();
  return items.map((item: any) => <SubMenu item={item} classes={classes} />);
}

const SubMenu = ({ item, classes }: any) => {
  return (
    <Fragment>
      <Divider />
      {item.children.map((subitem: any) => <MenuItem {...subitem} classes={classes} />)}
    </Fragment>
  );
}

const MenuItem = ({ label, to, classes, icon, activeOnlyWhenExact }: any) => {
  let match:any = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  });

  return (
    <Link className={classes.link} to={to}>
      <ListItem selected={match} button>
        <ListItemIcon><Icon>{icon}</Icon></ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    </Link>
  )
}