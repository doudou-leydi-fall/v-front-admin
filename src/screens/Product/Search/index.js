import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ProductMenu } from "./_views/ProductMenu";
import { ProductSearchResult } from "./_views/ProductSearchResult";

export const styles = makeStyles(theme => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "row"
  }
}));

export const ProductSearchPanel = () => {
  const classes = styles();

  return (
    <div className={classes.root}>
      <ProductMenu className={classes.menu} />
      <ProductSearchResult />
    </div>
  );
};
