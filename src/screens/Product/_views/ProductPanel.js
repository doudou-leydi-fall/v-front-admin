import React, { useState } from "react";
import { CreateProductStepper } from "../Create/_views/CreateProductStepper";
import { ProductSearchPanel } from "./ProductSearchPanel";
import { ProductToolbar } from "./ProductToolbar";

export const ProductPanel = ({ tabStore }) => {
  const [tpl, setTpl] = useState(0);

  return (
    <div>
      {tpl == 0 ? (
        <ProductSearchPanel
          placeid={tabStore.placeid}
          template={tpl}
          setTemplate={setTpl}
        />
      ) : (
        <CreateProductStepper template={tpl} setTemplate={setTpl} />
      )}
    </div>
  );
};
