import React, { useEffect, useReducer, useState } from "react";
import { useSnackbar } from "notistack";
import { useQuery } from "@apollo/react-hooks";
import { makeStyles } from "@material-ui/core/styles";
import { ProductMenu } from "./ProductMenu";
import { ProductSearchResult } from "./ProductSearchResult";
import { SEARCH_PRODUCT } from "../../../network";

export const styles = makeStyles(theme => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    textAlign: "center"
  },
  productContainer: {
    flexGrow: 2
  }
}));

const searchParamsReducer = (state, action) => {
  switch(action.type) {
    case 'ADD_COLLECTION':
      //const filters
      console.log("ADD_COLLECTION", state.filters);
      const collection = [...collection, action.payload]
    case 'SIZE_CHANGE':
      return {...state, size: action.payload};
    default:
      return state;
  }
}

export const ProductSearchPanel = ({ placeid, template, setTemplate }) => {
  const classes = styles();
  const { enqueueSnackbar } = useSnackbar();
  const [size, setSize] = useState(10);
  const [collections, setCollection] = useState([]);
  const [params, dispatch] = useReducer(searchParamsReducer, {criteria: [], size: 10, from: 0}); 
  const { loading, error, data, refetch } = useQuery(SEARCH_PRODUCT, {
    variables: { criteria: [{name: "collection", collection: collections}], size: 10, from: 0 }
  });

  useEffect(() => {
    refetch();
  });
  console.log(params);

  if (error) {
    if (error.networkError) {
      enqueueSnackbar(String(error.networkError), { variant: "error" });
    }
    if (error.graphQLErrors) {
      error.graphQLErrors.map(error => {
        enqueueSnackbar(error.message, { variant: "error" });
      });
    }
  }

  return (
    <div className={classes.root}>
      <ProductMenu
        setCollection={setCollection}
        dispatch={dispatch}
        placeid={placeid}
        setTemplate={setTemplate}
        className={classes.menu}
      />
      <ProductSearchResult
        products={data && data.searchProduct.data}
        className={classes.productContainer}
      />
    </div>
  );
};
