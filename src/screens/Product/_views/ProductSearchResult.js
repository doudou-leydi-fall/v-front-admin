import React, { useEffect } from "react";
import { useSnackbar } from "notistack";
import { makeStyles } from "@material-ui/core/styles";
import { useQuery } from "@apollo/react-hooks";
import { SEARCH_PRODUCT } from "../../../network";
import { ProductCard } from "./ProductCard";
import { InfoPanel } from "../../components";

const useStyles = makeStyles(theme => ({
  root: {
    //width: 360
    flexGrow: 2,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap"
  },
  card: {
    width: 360
  }
}));

export const ProductSearchResult = ({ products = [] }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {products.length ? (
        products.map(product => (
          <ProductCard product={product} classes={classes.card} />
        ))
      ) : (
        <InfoPanel label={"Chargement en cours ..."} />
      )}
    </div>
  );
};
