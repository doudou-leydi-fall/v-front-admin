import React, { Fragment, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useQuery } from "@apollo/react-hooks";
import { PRODUCT_AGG_CATCOL } from "../../../network";
//import ProductStore from "../Store/Search";
import {
  Divider,
  ListSubheader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  ListItemSecondaryAction,
  Checkbox,
  Button
} from "@material-ui/core";

export const ProductMenu = ({ setCollection, placeid, setTemplate }) => {
  //const [criteria, dispatch] = ProductStore;
  const classes = useStyles();
  const [collections, setCollections] = React.useState([]);
  const { data, refetch } = useQuery(PRODUCT_AGG_CATCOL, { variables: { placeid } });

  useEffect(() => {
    setCollection(collections);
    refetch();
  });

  const handleChecked = (checked, value) => {
    if (checked) {
      setCollections([...collections, value]);
    } else {
      setCollections(collections.filter(c => c != value));
    } 
  }

  //console.log("data", criteria);
  return (
    <div className={classes.root}>
      <Button
        variant="outlined"
        color="secondary"
        className={classes.buttonCreate}
        onClick={() => setTemplate(1)}
      >
        Nouveau produit
      </Button>
      {data &&
        data.getProductAgg.map((category, key) => (
          <Fragment>
            <Divider />
            <List
              component="nav"
              aria-labelledby="nested-list-subheader"
              subheader={
                <ListSubheader className={classes.categoryName} component="div">
                  {category.name}
                </ListSubheader>
              }
              className={classes.root}
            >
              {category &&
                category.children.map((col, key) => (
                  <ListItem key={key} button>
                    <ListItemIcon>
                      <span className={classes.count}>{col.value}</span>
                    </ListItemIcon>
                    <ListItemText
                      className={classes.collectionName}
                      primary={col.name}
                      variant="menu"
                    />
                    <ListItemSecondaryAction>
                      <Checkbox
                        edge="end"
                        checked={collections.includes(col.name)}
                        onChange={(e, checked) => handleChecked(checked, col.name)}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
            </List>
          </Fragment>
        ))}
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: 260,
    maxWidth: 260,
    minWidth: 260,
    backgroundColor: "#F5F5F5"
  },
  buttonCreate: {
    margin: "20px 0px"
  },

  nested: {
    paddingLeft: theme.spacing(4)
  },
  count: {
    background: "#F76C1E",
    height: 20,
    borderRadius: 30,
    padding: "2px 7px",
    fontSize: 12,
    fontWeight: 700,
    color: "#FFF"
  },
  categoryName: {
    textTransform: "capitalize"
  },
  collectionName: {
    textTransform: "capitalize"
  }
}));
