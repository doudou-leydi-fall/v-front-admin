import React, { useReducer, useContext } from "react";

const SearchContext = React.createContext({});
const initialState = [];

const reducer = useReducer((state, action) => {
  switch (action.type) {
    case "addCategoryCriteria":
      return [...state, action.payload];
    case "decrement":
      return state - 1;
    case "reset":
      return 0;
    default:
      throw new Error("Unexpected action");
  }
}, initialState);

export const SearchProvider = ({ children }) => {
  const contextValue = useReducer(reducer, initialState);
  return (
    <SearchContext.Provider value={contextValue}>
      {children}
    </SearchContext.Provider>
  );
};

const useCriteria = () => {
  const contextValue = useContext(SearchContext);
  return contextValue;
};

export default useCriteria;
