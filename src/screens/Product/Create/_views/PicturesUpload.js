import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { useSnackbar } from "notistack";
import { PicturesUploader } from "../../../components";
import { UPLOAD_PICTURES_PRODUCT } from "../../../../network";

export const ProductPicturesUpload = props => {
  const { enqueueSnackbar } = useSnackbar();
  const [uploadPicturesProduct, error, loading] = useMutation(
    UPLOAD_PICTURES_PRODUCT
  );

  const handleSubmit = async pictures => {
    try {
      props.setLoading(true);
      console.log("addProductPictures", pictures);
      const response = await uploadPicturesProduct({
        variables: { productid: props.store.id, pictures: pictures }
      });
      enqueueSnackbar("Photos ajoutées", { variant: "success" });
      //addToStore(files);
      props.setLoading(false);
      props.handleNext();
    } catch (e) {
      props.setLoading(false);
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  };
  return <PicturesUploader {...props} onSubmit={handleSubmit} />;
};
