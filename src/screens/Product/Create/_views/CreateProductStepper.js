import React from "react";
import { FormatAlignJustify, AddPhotoAlternate } from "@material-ui/icons";
import { StepperComponent } from "../../../components/Stepper";
import { AddProductToPlace } from "./ProductForm";
import { ProductPicturesUpload } from "./PicturesUpload";

const stepsConfig = {
  name: "productStepper",
  label: "Nouveau product",
  finalLink: "/place",
  cancelLink: "/place/list",
  steps: [
    {
      name: "Fiche produit",
      component: <AddProductToPlace />,
      icon: <FormatAlignJustify />
    },
    {
      name: "Photos",
      component: <ProductPicturesUpload />,
      icon: <AddPhotoAlternate />
    }
  ]
};

export const CreateProductStepper = ({ setTemplate }) => {
  return (
    <div>
      <StepperComponent
        stepsConfig={stepsConfig}
        onFinish={() => setTemplate(0)}
      />
    </div>
  );
};
