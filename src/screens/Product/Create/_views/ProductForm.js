import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Grid, Button, InputAdornment } from "@material-ui/core";
import { Formik } from "formik";
import { useSnackbar } from "notistack";
import { ADD_PRODUCT } from "../../../../network";
import {
  TextField,
  SelectField,
  SelectNumericField,
  SuggestField
} from "../../../components";

const categories = [
  {
    value: "food",
    label: "Repas"
  },
  {
    value: "smoke",
    label: "Fumette"
  },
  {
    value: "dessert",
    label: "Dessert"
  }
];

const collections = [
  {
    value: "crepes",
    label: "Crêpes"
  },
  {
    value: "pizzas",
    label: "Pizzas"
  },
  {
    value: "chicha",
    label: "Chicha"
  }
];

const sizes = [
  {
    value: "SMALL",
    label: "Petite"
  },
  {
    value: "MEDIUM",
    label: "Moyenne"
  },
  {
    value: "BIG",
    label: "Grande"
  }
];

const quantities = [
  {
    value: 1,
    label: "1"
  },
  {
    value: 2,
    label: "2"
  },
  {
    value: 3,
    label: "3"
  }
];

const spicyLevel = [
  {
    value: 0,
    label: "Non épicé"
  },
  {
    value: 1,
    label: "Légèrement épicé"
  },
  {
    value: 2,
    label: "Épicé"
  },
  {
    value: 3,
    label: "Très épicé"
  }
];

const recipes = [
  {
    value: "tomate",
    label: "Tomate"
  },
  {
    value: "viande",
    label: "Viande"
  },
  {
    value: "poulet",
    label: "Poulet"
  },
  {
    value: "orange",
    label: "Orange"
  },
  {
    value: "framboise",
    label: "Framboise"
  },
  {
    value: "ananas",
    label: "Ananas"
  },
  {
    value: "banane",
    label: "Banane"
  },
  {
    value: "fromage",
    label: "Fromage"
  },
  {
    value: "Poisson",
    label: "poisson"
  },
  {
    value: "Pomme de terre",
    label: "pomme_terre"
  }
];

export const ProductForm = props => {
  const classes = useStyles(); 

  return (
    <form className={classes.root} autoComplete="no-completion" onSubmit={props.handleSubmit}>
      <Grid style={styleGrid}>
        <Grid style={styleGridItem} sm={4}>
          <TextField
            type="text"
            name="name"
            label="Nom"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.name}
            error={props.errors.name}
          />
          <TextField
            type="number"
            name="price"
            label="Prix"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.price}
            error={props.errors.price}
            InputProps={{
              endAdornment: <InputAdornment position="start">FCFA</InputAdornment>
            }}
          />
          <TextField
            type="number"
            name="discount"
            label="Discount"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.discount}
            error={props.errors.name}
            InputProps={{
              startAdornment: <InputAdornment position="start">%</InputAdornment>
            }}
          />
        </Grid>
        <Grid item sm={4} style={styleGridItem}>
          <SelectField
            name="collection"
            label={"Collection"}
            optionLabelName={"label"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            value={props.values.collection}
            options={collections}
            onBlur={props.handleBlur}
          />
          <SelectField
            name="size"
            label={"Taille"}
            optionLabelName={"label"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            value={props.values.size}
            options={sizes}
            onBlur={props.handleBlur}
          />
        </Grid>
        <Grid item sm={4} style={styleGridItem}>
          <SelectNumericField
            type="number"
            name="quantity"
            label="Quantité"
            optionLabelName={"label"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            value={props.values.quantity}
            options={quantities}
            onBlur={props.handleBlur}
          />
          <SelectField
            name="spicy"
            label="Épices"
            optionLabelName={"label"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            value={props.values.spicy}
            options={spicyLevel}
            onBlur={props.handleBlur}
          />
          {props.errors.name && <div id="feedback">{props.errors.name}</div>}
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <TextField
          name="description"
          label="Description"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.description}
          error={props.errors.description}
          multiline
          rows="4"
        />
      </Grid>
      <Grid item xs={12}>
        <SuggestField
          name="recipes"
          label="Recipes"
          onChange={props.handleChange}
          setFieldValue={props.setFieldValue}
          value={props.values.recipes}
          options={recipes}
          filterSelectedOptions
          handleValue={value => value.value}
          getOptionLabel={option => option.label}
          error={props.errors.recipes}
          onBlur={props.handleBlur}
        />
      </Grid>
      <Grid item xs={12}>
        <SuggestField
          name="categories"
          label="Categories"
          onChange={props.handleChange}
          setFieldValue={props.setFieldValue}
          value={props.values.categories}
          options={categories}
          filterSelectedOptions
          handleValue={value => value.value}
          getOptionLabel={option => option.label}
          error={props.errors.categories}
          onBlur={props.handleBlur}
        />
      </Grid>
      <div>
        <Button disabled={props.activeStep === 0} onClick={props.handleBack}>
          Retour
      </Button>
        <Button type="submit" color="primary">
          Creer
      </Button>
      </div>
    </form>
  );
}

export const AddProductToPlace = ({ handleNext, addToStore, setLoading }) => {
  const [addProductToPlace, error, loading] = useMutation(ADD_PRODUCT);
  let { id } = useParams();
  const { enqueueSnackbar } = useSnackbar();
  const handleSubmit = async (values, actions) => {
    try {
      setLoading(true);
      const result = await addProductToPlace({
        variables: { placeid: id, product: values }
      });
      setLoading(false);
      addToStore({ id: result.data.addProductToPlace });
      handleNext();
    } catch (e) {
      setLoading(false);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map(error => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
      //console.log(errorsMessage);
    }
  };
  const initialValues = {
    name: "",
    discount: 0,
    description: ""
  };

  return (
    <Formik
      initialValues={initialValues}
      render={ProductForm}
      onSubmit={handleSubmit}
    />
  );
};

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
});

const styleGrid = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-between"
};

const styleGridItem = {
  paddingRight: 5,
  paddingLeft: 5
};

export default AddProductToPlace;
