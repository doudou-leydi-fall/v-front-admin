import React from "react";
import { Formik } from "formik";
import * as Yup from 'yup';
import { useSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { UPDATE_PLACE } from "../../../network";
import TextField from "../../components/Forms/TextField";
import CountryField from "../../components/CustomForms/CountryField";
import ZoneField from "../../components/CustomForms/ZoneField";
import SwitchField from "../../components/Forms/SwitchField";
import DistrictField from "../../components/CustomForms/DistrictField";
import PlaceCategoryField from "../../components/CustomForms/PlaceCategoryField";
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import { setBackdrop } from "../../../actions/ui/global";
import { setDisplayMode } from "../../../actions/place/details";
import { bindActionCreators } from "redux";
import { useMutation } from "@apollo/react-hooks";

const PlaceUpdateForm = ({ data, setDisplay, setBackdrop }) => {
  const classes = useStyles();
  const [updatePlace, { error, loading }] = useMutation(UPDATE_PLACE);
  const { enqueueSnackbar } = useSnackbar();
  setBackdrop(loading);
  const handleSubmit = async (values, actions) => {
    console.log(values);

    try {
      const { data } = await updatePlace({
        variables: { place: values }
      });
      if (data && data.updatePlace) {
        enqueueSnackbar("Mise à jour effectuée", { variant: "success" })
      }
      setDisplay(0);
    } catch (e) {
      console.log("e LOG", e);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map(error => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
    }
  };

  const ValidationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, 'Ce nom est trop court')
      .max(50, 'Ce nom dépasse la taille maximale')
      .required('Champ obligatoire'),
    email: Yup.string()
      .email('Email invalide')
      .required('Ce champ est obligatoire'),
    phone: Yup.string()
      .min(2, 'Numero invalide')
      .max(50, 'Numero invalide')
  });

  const RenderForm = props => {
    return (
      <form autoComplete="off" noValidate className={classes.root} onSubmit={props.handleSubmit}>
        <Grid container spacing={1}>
          <Grid item sm={3}>
            <TextField
              label="Nom"
              name="name"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              error={props.errors.name}
            />
            <TextField
              label="Email"
              name="email"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.email}
              error={props.errors.email}
            />
          </Grid>
          <Grid item sm={3}>
            <TextField
              label="Téléphone"
              name="phone"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.phone}
              error={props.errors.phone}
            />
            <CountryField
              setFieldValue={props.setFieldValue}
              value={props.values.location.country}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={3}>
            <ZoneField
              setFieldValue={props.setFieldValue}
              value={props.values.location.zone}
              setFieldValue={props.setFieldValue}
            />
            <DistrictField
              setFieldValue={props.setFieldValue}
              value={props.values.location.district}
              zone={props.values.location.zone}
            />
          </Grid>
          <Grid item sm={3}>
            <SwitchField
              name="location.ocean"
              label="À proximité de l'océan"
              value={props.values.location.ocean}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={12}>
            <PlaceCategoryField
              value={props.values.categories}
              setFieldValue={props.setFieldValue}
            />
          </Grid>
          <Grid item sm={12}>
            <TextField
              label="Adresse compléte"
              name="location.address"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.location && props.values.location.address}
              error={props.errors.location && props.errors.location.address}
              multiline
              rows="3"
            />
          </Grid>
          <Grid item sm={12}>
            <TextField
              label="Description"
              name="description"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.description}
              error={props.errors.description}
              multiline
              rows="5"
            />
          </Grid>
          <Grid sm={12}>
            <ButtonGroup variant="contained" color="primary">
              <Button onClick={() => setDisplay(0)}>Annuler</Button>
              <Button type="submit">Valider</Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
    );
  };

  const initialValues = {
    id: data.id,
    name: data.name || "",
    location: {
      country: data.location && data.location.country,
      zone: data.location && data.location.zone,
      district: data.location && data.location.district,
      address: data.location && data.location.address || "Non rens.",
      ocean: data.location && data.location.ocean,
    },
    email: data.email,
    phone: data.phone,
    categories: data.categories || [],
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      render={RenderForm}
      validationSchema={ValidationSchema}
    />
  )
}

const useStyles = makeStyles({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    padding: 20
  },
  btnGroupContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
});

const mapDispatchToProps = dispatch => bindActionCreators({ setBackdrop, setDisplayMode }, dispatch);

export default connect(null, mapDispatchToProps)(PlaceUpdateForm);
