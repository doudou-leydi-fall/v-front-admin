import React, { Fragment } from "react";
import { TabsPanel } from "../components";
import { UserEvo } from "./UserEvo";
import { ZoneMetric } from "./Metric";

export const CommonMetric = props => (
  <Fragment>
    <ZoneMetric />
  </Fragment>
);

const tabs = [
  {
    label: "Générale",
    component: <CommonMetric />
  },
  {
    label: "Place",
    component: <CommonMetric />
  },
  {
    label: "Product",
    component: <CommonMetric />
  }
];

export const MetricScreen = props => {
  return (
    <div>
      <TabsPanel
        style={{
          paddingTop: 10,
          backgroundColor: "#FAFAFA"
        }}
        tabs={tabs}
      />
    </div>
  );
};
