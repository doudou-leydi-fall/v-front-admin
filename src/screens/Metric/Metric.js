import React from "react";
import { Typography, Divider, Grid } from "@material-ui/core";
import { useQuery } from "@apollo/react-hooks";
import { AGG_PLACE_FIELD } from "../../network";
import { makeStyles } from "@material-ui/core/styles";
import { VictoryPie } from 'victory';
import PlaceZoneMetric from "./PlaceZoneMetric";

export const ZoneMetric = () => {
  const classes = useStyles();
  const { data } = useQuery(AGG_PLACE_FIELD, { variables: { criteria: [], fieldname: "categories" } });
  console.log("METRIC_USER_ZONE", data);
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <Typography variant="h3">Inscription utilisateur</Typography>
      </div>
      <Divider />
      <Grid container>
        <Grid item sm={4}>
          <VictoryPie
            colorScale={["#4A148C", "#F76D1E", "#F4D03F", "#C0392B", "#208449", "#3498DB", "#EC7064"]}
            data={data && data.aggPlaceByField.map(item => ({ x: `${item.name} : ${item.value}`, y: item.value }))}
          />
        </Grid>
        <Grid item sm={4}>
          <PlaceZoneMetric />
        </Grid>
        <Grid item sm={4}>
          <VictoryPie
            colorScale={["#4A148C", "#F76D1E", "#F4D03F", "#C0392B", "#208449", "#3498DB", "#EC7064"]}
            data={data && data.aggPlaceByField.map(item => ({ x: `${item.name} : ${item.value}`, y: item.value }))}
          />
        </Grid>
      </Grid>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    background: "#FFF",
    border: "1px solid #edf2f9",
    borderRadius: 3,
    width: "100%",
    marginTop: 20
  },
  chart: {
    width: "40%"
  },
  header: {
    height: 50,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px 20px"
  },
  body: {
    display: "flex",
    justifyContent: "center",
    alignItems: "start",
    padding: 30
  }
}));
