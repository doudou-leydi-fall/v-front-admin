import React from "react";
import { VictoryPie } from 'victory';
import { AGG_PLACE_FIELD } from "../../network";
import { useQuery } from "@apollo/react-hooks";

export default props => {
  const { data, refetch } = useQuery(AGG_PLACE_FIELD, { variables: { criteria: [], fieldname: "location.zone" } });

  return (
    <VictoryPie
      colorScale={["#4A148C", "#F76D1E", "#F4D03F", "#C0392B", "#208449", "#3498DB", "#EC7064"]}
      data={data && data.aggPlaceByField.map(item => ({ x: `${item.name} : ${item.value}`, y: item.value }))}
    />
  );
}