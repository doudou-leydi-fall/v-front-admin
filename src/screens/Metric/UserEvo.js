import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { Typography, Divider } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import * as moment from "moment";
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  ReferenceLine,
  Tooltip,
  AreaChart,
  Area,
  Legend
} from "recharts";
import { MetricTable } from "../components/Metrics/MetricTable";
import { METRIC_REGISTER } from "../../network";

const useStyles = makeStyles(theme => ({
  container: {
    background: "#FFF",
    border: "1px solid #edf2f9",
    borderRadius: 3,
    width: "100%"
  },
  header: {
    height: 50,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px 20px"
  },
  body: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: 30
  }
}));

const columns = [
  {
    name: "Date"
  },
  {
    name: "Nombre d'inscrits",
    align: "right"
  }
];
export const UserEvo = () => {
  const classes = useStyles();
  const { data } = useQuery(METRIC_REGISTER);
  console.log("data.metricRegister", data && data.metricRegister);
  const formatDate = data => {
    if (data) {
      return (
        data &&
        data.metricRegister.map(({ name, value }) => ({
          name: moment(name).format("DD/MM/YYYY HH:mm"),
          value
        }))
      );
    }
    return [];
  };
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <Typography variant="h3">Inscription utilisateur</Typography>
      </div>
      <Divider />
      <div className={classes.body}>
      <ResponsiveContainer width={700} height="80%">
        <LineChart
          data={
            data &&
            data.metricRegister.map(({ name, value }) => ({
              name: moment(name).format("DD/MM/YYYY HH:mm"),
              value
            }))
          }
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis dataKey="value" />
          <Tooltip />
          <Line
            connectNulls
            type="monotone"
            dataKey="value"
            stroke="#8884d8"
            fill="#8884d8"
          />
        </LineChart>
        </ResponsiveContainer>
        <ResponsiveContainer width={700} height="80%">
    <AreaChart data={data}
      margin={{ top: 20, right: 30, left: 0, bottom: 0 }}>
      <XAxis dataKey="name" />
      <YAxis />
      <CartesianGrid strokeDasharray="3 3" />
      <Tooltip />
      <ReferenceLine x="Page C" stroke="green" label="Min PAGE" />
      <ReferenceLine y={4000} label="Max" stroke="red" strokeDasharray="3 3" />
      <Area type="monotone" dataKey="uv" stroke="#8884d8" fill="#8884d8" />
    </AreaChart>
  </ResponsiveContainer>
        <MetricTable columns={columns} metrics={formatDate(data)} />
      </div>
    </div>
  );
};
