import React from "react";
import { Formik } from "formik";
import {
  FormControlLabel,
  Button
} from "@material-ui/core";
import { TextField } from "../components";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { LOGIN } from "../../network";


export const LoginForm = props => {
  const [login, error, loading] = useMutation(LOGIN);

  const handleSubmit = async (values, actions) => {
    console.log(values);
    const { email, password } = values;
    try {
      const result = await login({
        variables: { email, password }
      });
    } catch (e) {
      if (e.networkError) {
        
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map(error => {
          
        });
      }
    };
  }

  const RenderForm = props => (
    <form onSubmit={props.handleSubmit}>
      <TextField
        id="email"
        label="Email Address"
        name="email"
        onChange={props.handleChange}
        onBlur={props.handleBlur}
        value={props.values.email}
        error={props.errors.email}
      />
      <TextField
        name="password"
        label="Password"
        type="password"
        autoComplete="current-password"
        onChange={props.handleChange}
        onBlur={props.handleBlur}
        value={props.values.password}
        error={props.errors.password}
      />
      <Button
        fullWidth
        variant="contained"
        color="primary"
        type="submit"
      >
        Se connecter
              </Button>
      <Button
        fullWidth
        variant="contained"
        color="default"
      
      > S'inscrire</Button>
    </form>
  );

  return (
    <Formik
      initialValues={{
        email: "",
        password: ""
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
    />
  );
};
