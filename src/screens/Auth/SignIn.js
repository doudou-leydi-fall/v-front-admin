import React from "react";
import { Link, Paper, Grid} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { LOGIN } from "../../network";
import { makeStyles } from "@material-ui/core/styles";
import { LoginForm } from "./LoginForm";
const SignInScreen = () => {
  const classes = useStyles();

  return (
    <div>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <img
              height="100"
              width="100"
              src="/assets/img/dkrv-logo-3.png"
              alt="dkrv-logo-3"
            />
            <Typography component="h1" variant="h5">
              Espace professionnel
            </Typography>
            <LoginForm/>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url('../../../../assets/img/dakar-plateau.jpg')",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "dark"
        ? theme.palette.grey[900]
        : theme.palette.grey[50],
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  src: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  signup: {
    margin: theme.spacing(0, 0, 2)
  }
}));

export default SignInScreen;
