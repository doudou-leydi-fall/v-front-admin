import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { ProfileListLayout } from "./List";
import { CreateUserStepper } from "./Create";
import UserDetail from "./Details";

export const ProfileScreen = () => {
  let { path } = useRouteMatch();
  console.log("path", path);
  return (
    <div>
      <Switch>
        <Route path={`${path}/create`}>
          <CreateUserStepper />
        </Route>
        <Route path={`${path}/list`}>
          <ProfileListLayout />
        </Route>
        <Route path={`${path}/:id`} children={<UserDetail />} />
      </Switch>
    </div>
  );
};
