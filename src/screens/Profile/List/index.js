import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { SEARCH_USER } from "../../../network";
import { TableToolbar } from "./TableToolbar";
import {
  LinearProgress,
  TablePagination,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
  Paper,
  Avatar
} from "@material-ui/core";
import TableContainer from '@material-ui/core/TableContainer';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  avatar: {
    height: 50,
    width: 50,
    margin: "auto"
  }
}));

export const ProfileListLayout = () => {
  let history = useHistory();
  const classes = useStyles();

  const [pager, setPager] = useState({ size: 10, from: 0 });
  const [page, setPage] = useState(0);
  //const [loading, setLoading] = useState(false);
  const { data, refetch, loading } = useQuery(SEARCH_USER, {
    variables: { size: 10, from: 0 }
  });

  useEffect(() => {
    console.log("useEffect LOG", pager);
    refetch();
  });

  const handleChangeRowsPerPage = (data, value) => {
    console.log("handleChangeRowsPerPage log", data.target.value);
    setPager({ ...pager, size: data.target.value });
  };

  const handleChangePage = (data, page) => {
    console.log("onChangePage", page);
    setPager({ ...pager, from: page * pager.size });
    setPage(page);
  };
  const handleSearch = data => {
    console.log("onSearchChange", data);
    setPager({ ...pager, q: data.target.value });
  };

  return (
    <div>
      {loading && <LinearProgress color="secondary" />}
      <TableToolbar handleSearch={handleSearch} />
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Photo profil</TableCell>
              <TableCell align="left">Nom complet</TableCell>
              <TableCell align="left">Prénom</TableCell>
              <TableCell align="left">Nom</TableCell>
              <TableCell align="left">Email</TableCell>
              <TableCell align="left">Téléphone</TableCell>
              <TableCell align="center">Roles</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data && data.searchUser && data.searchUser.data ? (
              data.searchUser.data.map(row => (
                <TableRow
                  onClick={() => history.push(`/profile/${row.id}`)}
                  key={row.name}
                >
                  <TableCell align="center">
                    <Avatar
                      className={classes.avatar}
                      alt="Remy Sharp"
                      src={
                        row.avatar ||
                        process.env.PUBLIC_URL + "/assets/img/user.png"
                      }
                    />
                  </TableCell>
                  <TableCell align="left">{row.fullname}</TableCell>
                  <TableCell align="left">{row.firstname}</TableCell>
                  <TableCell align="left">{row.lastname}</TableCell>
                  <TableCell align="left">
                    {row.email}
                  </TableCell>
                  <TableCell align="left">
                    {row.phone}
                  </TableCell>
                  <TableCell align="center">
                    {row.roles && row.roles.join("-")}
                  </TableCell>
                </TableRow>
              ))
            ) : (
              <Typography variant="h6">Liste vide</Typography>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data && data.searchUser && data.searchUser.total}
        rowsPerPage={pager.size}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

const ToolBar = () => {};
