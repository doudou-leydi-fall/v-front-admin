import React from "react";
import { TablePagination } from "@material-ui/core";

export const PaginationPanel = props => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  console.log("PaginationPanel props", props);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (<TablePagination
    rowsPerPageOptions={[5, 10, 25]}
    component="div"
    count={40}
    rowsPerPage={20}
    page={1}
    onChangePage={handleChangePage}
    onChangeRowsPerPage={handleChangeRowsPerPage}
  />)
}