import React, { useContext } from "react";
import { ThumbnailsPanel } from "../../../components";
import UserContext from "../Context";

export const PicturesPanel = props => {
  const { pictures } = useContext(UserContext);
  return <ThumbnailsPanel pictures={pictures} />;
};
