import React from "react";
import { TabsPanel } from "../../../components";
import { PicturesPanel } from "./PicturesPanel";

const tabs = [
    {
      label: "Photos",
      component: <PicturesPanel/>
    },
    {
      label: "Abonnés",
      component: <PicturesPanel/>
    },
    {
      label: "Agenda",
      component: <PicturesPanel/>
    },
    {
      label: "LIKES",
      component: <PicturesPanel/>
    },
    {
      label: "Notifications",
      component: <PicturesPanel/>
    }
  ];

export const UserDetailsTabs = props => {
  return (
    <TabsPanel tabs={tabs}/>
  );
};
