import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import * as moment from "moment";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_USER } from "../../../../network";
import { ConfirmModal } from "../../../components";
import {
  Typography,
  Avatar,
  Toolbar,
  AppBar,
  Button,
  ButtonGroup,
  Divider,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Grid
} from "@material-ui/core";
import {
  AlternateEmail,
  Phone,
  Public,
  PermContactCalendar,
  EventAvailable,
  Map,
  MyLocation,
  Facebook,
  Twitter,
  Instagram
} from "@material-ui/icons";
import { useHistory } from "react-router-dom";

export const DetailsBar = ({ data }) => {
  const classes = useStyles();
  let history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [ modal, setModal ] = useState(false);
  const [ deleteUser, error ]= useMutation(DELETE_USER, {
    variables: { userid: data.id }
  });

  const handleDelete = async () => {
    try {
      const { data } = await deleteUser();
      setModal(false);
      if (data.deleteUser) {
        history.push('/profile/list');
      }
    } catch(e) {
      if (e) {
        if (e.networkError) {
          enqueueSnackbar(String(e.networkError), { variant: "error" });
        }
        if (e.graphQLErrors) {
          e.graphQLErrors.map(error => {
            enqueueSnackbar(error.message, { variant: "error" });
          });
        }
      }
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="#696969">
        <Toolbar className={classes.toolbar}>
          <span className={classes.leftItem}>
            {data.avatar ? (
              <Avatar
                alt="Remy Sharp"
                src={data.avatar}
                className={classes.avatar}
              />
            ) : (
              <Avatar
                alt="Remy Sharp"
                src={"/assets/img/user.png"}
                className={classes.avatar}
              />
            )}
            <Typography variant="h3" className={classes.title}>
              {data.artistname || data.fullname || "Non renseigné"}
            </Typography>
            <Typography className={classes.categories} variant="p">
              Genre: {data.gender || "Non rens."}
            </Typography>
            <Typography className={classes.categories} variant="p">
              Prénom: {data.firstname || "Non rens."}
            </Typography>
            <Typography className={classes.categories} variant="p">
              Nom: {data.lastname || "Non rens."}
            </Typography>
          </span>
          <div>
            <ButtonGroup
              color="primary"
              size="small"
              aria-label="outlined primary button group"
            >
              <Button
                classes={classes.menuButton}
                color="secondary"
                onClick={() => history.push("/profile/list")}
              >
                retour
              </Button>
              <Button
                classes={classes.menuButton}
                color="secondary"
                onClick={() => setModal(true)}
              >
                Supprimer
              </Button>
              <Button>Modifier</Button>
            </ButtonGroup>
          </div>
        </Toolbar>
        <Divider />
        <Grid container>
          <Grid xs={3}>
            <InfoItem label={"Référence"} value={data.id} />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Téléphone"}
              icon={<Phone size="small" />}
              value={data.phone}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Email"}
              icon={<AlternateEmail />}
              value={data.email}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Roles"}
              value={(data.roles && data.roles.join("-")) || "Non Rens."}
            />
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
        <Grid xs={3}>
            <InfoItem
              icon={<PermContactCalendar />}
              label={"Date de naissance"}
              value={
                data.birthdate
                  ? moment(data.birthdate).format("DD/MM/YYYY")
                  : "Non Rens."
              }
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Pays"}
              icon={<Public />}
              value={data.location.country}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem label={"Zone"} icon={<Map />} value={data.location.zone} />
          </Grid>
          <Grid xs={3}>
            <InfoItem label={"Adresse"} value={data.location.address} />
          </Grid>
          
        </Grid>
        <Divider />
        <Grid container>
        <Grid xs={3}>
            <InfoItem
              label={"Coordonnées"}
              icon={<MyLocation />}
              value={`Lon: ${(data.location.coordinates &&
                data.location.coordinates.lon) ||
                "Non rens."} - Lat: ${(data.location.coordinates &&
                data.location.coordinates.lat) ||
                "Non rens."}`}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Facebook"}
              icon={<Facebook />}
              value={data.facebook}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Twitter"}
              icon={<Twitter />}
              value={data.twitter}
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Insta"}
              icon={<Instagram />}
              value={data.instagram || "Non Rens."}
            />
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
          <Grid xs={3}>
            <InfoItem
              label={"Date de création"}
              icon={<EventAvailable />}
              value={
                data.CREATED_AT
                  ? moment(data.CREATED_AT).format("DD/MM/YYYY HH:mm")
                  : "Non Rens."
              }
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Dernière modification"}
              value={
                data.LAST_UPDATE_DATE
                  ? moment(data.LAST_UPDATE_DATE).format("DD/MM/YYYY HH:mm")
                  : "Non Rens."
              }
            />
          </Grid>
          <Grid xs={3}>
            <InfoItem
              label={"Dernière modification"}
              value={data.LAST_UPDATE_USER || "Non Rens."}
            />
          </Grid>
        </Grid>
      </AppBar>
      <ConfirmModal open={modal} action={handleDelete} buttonLabel="Supprimer"/>
    </div>
  );
};

const InfoItem = props => {
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar>{props.icon}</Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={props.label}
        secondary={props.value || "Non Rens."}
      />
    </ListItem>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    background: "#FFFFFF",
    boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03) !important",
    border: "1px solid #edf2f9"
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  menuButton: {
    root: {
      marginRight: theme.spacing(2),
      backgroundColor: "#F76D1E"
    }
  },
  leftItem: {
    display: "flex",
    alignItems: "center"
  },
  categories: {
    marginLeft: 15
  },
  title: {
    marginLeft: 5
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    marginBottom: 10,
    marginTop: 10,
    marginRight: 10
  }
}));
