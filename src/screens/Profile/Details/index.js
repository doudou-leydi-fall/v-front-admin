import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { GET_USER_DETAIL } from "../../../network/index";
import { DetailsBar } from "./_views/DetailsBar";
import { UserDetailsTabs } from "./_views/UserDetailsTabs";
import { UserProvider } from "./Context";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center"
  }
}));

const UserDetail = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let { id } = useParams();
  const { loading, error, data } = useQuery(GET_USER_DETAIL, {
    variables: { userid: id }
  });

  if (error) {
    if (error.networkError) {
      enqueueSnackbar(String(error.networkError), { variant: "error" });
    }
    if (error.graphQLErrors) {
      error.graphQLErrors.map(error => {
        enqueueSnackbar(error.message, { variant: "error" });
      });
    }
  }

  return data && data.getUser ? (
    <UserProvider value={data.getUser}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <DetailsBar data={data.getUser} />
          </Grid>
          <Grid item xs={12} sm={12}>
            <UserDetailsTabs />
          </Grid>
        </Grid>
      </div>
    </UserProvider>
  ) : (
    <span>Veuillez patientez svp</span>
  );
};

export default UserDetail;
