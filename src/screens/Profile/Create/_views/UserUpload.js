import React from "react";
import { PicturesUploader } from '../../../components';
import { useMutation } from "@apollo/react-hooks";
import { useSnackbar } from "notistack";
import { PROFILE_PICTURES_UPLOAD } from "../../../../network";

export const UserUpload = props => {
  const { enqueueSnackbar } = useSnackbar();
  const [addPicturesUser] = useMutation(PROFILE_PICTURES_UPLOAD);

  const handleSubmit = async (pictures) => {
    try {
      props.setLoading(true);
      const response = await addPicturesUser({variables: {userid: props.store.id, pictures: pictures}});
      enqueueSnackbar("Photos ajoutées", { variant: "success" });
        //addToStore(files);
      props.setLoading(false);
      props.handleNext();
    } catch (e) {
      props.setLoading(false);
      enqueueSnackbar(String(e), { variant: "error" });
      console.log(e);
    }
  }

  return <PicturesUploader {...props} onSubmit={handleSubmit}/>
}