import React from "react";
import { Typography } from "@material-ui/core";
import { FormatAlignJustify, AddPhotoAlternate } from "@material-ui/icons";
import { StepperComponent } from "../../../components/Stepper";
import { UserForm } from "./UserForm";
import { UserUpload } from "./UserUpload";

const stepsConfig = {
  name: "userStepper",
  label: "Nouvel utilisateur",
  finalLink: "/profile",
  cancelLink: "/profile/list",
  steps: [
    {
      name: "Fiche produit",
      component: <UserForm />,
      icon: <FormatAlignJustify />
    },
    {
      name: "Fiche produit",
      component: <UserUpload />,
      icon: <AddPhotoAlternate />
    }
  ]
};

export const CreateUserStepper = () => {
  return (
    <div>
      <StepperComponent stepsConfig={stepsConfig} />
    </div>
  );
};
