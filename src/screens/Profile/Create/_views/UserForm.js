import React from "react";
import { Formik } from "formik";
import {
  Grid,
  Button,
} from "@material-ui/core";
import { TextField, SelectField, DateField, SwitchField, SuggestField } from "../../../components";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_USER } from "../../../../network";
const country = require("./country.json");

const genders = [
  { label: "Homme", value: "h" },
  { label: "Femme", value: "f" },
];

const rolesOptions = [
  { label: "Administrateur", value: "ADMIN" },
  { label: "Utilisateur", value: "USER" },
  { label: "Professionel", value: "PRO" },
];

export const UserForm = ({
  handleNext,
  handleBack,
  handleReset,
  handleCancel,
  addToStore,
  store,
  setLoading
}) => {
  const [createUser, error, loading] = useMutation(CREATE_USER);
  const { enqueueSnackbar } = useSnackbar();
  const handleSubmit = async (values, actions) => {
    console.log('values log', values);
    console.log(values);
    try {
      setLoading(true);
      console.log("setLoading", setLoading);
      const result = await createUser({
        variables: { user: values }
      });
      setLoading(false);
      console.log('registe result', result);
      addToStore({ id: result.data.createUser });
      handleNext();
    } catch (e) {
      setLoading(false);
      if (e.networkError) {
        enqueueSnackbar(String(e.networkError), { variant: "error" });
      }
      if (e.graphQLErrors) {
        e.graphQLErrors.map(error => {
          enqueueSnackbar(error.message, { variant: "error" });
        });
      }
      //console.log(errorsMessage);
    }
  };

  const renderSubmit = props => (
    <div>
      <Button color="primary" onClick={handleCancel}>Annuler</Button>
      <Button type="submit" color="primary">
        Creer
      </Button>
    </div>
  );

  const RenderForm = props => (
    <form onSubmit={props.handleSubmit}>
      <Grid style={styleGrid}>
        <Grid style={styleGridItem} sm={4}>
          <SelectField
            name="gender"
            label={"Genre"}
            options={genders}
            optionLabelName={"label"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            value={props.values.gender}
          />
          <TextField
            type="text"
            name="firstname"
            label="Prenom"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.firstname}
            error={props.errors.firstname}
          />
          <TextField
            type="text"
            name="lastname"
            label="Nom"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.lastname}
            error={props.errors.lastname}
          />
          <TextField
            type="text"
            name="fullname"
            label="Nom complet"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.fullname}
            error={props.errors.fullname}
          />
          <TextField
            type="text"
            name="artistname"
            label="Nom artiste"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.artistname}
            error={props.errors.artistname}
          />
        </Grid>
        <Grid item sm={4} style={styleGridItem}>
          <DateField
            name="birthdate"
            label={"Date de naissance"}
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
            onBlur={props.handleBlur}
            value={props.values.birthdate}
            error={props.errors.birthdate}
          />
          <TextField
            type="text"
            name="phone"
            label="Téléphone"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.phone}
            error={props.errors.phone}
          />
          <TextField
            type="text"
            name="email"
            label="Email"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.email}
            error={props.errors.email}
          />
          <TextField
            type="password"
            name="password"
            label="Mot de passe"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.password}
            error={props.errors.password}
          />
        </Grid>
        <Grid item sm={4} style={styleGridItem}>
          <TextField
            name="location.address"
            label="Adresse complète"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location.address}
            error={props.errors.location && props.errors.location.address}
          />
          <TextField
            type="text"
            name="location.zone"
            label="Zone"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location.zone}
            error={props.errors.location && props.errors.location.zone}
          />
          <TextField
            type="text"
            name="location.country"
            label="Pays"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.location.country}
            error={props.errors.location && props.errors.location.country}
          />
          <SwitchField
            name="visibility"
            label="Visibilté"
            onChange={props.handleChange}
            setFieldValue={props.setFieldValue}
          />
        </Grid>
        <Grid item sm={12} style={styleGridItem}>
          <SuggestField
            name="tags"
            label="tags"
            setFieldValue={props.setFieldValue}
            value={props.values.tags}
            options={country}
            getOptionLabel={option => option.name}
            handleValue={value => value.value}
            error={props.errors.tags}
            onBlur={props.handleBlur}
          />
        </Grid>
        <Grid item sm={12} style={styleGridItem}>
          <SuggestField
            name="roles"
            label="roles"
            setFieldValue={props.setFieldValue}
            value={props.values.roles}
            options={rolesOptions}
            getOptionLabel={option => option.label}
            handleValue={value => value.value}
            error={props.errors.roles}
            onBlur={props.handleBlur}
          />
        </Grid>
        <Grid item sm={12} style={styleGridItem}>
          <TextField
            name="description"
            label="Description"
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            value={props.values.description}
            error={props.errors.description}
            rows="10"
          />
        </Grid>
      </Grid>
      {renderSubmit()}
    </form>
  );

  return (
    <Formik
      initialValues={{
        gender: "h",
        fullname: "",
        firstname: "",
        lastname: "",
        artistname: "",
        birthdate: new Date(),
        email: "",
        phone: "",
        location: {
          address: "",
          country: "",
          zone: "",
          location: {
            lon: 0,
            lat: 0
          }
        },
        password: ""
      }}
      onSubmit={handleSubmit}
      render={RenderForm}
    />
  );
};

const styleGrid = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-between"
};

const styleGridItem = {
  paddingRight: 5,
  paddingLeft: 5
};
