import React, { Fragment } from "react";
import Backdrop from "../components/Panel/Backdrop";
import { useSnackbar } from "notistack";
import { connect } from "react-redux";

const UIController = props => {
  console.log("UIController LOG", props);
  //const { enqueueSnackbar } = useSnackbar();
  //enqueueSnackbar(String(props.alert.message), { variant: props.alert.status });
  return (
    <Fragment>
      <Backdrop open={props.open}/>
    </Fragment>
  );
}

const mapStateToProps = state => {
  return {
    open: state.ui.backdropOpen,
  }
};

export default connect(mapStateToProps)(props => (<UIController {...props}/>))